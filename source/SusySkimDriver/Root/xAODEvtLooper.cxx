#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/OutputStream.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoopAlgs/AlgSelect.h>
#include <EventLoop/Worker.h>
#include <SusySkimDriver/xAODEvtLooper.h>
#include "xAODBase/IParticleHelpers.h"
#include "TTreeFormula.h"
#include "SusySkimMaker/ConfigMgr.h"
//#include "SusySkimMaker/StatusCodeCheck.h"
#include "AsgMessaging/StatusCode.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/Timer.h"
//#include "AthenaKernel/errorcheck.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "SusySkimMaker/PkgDiscovery.h"

// RootCore includes
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/BaseUser.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"

// c++
#include <sstream>

// this is needed to distribute the algorithm to the workers
ClassImp(xAODEvtLooper)

// ----------------------------------------------------------------------------------------------- //
xAODEvtLooper::xAODEvtLooper() : 
  m_event(0), 
  m_configMgr(0), 
  m_dir(0), 
  m_store(0), 
  m_baseUser(0),                                
  m_isData(0), 
  m_isAf2(0), 
  m_isTruthOnly(false), 
  m_writeSkims(1), 
  m_writeTrees(0),
  m_writeMetaData(3), 
  m_nEvents(1.0),
  m_derivationName(""),
  m_noCutBookkeepers(false),
  m_disableCuts(false),
  m_sysSetName(""), 
  m_selectorName(""),
  m_sampleName(""),
  m_deepConfig(""),
  m_isDerivation(false),
  m_nProcEvents(-1),
  m_ievent(0),
  m_add_MET_with_muons_invisible(false),
  m_add_MET_with_electrons_invisible(false),
  m_add_MET_with_photons_invisible(false),
  m_add_MET_with_leptons_invisible(false),
  m_add_loose_MET_WP(false),    
  m_add_tighter_MET_WP(false),  
  m_add_tenacious_MET_WP(false),
  m_emulateAuxTriggers(false),
  m_saveTaus(false),
  m_truthJetContainerName("AntiKt4TruthDressedWZJets"),
  m_doPhoton(true),
  m_doFatjet(false),
  m_fatjetContainerName("AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"),
  m_truthFatjetContainerName("AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"),
  m_doTrackjet(false),
  m_trackjetContainerName("AntiKtVR30Rmax4Rmin02TrackJets"),
  m_containTruthParticles(false),
  m_containTruthNeutrinos(false),
  m_containTruthBosonWDP(false),
  m_containTruthTopWDP(false),
  m_containTruthBSMWDP(false),
  m_containTruthEvents(false),
  m_containTruthTaus(false),
  m_containTruthVertices(false),
  m_containBornLeptons(false)
{

  Info("xAODEvtLooper","Creating xAODEvtLooper class");

}	
// ----------------------------------------------------------------------------------------------- //
EL::StatusCode xAODEvtLooper::changeInput (bool firstFile)
{ 
  
  (void)firstFile;
  return EL::StatusCode::SUCCESS;

}
// ----------------------------------------------------------------------------------------------- //
EL::StatusCode xAODEvtLooper::setupJob (EL::Job& job)
{

  job.useXAOD ();

  xAOD::Init( "xAODEvtLooper" ).ignore(); 

  // Streams to save output information
  EL::OutputStream skimOutput ("skim");
  EL::OutputStream treeOutput ("tree");
  job.outputAdd (skimOutput);
  job.outputAdd (treeOutput);

  return EL::StatusCode::SUCCESS;

}
// ----------------------------------------------------------------------------------------------- //
EL::StatusCode xAODEvtLooper::initialize ()
{

  // 
  const char* APP_NAME = "xAODEvtLooper";
  CHECK( PkgDiscovery::Init( m_deepConfig, m_selectorName ) );

  // Prevent upload of file access statistics (which is buggy and hangs in 2.4.21)
  xAOD::TFileAccessTracer::enableDataSubmission( false );

  const xAOD::EventInfo* eventInfo = 0;

  m_dir = gDirectory;
  
  // Please don't move
  m_event = wk()->xaodEvent();

  // Retrieve Event info to get dsid
  CHECK( m_event->retrieve( eventInfo, "EventInfo") );

  // Running over data
  m_isData = !eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ); 

  // Tools
  m_configMgr  = new ConfigMgr();
  m_store      = new xAOD::TStore();
  // Create all the tools, such that we can configure them before initializing them
  CHECK( m_configMgr->create_xAOD_tools() );

  // 999 is our default DSID for data; in MC this will immediately be overwritten
  int channelNumber = 999;

  // Dsid and runNumber is needed in order to label sumOfWeights bin or to fill CBKTree
  if ( m_isData ) {
    m_configMgr->objectTools->m_eventObject->setRunNumber(eventInfo->runNumber());
    m_configMgr->objectTools->m_eventObject->setDSID(999);
  }
  else {
    channelNumber = eventInfo->mcChannelNumber();
    if( channelNumber<=0 ) channelNumber = eventInfo->runNumber();
    m_configMgr->objectTools->m_eventObject->setDSID(channelNumber);
    m_configMgr->objectTools->m_eventObject->setRunNumber(eventInfo->runNumber());
  }

  // Are fat jets are going be processed?
  // Is so, check if the file contains the fat jet container. 
  CentralDB::retrieve(CentralDBFields::DOFATJET, m_doFatjet);
  CentralDB::retrieve(CentralDBFields::FATJETCONTAINERNAME, m_fatjetContainerName);
  CentralDB::retrieve(CentralDBFields::TRUTHFATJETCONTAINERNAME, m_truthFatjetContainerName);
  if (m_doFatjet) m_doFatjet = checkJetContainer(m_isTruthOnly ? m_truthFatjetContainerName : m_fatjetContainerName);
  if(!m_doFatjet) Info("initialize()", "m_doFatjet is false. Will not try to retrieve fat jets.");

  // Are track jets are going be processed?
  // Is so, check if the file contains the track jet container. 
  CentralDB::retrieve(CentralDBFields::DOTRACKJET, m_doTrackjet);
  CentralDB::retrieve(CentralDBFields::TRACKJETCONTAINERNAME, m_trackjetContainerName);
  if (m_doTrackjet) m_doTrackjet = checkJetContainer(m_trackjetContainerName);  
  if(!m_doTrackjet) Info("initialize()", "m_doTrackjet is false. Will not try to retrieve track jets.");

  // Does the file have designated truth jets collection? 
  // Fallback to AntiKt4TruthDressedWZJets -> AntiKt4TruthJets if not (or not specified).
  CentralDB::retrieve(CentralDBFields::TRUTHJETCONTAINERNAME, m_truthJetContainerName);
  if(!checkJetContainer(m_truthJetContainerName)){
    Warning("initialize()", "%s doesn't seem to exist in the file! Please review the name of the truth jet collection in your deep config (key name: TruthjetContainerName).", m_truthJetContainerName);
    
    bool foundFallback=false;
    for(std::string trJetCont_fallback : {"AntiKt4TruthDressedWZJets","AntiKt4TruthJets"}){
      if(m_truthJetContainerName==trJetCont_fallback) continue;
      Info("initialize()", "Trying to fall back to %s", trJetCont_fallback);
      if(checkJetContainer(trJetCont_fallback)){
	Info("initialize()", "Found %s! Use this container as TruthJets", trJetCont_fallback);
	m_truthJetContainerName =  trJetCont_fallback;
	foundFallback = true;
	break;
      }
    }
    if(!foundFallback){
      Warning("initialize()", "No available TruthJetContainer found. Will not try to retrieve in the loop.");
      m_truthJetContainerName = "";
    }
  }
  
  // Does the file has truth events container?
  m_containTruthEvents = checkTruthEventContainer("TruthEvents");
  Info("initialize()", "Does the file has TruthEvents? %i", (int)m_containTruthEvents);

  // Does the file has truth particle containers? 
  m_containTruthParticles = checkTruthParticleContainer("TruthParticles");
  Info("initialize()", "Does the file has TruthParticles? %i", (int)m_containTruthParticles);

  // Does the file has truth particle containers? 
  m_containTruthVertices = checkTruthVertexContainer("TruthPrimaryVertices");
  Info("initialize()", "Does the file has TruthVertices? %i", (int)m_containTruthVertices);
  
  // Does the file has truth particle containers? 
  m_containBornLeptons = checkTruthParticleContainer("BornLeptons");
  Info("initialize()", "Does the file has BornLeptons? %i", (int)m_containBornLeptons);

  m_containTruthNeutrinos = checkTruthParticleContainer("TruthNeutrinos");
  Info("initialize()", "Does the file has TruthNeutrinos? %i", (int)m_containTruthNeutrinos);

  m_containTruthBosonWDP = checkTruthParticleContainer("TruthBosonsWithDecayParticles");
  Info("initialize()", "Does the file has TruthBosonsWithDecayParticles? %i", (int)m_containTruthBosonWDP);

  m_containTruthTopWDP = checkTruthParticleContainer("TruthTopWithDecayParticles");
  Info("initialize()", "Does the file has TruthTopWithDecayParticles? %i", (int)m_containTruthTopWDP);

  m_containTruthBSMWDP = checkTruthParticleContainer("TruthBSMWithDecayParticles");
  Info("initialize()", "Does the file has TruthBSMWithDecayParticles? %i", (int)m_containTruthBSMWDP);

  m_containTruthTaus = checkTruthParticleContainer("TruthTaus");
  Info("initialize()", "Does the file has TruthTaus? %i", (int)m_containTruthTaus);

  // This setups the entire job, and initializes Objects,ObjectTools, TreeMaker and SUSYTools
  if( m_configMgr->init_xAOD_running(  wk()->getOutputFile ("skim"),
                                       wk()->xaodEvent(),
                                       m_nEvents,m_writeSkims,
                                       m_writeTrees,m_isData,
                                       m_isAf2,m_sysSetName,
                                       m_isTruthOnly,
				       channelNumber,
				       m_doFatjet,
				       m_doTrackjet) != StatusCode::SUCCESS ){

    return EL::StatusCode::FAILURE;
    
  } 

  // Now fill the eventCounter histogram for original SOW and nEvents for this file
  m_configMgr->fillEvtCounterOrigSOW(m_fileTotalSOW);
  m_configMgr->fillEvtCounterOrigNEvents(m_fileTotalNEvents);

  // New method: store in tree
  const xAOD::CutBookkeeperContainer* completeCBC = 0;
  if(!m_noCutBookkeepers){
    if( !m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess() ){
      Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
      return EL::StatusCode::FAILURE;
    }
    m_configMgr->fillCBKTree(completeCBC, wk()->inputFile()->GetName());
  }

  if( m_selectorName != "" && m_writeTrees<=0 ){
    MsgLog::INFO("xAODEvtLooper::initialize","A selector was provided with writeTrees %i. Turning on writeTrees==1",m_writeTrees);
    m_writeTrees = 1;
  }

  if(m_writeTrees>0){

    std::cout<<"m_writeTrees > 0, starting baseuser"<<std::endl;
    
    // Retrieve the selector the user wishes to run
    m_baseUser = getAnalysisSelector(m_selectorName);
    m_baseUser->setSampleName(m_sampleName);

    TFile* treeFile = wk()->getOutputFile("tree");

    // Associate histograms to the tree stream
    m_configMgr->histMaker->addOutputFile(treeFile);

    // Tree maker setup
    m_configMgr->treeMaker->setStreamState("tree");
    m_configMgr->treeMaker->createTrees(treeFile ,"tree");

    // Run user define setup
    m_baseUser->setup( m_configMgr );
    m_baseUser->setTruthOnly(m_isTruthOnly);
    m_baseUser->setDisableCuts(m_disableCuts);

    // Override the users configuration
    if( m_isTruthOnly ){

      Info("initialize()","Configuring truth only overlap removal");

      // Do custom overlap removal for truth
      m_configMgr->obj->setMinDrMuJet(0.4);
      m_configMgr->obj->setMinDrJetEle(0.2);
      m_configMgr->obj->setMinDrEleJet(0.4);
      m_configMgr->obj->doUserOR(1);

    }

    // Add all weight systematics to the nominal ("") tree, if running over systematics 
    // Note: this must come after users setup, since they can configure the Systematics class
    m_configMgr->systematics->addSysBranches(m_configMgr->treeMaker,"");

  }

  // Basic information, required to be in all trees
  // Mostly needed when running SampleHandler::merge(...)
  m_configMgr->treeMaker->addDefaultVariables();

  // After user setup, print cuts applied to objects
  m_configMgr->obj->print( Objects::GRID );

  m_dir->cd();

  // Read in option here to avoid that error messages come up every event in case the
  // associated field is not set in the deepConfig
  CentralDB::retrieve(CentralDBFields::ADDMETMUONSINVIS, m_add_MET_with_muons_invisible);
  CentralDB::retrieve(CentralDBFields::ADDMETELECTRONSINVIS, m_add_MET_with_electrons_invisible);
  CentralDB::retrieve(CentralDBFields::ADDMETPHOTONSINVIS, m_add_MET_with_photons_invisible);  
  CentralDB::retrieve(CentralDBFields::ADDMETLEPTONSINVIS, m_add_MET_with_leptons_invisible);
  CentralDB::retrieve(CentralDBFields::ADDMETLOOSEWP, m_add_loose_MET_WP);
  CentralDB::retrieve(CentralDBFields::ADDMETTIGHTERWP, m_add_tighter_MET_WP );
  CentralDB::retrieve(CentralDBFields::ADDMETTENACIOUSWP, m_add_tenacious_MET_WP);
  CentralDB::retrieve(CentralDBFields::EMULATEAUXTRIGGERS, m_emulateAuxTriggers);
  CentralDB::retrieve(CentralDBFields::SAVETAUS, m_saveTaus);
  CentralDB::retrieve(CentralDBFields::DOPHOTON, m_doPhoton);

  return EL::StatusCode::SUCCESS;

}
// ----------------------------------------------------------------------------------------------- //
EL::StatusCode xAODEvtLooper::fileExecute () {

  /* 
     Using instructions from
     https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisMetadata#Event_Bookkeepers
  */

  //
  const char* APP_NAME = "xAODEvtLooper";
  CHECK( PkgDiscovery::Init( m_deepConfig, m_selectorName ) );

  m_event = wk()->xaodEvent();

  if( m_isTruthOnly ){
    Info("fileExecute","Runnning in truth only mode!");
  }
  
  // Try to take from deep config if not specified by command-line
  if( m_derivationName=="" )
    CentralDB::retrieve(CentralDBFields::DXAODKERNEL,m_derivationName);

  if( m_derivationName=="" ){
    Error("fileExecute","No DxAOD kernel name, must be specified in the steering files!!");
    return EL::StatusCode::FAILURE;    
  }
  else{
    Info("fileExecute","Running with %s DxAOD Kernel",m_derivationName.Data() );
  }

  TTree *CollectionTree = dynamic_cast<TTree*>(wk()->inputFile()->Get("CollectionTree"));

  TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (!MetaData) {
    Error("fileExecute()", "MetaData not found! Exiting.");
    return EL::StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);


  //check if file is from a DxAOD
  m_isDerivation = !MetaData->GetBranch("StreamAOD");

  // Check if the file contains the LHE weight
  bool containsLHE3Weights = false;

  if (m_noCutBookkeepers) {

  	  // use nEvents from tree size and calculate sum-of-weights on the fly (later)
  	  m_fileTotalNEvents = CollectionTree->GetEntries(); //m_nEvents;
  	  m_fileTotalSOW = 0;
  	  m_fileDerivationNEvents = m_fileTotalNEvents; //m_nEvents;
  	  m_fileDerivationSOW = 0;
  	  if (m_fileDerivationNEvents > 0) {
  		m_sumOfWeightsPerEvent = m_fileTotalSOW/m_fileDerivationNEvents;
  	  } else {
  		m_sumOfWeightsPerEvent = 0;
  	  }

  } else {
	  // check for corruption
    /**
	  const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
	  if( !m_event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
		Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
		return EL::StatusCode::FAILURE;
	  }else{
		if ( incompleteCBC->size() != 0 ) {
		  Error("initializeEvent()","Found incomplete Bookkeepers! Check file for corruption.");
		  //return EL::StatusCode::FAILURE;
		}
	  }
    */
	  // Now, let's find the actual information
	  const xAOD::CutBookkeeperContainer* completeCBC = 0;
	  if( !m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess() ){
		Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
		return EL::StatusCode::FAILURE;
	  }

	  const xAOD::CutBookkeeper* allEventsCBK = 0;
	  int maxCycle = -1;
	  for (const auto& cbk: *completeCBC) {

		TString inputStream = cbk->inputStream();

		if( m_isTruthOnly ){
		  if( cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && inputStream.Contains("TRUTH") ){
			Info("fileExecute()","Successfully got allEventsCBK");
			allEventsCBK = cbk;
			maxCycle = cbk->cycle();
			break;
		  }
		}

		if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
		  allEventsCBK = cbk;
		  maxCycle = cbk->cycle();
		}
	  }

	  m_fileTotalNEvents = 0;
	  m_fileTotalSOW = 0;
	  m_fileDerivationNEvents = 0;
	  m_fileDerivationSOW = 0;
	  m_sumOfWeightsPerEvent = 0;

	  // For derivation
	  if(m_isDerivation ) {
	    
	    Info("fileExecute()", "This file is a derivation");

	    // Print all the cbk and see if LHE3 weights are stored
	    for ( auto cbk :  *completeCBC ) {
	      if( (TString(cbk->name())).BeginsWith("LHE3Weight_") ) containsLHE3Weights = true;
	    }
	    
	    // Find Derivation cbk
	    const xAOD::CutBookkeeper* derivationCBK=0;
	    for ( auto cbk :  *completeCBC ) {

	      if( m_isTruthOnly ){
		if (cbk->name() == "AllExecutedEvents") {
		  derivationCBK = cbk;
		  Info("fileExecute()","Successfully got derivationCBK");
		  break;
		}
	      }

	      if (cbk->name() == m_derivationName) {
		derivationCBK = cbk;
	      }
	    }
	    
	    if (allEventsCBK && derivationCBK) {
	      m_fileTotalNEvents = allEventsCBK->nAcceptedEvents();
	      m_fileTotalSOW = allEventsCBK->sumOfEventWeights();
	      m_fileDerivationNEvents = derivationCBK->nAcceptedEvents();
	      m_fileDerivationSOW = derivationCBK->sumOfEventWeights();
	      if (m_fileDerivationNEvents > 0) {
		m_sumOfWeightsPerEvent = m_fileTotalSOW/m_fileDerivationNEvents;
	      } else {
		m_sumOfWeightsPerEvent = 0;
	      }
	    }
	    else if (!derivationCBK) {
	      Error("fileExecute()","Could not retrieve the derivationCBK. DAODKernelName provided: %s while this file has:", m_derivationName.Data());
	      for ( auto cbk :  *completeCBC ) std::cout << cbk->name() << std::endl;
	      Error("fileExecute()","Probably this is inconsistent with the input derivation type. Check the the deep config or command-line option!");
	      return EL::StatusCode::FAILURE;
	    }
	    else{
	      Error("fileExecute()","Error getting metadata for normalization. Could not retrieve the allEventsCBK.");
	      return EL::StatusCode::FAILURE;
	    }	    
	    
	    // For running on full AOD
	  } else {
	    
	    Info("fileExecute()", "This file is no derivation");
	    
	    if (allEventsCBK) {
	      m_fileTotalNEvents = allEventsCBK->nAcceptedEvents();
	      m_fileTotalSOW = allEventsCBK->sumOfEventWeights();
	      m_fileDerivationNEvents = m_fileTotalNEvents;
	      m_fileDerivationSOW = m_fileTotalSOW;
	      if (m_fileDerivationNEvents > 0) {
		m_sumOfWeightsPerEvent = m_fileTotalSOW/m_fileDerivationNEvents;
	      }
	    }
	    else{
	      Error("fileExecute()","Error getting metadata for normalization");
	      return EL::StatusCode::FAILURE;
	    }
	    
	  }
  }
  
  
  Info("fileExecute()", "Total NEvents in original file:      %.0f", m_fileTotalNEvents);
  Info("fileExecute()", "Total NEvents in this file:          %.0f", m_fileDerivationNEvents);
  Info("fileExecute()", "Total sumOfWeights in original file: %.2f", m_fileTotalSOW);
  Info("fileExecute()", "Total sumOfWeights in this file:     %.2f", m_fileDerivationSOW);
  Info("fileExecute()", "SumOfWeights per event:              %.2f", m_sumOfWeightsPerEvent);

  // If this is not the first file e.g. configMgr is already created by initialize()
  // then set the sumOfWeightsPerEvent and fill the eventCounter histogram
  if (m_configMgr) {
    m_configMgr->setDerivName(m_derivationName);

    // Turn off if the LHE weights are not found in the file or it is a data file
    // if(!containsLHE3Weights || m_isData){
    if(m_isData){
      m_configMgr->treeMaker->setWriteLHEWeights(false);
      m_configMgr->objectTools->setWriteLHEWeights(false);
    }
    m_configMgr->objectTools->m_eventObject->setSOWPerEvent(m_sumOfWeightsPerEvent);
    m_configMgr->fillEvtCounterOrigSOW(m_fileTotalSOW);
    m_configMgr->fillEvtCounterOrigNEvents(m_fileTotalNEvents);

    // New method: store in tree
    if( !m_noCutBookkeepers ){
    const xAOD::CutBookkeeperContainer* completeCBC = 0;
    if( !m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess() ){
      Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
      return EL::StatusCode::FAILURE;
    }
    m_configMgr->fillCBKTree(completeCBC, wk()->inputFile()->GetName());
    }
  }

  // Find the number of events to process
  //    If --MaxEvent is not set, set greater than #events in the file or next file is being processed, use the #events in the file
  if(m_nProcEvents==-1 || m_nProcEvents>m_fileDerivationNEvents || m_ievent!=0) m_nProcEvents = m_fileDerivationNEvents;
  
  // Reset the counter & stopwatch
  m_ievent = 0;
  m_clock_fileStart = clock();

  return EL::StatusCode::SUCCESS;

}
// ----------------------------------------------------------------------------------------------- //
EL::StatusCode xAODEvtLooper::execute ()
{

  Timer::Instance()->Start( "xAODEvtLooper::execute" );

  const char* APP_NAME = "xAODEvtLooper";

  // Increment the event counter
  m_ievent++;

  // Shout regularly to avoid being lonely
  if(m_ievent%100==0){
    // Time lapse & ETA
    reportLapse(m_ievent, m_nProcEvents, m_clock_fileStart);  

    // Mem usage
    double vm_usage=0, resident_set=0;
    getMemUsage(vm_usage, resident_set);
    Info("execute()", "  Mem usage: VSS=%i, RSS=%i (MB)", int(vm_usage), int(resident_set) ); // round to int
  }
  // Run on truth only samples?
  if (m_isTruthOnly) {
    return executeTruthOnly();
  }
  // Reco information
  const xAOD::EventInfo* eventInfo          = 0;
  const xAOD::VertexContainer* primVertex   = 0;	 

  // Truth information
  const xAOD::TruthParticleContainer* truthParticles = 0;
  const xAOD::TruthParticleContainer* truthElectrons = 0;
  const xAOD::TruthParticleContainer* truthMuons     = 0;
  const xAOD::TruthParticleContainer* truthTaus      = 0;
  const xAOD::TruthParticleContainer* truthPhotons   = 0;
  const xAOD::TruthParticleContainer* truthNeutrinos = 0;
  const xAOD::TruthParticleContainer* truthBoson     = 0;
  const xAOD::TruthParticleContainer* truthTop       = 0;
  const xAOD::TruthParticleContainer* truthBSM       = 0;
  const xAOD::JetContainer* truthJets                = 0;
  const xAOD::JetContainer* truthFatjets             = 0;
  const xAOD::MissingETContainer* truthMET           = 0;
  const xAOD::TruthEventContainer* truthEvents       = 0;
  const xAOD::TruthVertexContainer*   truthVertices  = 0;
  const xAOD::TruthParticleContainer* truthBornLep   = 0;

  // Event info
  CHECK( m_event->retrieve( eventInfo, "EventInfo") );
  CHECK( m_event->retrieve( primVertex, "PrimaryVertices" ) );
  /**
  if(!(std::count(check_even.begin(), check_even.end(), eventInfo->eventNumber()))){
    return EL::StatusCode::SUCCESS;
  }
  
  std::cout<<"Found event "<<eventInfo->eventNumber()<<std::endl;
  */
  // Truth information
  if ( eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) ){

    if(m_containTruthVertices) ANA_CHECK( m_event->retrieve( truthVertices, "TruthPrimaryVertices" )  );

    if(m_containBornLeptons) ANA_CHECK( m_event->retrieve( truthBornLep      , "BornLeptons"  ));

    if(m_containTruthParticles) CHECK( m_event->retrieve( truthParticles, "TruthParticles" )  );

    if(m_containTruthTaus) CHECK( m_event->retrieve( truthTaus, "TruthTaus" )  );

    if(m_containTruthEvents) CHECK( m_event->retrieve( truthEvents , "TruthEvents" )  );

    if( m_configMgr->objectTools->m_truthObject->saveTruthEvtInfoFull() ){ // in case you want to store the full decay chain

      // EXOT5 derivations have dedicated truth containers
      if (m_derivationName.Contains("EXOT5")){
        CHECK( m_event->retrieve( truthElectrons, "EXOT5TruthElectrons" )  );
        CHECK( m_event->retrieve( truthMuons, "EXOT5TruthMuons" )  );
      }
      else{
        CHECK( m_event->retrieve( truthElectrons, "TruthElectrons" )  );
        CHECK( m_event->retrieve( truthMuons, "TruthMuons" )  );
      }
      
      //CHECK( m_event->retrieve( truthTaus, "TruthTaus" )  );
      CHECK( m_event->retrieve( truthPhotons, "TruthPhotons" )  );
      
      if(m_containTruthNeutrinos) CHECK( m_event->retrieve( truthNeutrinos, "TruthNeutrinos"              ));
      if(m_containTruthBosonWDP)  CHECK( m_event->retrieve( truthBoson    , "TruthBosonsWithDecayParticles"));  
      if(m_containTruthTopWDP)    CHECK( m_event->retrieve( truthTop      , "TruthTopWithDecayParticles"  ));
      if(m_containTruthBSMWDP)    CHECK( m_event->retrieve( truthBSM      , "TruthBSMWithDecayParticles"  ));

    }
    CHECK( m_event->retrieve( truthJets, m_truthJetContainerName )  );
    if(m_doFatjet) CHECK( m_event->retrieve(truthFatjets  , m_truthFatjetContainerName) ); 
    CHECK( m_event->retrieve( truthMET, "MET_Truth" ) );
  }

  // Clear all physics containers
  // Note this clears the object maps, so doesn't need 
  // to be called for each systematic, only each event
  // Requirements:
  //   - Should be called before event filter, as we fill an event counter histogram
  m_configMgr->Reset_perEvt();

  // Only needs to be called once per event. Doesn't depend on systematics
  m_configMgr->objectTools->LoadEvtTriggers();

  // Check to see if we pass truth level filtering
  if( !m_configMgr->objectTools->passEventFiltering(truthParticles,eventInfo,primVertex) ) return EL::StatusCode::SUCCESS;

  // Try and optimize on retriveing our objects
  // Lets get the nominal first
  xAOD::MuonContainer* muons_nominal(0);
  xAOD::ShallowAuxContainer* muons_nominal_aux(0);
  xAOD::ElectronContainer* electrons_nominal(0);
  xAOD::ShallowAuxContainer* electrons_nominal_aux(0);
  xAOD::JetContainer* jets_nominal(0);
  xAOD::ShallowAuxContainer* jets_nominal_aux(0);
  xAOD::JetContainer* fatjets_nominal(0);
  xAOD::ShallowAuxContainer* fatjets_nominal_aux(0);
  xAOD::JetContainer* trackjets_nominal(0);
  xAOD::ShallowAuxContainer* trackjets_nominal_aux(0);
  xAOD::TauJetContainer* taus_nominal(0);
  xAOD::ShallowAuxContainer* taus_nominal_aux(0);
  xAOD::PhotonContainer* photons_nominal(0);
  xAOD::ShallowAuxContainer* photons_nominal_aux(0);

  //  Apply PRW in SUSYTools, since the CP tools depend upon them
  //  Note this is only used within SUSYTools, we are using our own 
  //  version of PRW tool to get the weights themselves
  CHECK( m_configMgr->susyObj.ApplyPRWTool() );
  CHECK( m_configMgr->susyObj.GetMuons(muons_nominal,muons_nominal_aux) );
  CHECK( m_configMgr->susyObj.GetElectrons(electrons_nominal,electrons_nominal_aux) );
  
  CHECK( m_configMgr->susyObj.GetJets(jets_nominal,jets_nominal_aux) );
 
  if(m_doPhoton  ) CHECK( m_configMgr->susyObj.GetPhotons(photons_nominal, photons_nominal_aux) );
  if(m_doFatjet  ) CHECK( m_configMgr->susyObj.GetFatJets(fatjets_nominal,fatjets_nominal_aux,true,m_fatjetContainerName,false) ); // recordSG, doLargeRdecorations = true, false
  if(m_doTrackjet) CHECK( m_configMgr->susyObj.GetTrackJets(trackjets_nominal,trackjets_nominal_aux,true,m_trackjetContainerName) );
  if( m_saveTaus ) CHECK( m_configMgr->susyObj.GetTaus(taus_nominal,taus_nominal_aux) );
  
  // Perform trigger emulations. Maybe this should go elsewhere?
  // Check tool exists first, since in truth only mode, 
  // it is not created 
  if( m_emulateAuxTriggers && m_configMgr->objectTools->m_trigTool ){
    CHECK( m_configMgr->objectTools->m_trigTool->emulateMETTriggers() );
    CHECK( m_configMgr->objectTools->m_trigTool->emulateJetTriggers() );
    CHECK( m_configMgr->objectTools->m_trigTool->emulateHiggsinoTriggers() );
  }
  
  // Begin loop over kinematic based systematics

  //std::cout<<"Number of systematics = "<<m_configMgr->systematics->getKinematicSystematics().size()<<std::endl;
  
  for (const auto& sysItr : m_configMgr->systematics->getKinematicSystematics() ){

    // Full systematic name
    std::string sys_name = sysItr.systset.name();

    // 
    Timer::Instance()->Start( "xAODEvtLooper::executePerSys%s", sys_name.c_str() );

    // Reset all tools which depend on systematics
    m_configMgr->Reset_perSys();

    // Configure all ConfigMgr tools to process this systematic
    m_configMgr->setSysState(sys_name);
    
    // !! Method is in testing and not actually used right now !!
    m_configMgr->setSystematicSet(sysItr.systset);

    // This is our local systematic copy
    xAOD::ElectronContainer* electrons(electrons_nominal);
    xAOD::MuonContainer* muons(muons_nominal);
    xAOD::JetContainer* jets(jets_nominal);
    xAOD::JetContainer* fatjets(fatjets_nominal);
    xAOD::JetContainer* trackjets(trackjets_nominal);
    xAOD::TauJetContainer* taus(taus_nominal);
    xAOD::PhotonContainer* photons(photons_nominal);

    xAOD::MissingETContainer* met = new xAOD::MissingETContainer;
    xAOD::MissingETAuxContainer* met_aux = new xAOD::MissingETAuxContainer;
    met->setStore(met_aux);
    met->reserve(10);

    // The different MET WPs (standard MET)
    // Loose
    xAOD::MissingETContainer* met_loose = 0;
    xAOD::MissingETAuxContainer* met_loose_aux = 0; 
    if( m_add_loose_MET_WP ){
      met_loose = new xAOD::MissingETContainer;
      met_loose_aux = new xAOD::MissingETAuxContainer;
      met_loose->setStore(met_loose_aux);
      met_loose->reserve(10);
    }
    // Tighter
    xAOD::MissingETContainer* met_tighter = 0;
    xAOD::MissingETAuxContainer* met_tighter_aux = 0;
    if( m_add_tighter_MET_WP ){
      met_tighter = new xAOD::MissingETContainer;
      met_tighter_aux = new xAOD::MissingETAuxContainer;
      met_tighter->setStore(met_tighter_aux);
      met_tighter->reserve(10);
    }
    // Tenacious
    xAOD::MissingETContainer* met_tenacious = 0;
    xAOD::MissingETAuxContainer* met_tenacious_aux = 0;
    if( m_add_tenacious_MET_WP ){
      met_tenacious = new xAOD::MissingETContainer;
      met_tenacious_aux = new xAOD::MissingETAuxContainer;
      met_tenacious->setStore(met_tenacious_aux);
      met_tenacious->reserve(10);
    }
    

    xAOD::MissingETContainer* metTrack = new xAOD::MissingETContainer;
    xAOD::MissingETAuxContainer* metTrack_aux = new xAOD::MissingETAuxContainer;
    metTrack->setStore(metTrack_aux);
    metTrack->reserve(10);

    // The different MET WPs (treck MET)
    // Loose
    xAOD::MissingETContainer* metTrack_loose = 0;
    xAOD::MissingETAuxContainer* metTrack_loose_aux = 0; 
    if( m_add_loose_MET_WP ){
      metTrack_loose = new xAOD::MissingETContainer;
      metTrack_loose_aux = new xAOD::MissingETAuxContainer;
      metTrack_loose->setStore(metTrack_loose_aux);
      metTrack_loose->reserve(10);
    }
    // Tighter
    xAOD::MissingETContainer* metTrack_tighter = 0;
    xAOD::MissingETAuxContainer* metTrack_tighter_aux = 0;
    if( m_add_tighter_MET_WP ){
      metTrack_tighter = new xAOD::MissingETContainer;
      metTrack_tighter_aux = new xAOD::MissingETAuxContainer;
      metTrack_tighter->setStore(metTrack_tighter_aux);
      metTrack_tighter->reserve(10);
    }
    // Tenacious
    xAOD::MissingETContainer* metTrack_tenacious = 0;
    xAOD::MissingETAuxContainer* metTrack_tenacious_aux = 0;
    if( m_add_tenacious_MET_WP ){
      metTrack_tenacious = new xAOD::MissingETContainer;
      metTrack_tenacious_aux = new xAOD::MissingETAuxContainer;
      metTrack_tenacious->setStore(metTrack_tenacious_aux);
      metTrack_tenacious->reserve(10);
    }

    xAOD::MissingETContainer* met_muon_invis = 0;
    xAOD::MissingETAuxContainer* met_muon_invis_aux = 0;
    if( m_add_MET_with_muons_invisible ){
      met_muon_invis = new xAOD::MissingETContainer;;
      met_muon_invis_aux = new xAOD::MissingETAuxContainer;
      met_muon_invis->setStore(met_muon_invis_aux);
      met_muon_invis->reserve(10);
    }

    xAOD::MissingETContainer* met_electron_invis = 0;
    xAOD::MissingETAuxContainer* met_electron_invis_aux = 0;
    if( m_add_MET_with_electrons_invisible ){
      met_electron_invis = new xAOD::MissingETContainer;;
      met_electron_invis_aux = new xAOD::MissingETAuxContainer;
      met_electron_invis->setStore(met_electron_invis_aux);
      met_electron_invis->reserve(10);
    }

    xAOD::MissingETContainer* met_photon_invis = 0;
    xAOD::MissingETAuxContainer* met_photon_invis_aux = 0;
    if( m_add_MET_with_photons_invisible ){
      met_photon_invis = new xAOD::MissingETContainer;;
      met_photon_invis_aux = new xAOD::MissingETAuxContainer;
      met_photon_invis->setStore(met_photon_invis_aux);
      met_photon_invis->reserve(10);
    }

    xAOD::MissingETContainer* met_lepton_invis = 0;
    xAOD::MissingETAuxContainer* met_lepton_invis_aux = 0;
    if( m_add_MET_with_leptons_invisible ){
      met_lepton_invis = new xAOD::MissingETContainer;;
      met_lepton_invis_aux = new xAOD::MissingETAuxContainer;
      met_lepton_invis->setStore(met_lepton_invis_aux);
      met_lepton_invis->reserve(10);
    }


    // Set systematic state for xAOD objects
    if( sys_name != "" ){
      if( m_configMgr->susyObj.applySystematicVariation(sysItr.systset) != EL::StatusCode::SUCCESS ){
        std::cout << "<xAODEvtLooper::execute> FATAL Cannot configure SUSYTools for systematic variation: " << sys_name << std::endl;
        abort();
      }
    }

    // Retrieve electrons only when affected
    if( ST::testAffectsObject(xAOD::Type::Electron,sysItr.affectsType) ){
      xAOD::ElectronContainer* electrons_syst(0);
      xAOD::ShallowAuxContainer* electrons_syst_aux(0);
      //
      CHECK( m_configMgr->susyObj.GetElectrons(electrons_syst,electrons_syst_aux) );
      //
      electrons      = electrons_syst;
    }

    // Retrieve muons only when affected
    if( ST::testAffectsObject(xAOD::Type::Muon, sysItr.affectsType) ){
      xAOD::MuonContainer* muons_syst(0);
      xAOD::ShallowAuxContainer* muons_syst_aux(0);
      //
      CHECK( m_configMgr->susyObj.GetMuons(muons_syst,muons_syst_aux) );
      //
      muons       = muons_syst;
    }

    //    Retrieve taus only when affected
    if( m_saveTaus &&  ST::testAffectsObject(xAOD::Type::Tau, sysItr.affectsType) ){
      xAOD::TauJetContainer* taus_syst(0);
      xAOD::ShallowAuxContainer* taus_syst_aux(0);
      //
      CHECK( m_configMgr->susyObj.GetTaus(taus_syst,taus_syst_aux,true,"TauJets") );
      //
      taus        = taus_syst;
    }

    //    Retrieve photons only when affected
    if( ST::testAffectsObject(xAOD::Type::Photon, sysItr.affectsType) ){
      xAOD::PhotonContainer* photons_syst(0);
      xAOD::ShallowAuxContainer* photons_syst_aux(0);
      //
      CHECK( m_configMgr->susyObj.GetPhotons(photons_syst,photons_syst_aux) );
      //
      photons        = photons_syst;
    }
    
    // Retrieve jets only when affected
    if( ST::testAffectsObject(xAOD::Type::Jet, sysItr.affectsType) ){
      xAOD::JetContainer* jets_syst(0);
      xAOD::ShallowAuxContainer* jets_syst_aux(0);
      //
      CHECK( m_configMgr->susyObj.GetJets(jets_syst,jets_syst_aux) );
      //
      jets       = jets_syst;
      
      if( m_doFatjet ){
        //////// Fat jets
        xAOD::JetContainer* fatjets_syst(0);
        xAOD::ShallowAuxContainer* fatjets_syst_aux(0);
        //
        CHECK( m_configMgr->susyObj.GetFatJets(fatjets_syst,fatjets_syst_aux,true,m_fatjetContainerName ) );
        //
        fatjets       = fatjets_syst;
      } 
    }

    // Perform overlap removal
    m_configMgr->susyObj.OverlapRemoval( electrons, muons, jets, photons );

    // Add in index counters
    m_configMgr->objectTools->DecorateWithIdx(muons);
    m_configMgr->objectTools->DecorateWithIdx(electrons);
    m_configMgr->objectTools->DecorateWithIdx(taus);
    m_configMgr->objectTools->DecorateWithIdx(jets);
    if(fatjets)   m_configMgr->objectTools->DecorateWithIdx(fatjets);   // fatjets can be !=0 only when m_doFatjet=true
    if(trackjets) m_configMgr->objectTools->DecorateWithIdx(trackjets); // trackjets can be !=0 only when m_doTrackjet=true
    m_configMgr->objectTools->DecorateWithIdx(trackjets);
    m_configMgr->objectTools->DecorateWithIdx(photons);

    // Do weight systematics
    //   -- Requirements
    //        - requires signal leptons being classified, efficiency scale factors only for signal leptons 
    //        - requires jets to be b-tagged for flavor tagging systematics
    if( !m_isData ){
      m_configMgr->objectTools->doWeights(m_configMgr->susyObj,muons,electrons,taus,jets,trackjets,photons,m_configMgr->systematics->getWeightSystematics());
    }
       
    // Fill output skim containers
    m_configMgr->objectTools->get_xAOD_muons(muons,primVertex,eventInfo,m_configMgr->susyObj);
    m_configMgr->objectTools->get_xAOD_electrons(electrons,primVertex,eventInfo,m_configMgr->susyObj);
    m_configMgr->objectTools->get_xAOD_jets(jets,fatjets,trackjets,primVertex);
    m_configMgr->objectTools->get_xAOD_truth(truthParticles,truthElectrons,truthMuons,truthTaus,truthPhotons,truthNeutrinos,truthBoson,truthTop,truthBSM,truthBornLep,truthVertices,truthJets,truthFatjets,eventInfo);


    if(m_doPhoton ) m_configMgr->objectTools->get_xAOD_photons(photons);
    if(m_saveTaus ) m_configMgr->objectTools->get_xAOD_taus(taus,primVertex);

    // M.G. Tracks -- This is development only for now
    if( m_configMgr->objectTools->m_trackObject ){
      m_configMgr->objectTools->m_trackObject->fillTrackContainer(primVertex,eventInfo,truthParticles,sys_name);
    }

    // Note this only gets used if useNearbyLepIsoCorr is set to
    // true in the deep.config file!
    CHECK( m_configMgr->objectTools->applyNearbyLepIsoCorr(m_configMgr->susyObj,electrons,muons) );
    m_configMgr->objectTools->get_xAOD_muons_NearbyLepIsoCorr(muons);
    m_configMgr->objectTools->get_xAOD_electrons_NearbyLepIsoCorr(electrons);

    // Rebuild MET 
    m_configMgr->objectTools->get_xAOD_met(m_configMgr->susyObj,met,metTrack,met_lepton_invis,met_muon_invis,met_electron_invis,met_photon_invis,electrons,taus,muons,jets,photons,truthMET,MetVariable::MetFlavor::DEFAULT);
    
    // Get the various WPs ( no support for muon invis or lepton_invis MET)
    if( m_add_loose_MET_WP     )m_configMgr->objectTools->get_xAOD_met(m_configMgr->susyObjLoose    ,met_loose    ,metTrack_loose,0,0,0,0,electrons,taus,muons,jets,photons,truthMET,MetVariable::MetFlavor::LOOSE);
    if( m_add_tighter_MET_WP   )m_configMgr->objectTools->get_xAOD_met(m_configMgr->susyObjTighter  ,met_tighter  ,metTrack_tighter,0,0,0,0,electrons,taus,muons,jets,photons,truthMET,MetVariable::MetFlavor::TIGHTER);
    if( m_add_tenacious_MET_WP )m_configMgr->objectTools->get_xAOD_met(m_configMgr->susyObjTenacious,met_tenacious,metTrack_tenacious,0,0,0,0,electrons,taus,muons,jets,photons,truthMET,MetVariable::MetFlavor::TENACIOUS);

    // This should be the last container filled
    if( m_configMgr->objectTools->get_xAOD_event(m_configMgr->susyObj,
                                                 eventInfo,
                                                 truthEvents,
                                                 primVertex,
                                                 jets,
                                                 muons,
                                                 electrons,
						 photons )){
      // Do full chain analysis
      if(m_baseUser){
	
        Timer::Instance()->Start("UserAnalysisCode");

        // Get objects from internally stored objects
        m_configMgr->objectTools->get_objects(m_configMgr->obj);

        // Auto fill branches for all selectors         
        m_configMgr->treeMaker->setDefaultVariables(m_configMgr->obj);
        
        // Trigger analysis, as configured by the user in setup
        m_configMgr->doTriggerAna();

        // Perform users analysis
        m_baseUser->doAnalysis(m_configMgr);

        Timer::Instance()->End("UserAnalysisCode");

      }
      // Fill skim trees
      m_configMgr->treeMaker->Fill( TString(sys_name),"skim");
      
    }
    // Reset systematics to default set
    if( sys_name != "" ){
      if( m_configMgr->susyObj.resetSystematics() != EL::StatusCode::SUCCESS ){
        std::cout << "<xAODEvtLooper::execute> ERROR Cannot reset SUSYTools systematics" << std::endl;
        abort();
      }
    }
    delete met;
    delete met_aux;

    delete metTrack;
    delete metTrack_aux;

    if(met_loose){
      delete met_loose;
      delete met_loose_aux;
    }
    if(met_tighter){
      delete met_tighter;
      delete met_tighter_aux;
    }
    if(met_tenacious){
      delete met_tenacious;
      delete met_tenacious_aux;
    }

    if(metTrack_loose){
      delete metTrack_loose;
      delete metTrack_loose_aux; 
    }
    if(metTrack_tighter){
      delete metTrack_tighter;
      delete metTrack_tighter_aux;
    }
    if(metTrack_tenacious){
      delete metTrack_tenacious;
      delete metTrack_tenacious_aux;
    }
    
    if( met_muon_invis ){
      delete met_muon_invis;
      delete met_muon_invis_aux;
    }

    if( met_electron_invis ){
      delete met_electron_invis;
      delete met_electron_invis_aux;
    }

    if( met_photon_invis ){
      delete met_photon_invis;
      delete met_photon_invis_aux;
    }

    if( met_lepton_invis ){
      delete met_lepton_invis;
      delete met_lepton_invis_aux;
    }

    // 
    Timer::Instance()->End( "xAODEvtLooper::executePerSys%s", sys_name.c_str() );


  } /// Loop over systematics

  m_store->clear();

  //
  Timer::Instance()->End( "xAODEvtLooper::execute" );

  return EL::StatusCode::SUCCESS;

}
// ----------------------------------------------------------------------------------------------- //
EL::StatusCode xAODEvtLooper::executeTruthOnly ()
{

  const char* APP_NAME = "xAODEvtLooper";

  const xAOD::EventInfo* eventInfo = 0;
  const xAOD::TruthParticleContainer* truthParticles = 0;
  const xAOD::TruthParticleContainer* truthElectrons = 0;
  const xAOD::TruthParticleContainer* truthMuons     = 0;
  const xAOD::TruthParticleContainer* truthTaus      = 0;
  const xAOD::TruthParticleContainer* truthPhotons   = 0;
  const xAOD::TruthParticleContainer* truthNeutrinos = 0;
  const xAOD::TruthParticleContainer* truthBoson     = 0;
  const xAOD::TruthParticleContainer* truthTop       = 0;
  const xAOD::TruthParticleContainer* truthBSM       = 0;
  const xAOD::JetContainer* truthJets                = 0;
  const xAOD::JetContainer* truthFatjets             = 0;
  const xAOD::MissingETContainer* truthMET           = 0;
  const xAOD::TruthEventContainer* truthEvents       = 0;
  const xAOD::TruthVertexContainer*   truthVertices  = 0;
  const xAOD::TruthParticleContainer* truthBornLep   = 0;

  if(m_containTruthVertices) ANA_CHECK( m_event->retrieve( truthVertices, "TruthPrimaryVertices" )  );
  
  if(m_containBornLeptons) ANA_CHECK( m_event->retrieve( truthBornLep      , "BornLeptons"  ));
  

  if(m_containTruthParticles) CHECK( m_event->retrieve( truthParticles, "TruthParticles" )  );
  
  // EXOT5 derivations have dedicated truth containers
  if (m_derivationName.Contains("EXOT5")){
    CHECK( m_event->retrieve( truthElectrons, "EXOT5TruthElectrons" )  );
    CHECK( m_event->retrieve( truthMuons, "EXOT5TruthMuons" )  );
  }
  else{
    CHECK( m_event->retrieve( truthElectrons, "TruthElectrons" )  );
    CHECK( m_event->retrieve( truthMuons, "TruthMuons" )  );
  }  
  
  CHECK( m_event->retrieve(truthTaus     , "TruthTaus")        );
  CHECK( m_event->retrieve(truthPhotons  , "TruthPhotons")     );
  if(m_containTruthNeutrinos) CHECK( m_event->retrieve( truthNeutrinos, "TruthNeutrinos"              ));
  if(m_containTruthBosonWDP)  CHECK( m_event->retrieve( truthBoson    , "TruthBosonsWithDecayParticles"));  
  if(m_containTruthTopWDP)    CHECK( m_event->retrieve( truthTop      , "TruthTopWithDecayParticles"  ));
  if(m_containTruthBSMWDP)    CHECK( m_event->retrieve( truthBSM      , "TruthBSMWithDecayParticles"  ));
  CHECK( m_event->retrieve(truthJets     , m_truthJetContainerName) ); // Where did the standard truth jet container go in TRUTH1?
  if(m_doFatjet) CHECK( m_event->retrieve(truthFatjets  , m_truthFatjetContainerName) ); 
  CHECK( m_event->retrieve(truthMET      , "MET_Truth")        );
  CHECK( m_event->retrieve(truthEvents   , "TruthEvents")      );
  CHECK( m_event->retrieve(eventInfo     , "EventInfo")        );


  // if not using cut bookkeepers: fill "sum of weights" histogram per event...
  if (m_noCutBookkeepers) {
	  bool isSimulation = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);
	  float mcEvtWeight = 1.0;
	  if(isSimulation) {
	    mcEvtWeight = eventInfo->mcEventWeight();
	  }
	m_configMgr->fillEvtCounterOrigSOW(mcEvtWeight);
  }

  // work with shallow copies
  std::pair< xAOD::TruthParticleContainer*,xAOD::ShallowAuxContainer* > \
    electrons_shallowCopy = xAOD::shallowCopyContainer( *truthElectrons );
  std::pair< xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer* > \
    muons_shallowCopy = xAOD::shallowCopyContainer( *truthMuons );
  std::pair< xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer* > \
    photons_shallowCopy = xAOD::shallowCopyContainer( *truthPhotons );
  std::pair< xAOD::MissingETContainer*, xAOD::ShallowAuxContainer* > \
    met_shallowCopy = xAOD::shallowCopyContainer( *truthMET );
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > \
    jets_shallowCopy = xAOD::shallowCopyContainer( *truthJets );

  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > fatjets_shallowCopy;  
  fatjets_shallowCopy.first=0;
  fatjets_shallowCopy.second=0;
  if(m_doFatjet) fatjets_shallowCopy = xAOD::shallowCopyContainer( *truthFatjets );

  m_configMgr->Reset_perEvt();
  m_configMgr->Reset_perSys();

  // MET filtering
  // TODO: Put through passEvtFiltering function
  if( !m_configMgr->objectTools->passTtbarMetOverlap(truthParticles,eventInfo) ){
    return EL::StatusCode::SUCCESS; 
  }

  // At the moment we don't create trees for different systematics here
  std::string sys_name = "";
  m_configMgr->setSysState(sys_name);

  // Create the skim objects
  m_configMgr->objectTools->get_xAOD_electrons_truth(electrons_shallowCopy.first,4000.0,2.47);
  m_configMgr->objectTools->get_xAOD_muons_truth(muons_shallowCopy.first,4000.0,2.70);
  m_configMgr->objectTools->get_xAOD_photons_truth(photons_shallowCopy.first,10000.0,2.37);
  m_configMgr->objectTools->get_xAOD_met_truth(met_shallowCopy.first);
  m_configMgr->objectTools->get_xAOD_jets_truth(jets_shallowCopy.first,20000.0,4.5);
  if(m_doFatjet) m_configMgr->objectTools->get_xAOD_fatjets_truth(fatjets_shallowCopy.first,100000.0,2.0);
  m_configMgr->objectTools->get_xAOD_truth(truthParticles,truthElectrons,truthMuons,truthTaus,truthPhotons,truthNeutrinos,truthBoson,truthTop,truthBSM,truthBornLep,truthVertices,truthJets,truthFatjets,eventInfo);

  // This should be the last container filled, i.e. cleaning cuts depend on our objects
  if( m_configMgr->objectTools->get_xAOD_event_truth(eventInfo,truthParticles,truthEvents) ){
    
    // Do full chain analysis
    if(m_baseUser){
      
      // Get objects from internally stored objects
      m_configMgr->objectTools->get_objects(m_configMgr->obj);

      // 
      m_configMgr->treeMaker->setDefaultVariables(m_configMgr->obj);

      // Perform users analysis
      m_baseUser->doAnalysis(m_configMgr);

    }

    // Fill skim trees
    m_configMgr->treeMaker->Fill(sys_name,"skim");

  }

  // delete the shallow copies
  delete electrons_shallowCopy.first;
  delete electrons_shallowCopy.second;
  delete muons_shallowCopy.first;
  delete muons_shallowCopy.second;
  delete photons_shallowCopy.first;
  delete photons_shallowCopy.second;
  delete met_shallowCopy.first;
  delete met_shallowCopy.second;
  delete jets_shallowCopy.first;
  delete jets_shallowCopy.second;
  delete fatjets_shallowCopy.first;
  delete fatjets_shallowCopy.second;

  return EL::StatusCode::SUCCESS;

}
// ----------------------------------------------------------------------------------------------- //
EL::StatusCode xAODEvtLooper::finalize()
{
 
  // Print cut flow
  m_configMgr->cutflow->Print();

  // Run finalize method for analysis codes
  if( m_baseUser ){
    m_baseUser->finalize(m_configMgr);
  }

  //Timer::Instance()->write();
  Timer::Instance()->printPerformance();

  // Log the time log in MetaData
  m_configMgr->metaData->timeLog = Timer::Instance()->getTimeLog();

  // Store this classes configuration
  m_configMgr->metaData->log("xAODEvtLooper","Running over data?",m_isData);
  m_configMgr->metaData->log("xAODEvtLooper","Fast simulation sample?",m_isAf2);
  m_configMgr->metaData->log("xAODEvtLooper","Processing truth only samples",m_isTruthOnly);
  m_configMgr->metaData->log("xAODEvtLooper","Write skims",m_writeSkims);
  m_configMgr->metaData->log("xAODEvtLooper","Write trees",m_writeTrees);
  m_configMgr->metaData->log("xAODEvtLooper","User configured number of events",m_nEvents);
  m_configMgr->metaData->log("xAODEvtLooper","Systematic set name",m_sysSetName);
  m_configMgr->metaData->log("xAODEvtLooper","Analysis selector name",m_selectorName);

  // Store the configuration of all classes the configMgr used.
  m_configMgr->Log();

  // Print out everything we stored
  m_configMgr->metaData->print();

  // Write out metaData
  if(m_writeMetaData>0){

    //
    MetaData* metaData = m_configMgr->metaData;
    const TH1* eventCounter = m_configMgr->getEvtCounter();
    const TH1* sumOfWeights = m_configMgr->getSOWHist();
    const TH1* nEventsHist = m_configMgr->getNEventsHist();
    //

    TTree* metaTree = new TTree("MetaTree","MetaTree");
    metaTree->SetDirectory( wk()->getOutputFile("tree") );
    metaTree->Branch("metaData",&metaData);
    metaTree->Fill();    

    // Write out metaData into skim files
    if(m_writeMetaData>=1 && m_writeSkims>0){
      //Info("xAODEvtLooper::finalize","Writing MetaData to skim stream...");
      wk()->getOutputFile("skim")->cd();
      //metaTree->Write();
      eventCounter->Write();
      sumOfWeights->Write();
      nEventsHist->Write();
      if(!m_noCutBookkeepers){
        m_configMgr->getCBKTree()->Write();
      }
    }
    
    // Write metaData into tree files
    if(m_writeMetaData>=2 && m_writeTrees>0){
      Info("xAODEvtLooper::finalize","Writing MetaData to tree stream...");
      wk()->getOutputFile("tree")->cd();
      metaTree->Write();
      eventCounter->Write();
      sumOfWeights->Write();
      nEventsHist->Write();
      if(!m_noCutBookkeepers){
        m_configMgr->getCBKTree()->Write();
      }
    }

  }

  // Clean up memory
  //if( m_configMgr ) delete m_configMgr;
  //if( m_baseUser  ) delete m_baseUser;
  return EL::StatusCode::SUCCESS;

}


///
///
/// Helper functions for event loop
///
///

/////////////////////////////////////////////////////////////////
void xAODEvtLooper::reportLapse(const int &ievt, const int &nevt, const clock_t &clock_start){

  /// 
  /// Print out the time lapse and the ETA to complete the loop (per file)
  ///
  ///   ievt:        i-th event in the loop
  ///   nevt:        Number of events to loop
  ///   clock_start: clock_t instance of start time
  ///

  if(nevt<=0) return;
  
  clock_t clock_stop = clock(); 

  std::stringstream ss;
  ss << "Event: " << ievt << " / " << nevt
     << "    (" << (int)ievt*100/nevt << "%) processed so far."
     << "       lapse:    " << (double)(clock_stop-clock_start)/CLOCKS_PER_SEC << "s";
  
  if(ievt==0)
    ss  << "       ETA: ---- ";
  else{
    double ETA = ((nevt-ievt)/(ievt+0.))*(double)(clock_stop-clock_start)/CLOCKS_PER_SEC;
    if(ETA<60) 
      ss  << "       ETA:    " << ETA << "s";
    else if(ETA<3600)
      ss  << "       ETA:    " << (int)(ETA/60) << "m" << (int)ETA-60*(int)(ETA/60) << "s";
    else
      ss  << "       ETA:    " << (int)(ETA/3600) << "h" << (int)(ETA-3600*(int)(ETA/3600))/60 << "m";
  }

  Info("execute()", "%s", ss.str().c_str());  

}

/////////////////////////////////////////////////////////////////
bool xAODEvtLooper::checkJetContainer(std::string containerName){  
  const xAOD::JetContainer *test_container(0);
  Info("checkContainerExistence()", "Test if the file has container %s.", containerName.c_str());
  bool containerExist = m_event->retrieve(test_container, containerName).isSuccess();    
  if(containerExist)
    Info("checkContainerExistence()", "Container %s is successfully found.", containerName.c_str());
  else
    Info("checkContainerExistence()", "No container %s is found. Will not try to retrieve in the event loop.", containerName.c_str());  
  return containerExist;
}

/////////////////////////////////////////////////////////////////
bool xAODEvtLooper::checkTruthVertexContainer(std::string containerName){  
  const xAOD::TruthVertexContainer *test_container(0);
  Info("checkContainerExistence()", "Test if the file has container %s.", containerName.c_str());
  bool containerExist = m_event->retrieve(test_container, containerName).isSuccess();    
  if(containerExist)
    Info("checkContainerExistence()", "Container %s is successfully found.", containerName.c_str());
  else
    Info("checkContainerExistence()", "No container %s is found. Will not try to retrieve in the event loop.", containerName.c_str());  
  return containerExist;
}

/////////////////////////////////////////////////////////////////
bool xAODEvtLooper::checkTruthParticleContainer(std::string containerName){  
  const xAOD::TruthParticleContainer *test_container(0);
  Info("checkContainerExistence()", "Test if the file has container %s.", containerName.c_str());
  bool containerExist = m_event->retrieve(test_container, containerName).isSuccess();    
  if(containerExist)
    Info("checkContainerExistence()", "Container %s is successfully found.", containerName.c_str());
  else
    Info("checkContainerExistence()", "No container %s is found. Will not try to retrieve in the event loop.", containerName.c_str());  
  return containerExist;
}
/////////////////////////////////////////////////////////////////
bool xAODEvtLooper::checkTruthEventContainer(std::string containerName){
  const xAOD::TruthEventContainer *test_container(0);
  Info("checkContainerExistence()", "Test if the file has container %s.", containerName.c_str());
  bool containerExist = m_event->retrieve(test_container, containerName).isSuccess();
  if(containerExist)
    Info("checkContainerExistence()", "Container %s is successfully found.", containerName.c_str());
  else
    Info("checkContainerExistence()", "No container %s is found. Will not try to retrieve in the event loop.", containerName.c_str());
  return containerExist;
}
/////////////////////////////////////////////////////////////////
void xAODEvtLooper::getMemUsage(double& vm_usage, double& resident_set)
{  
  ///
  /// Credit to https://stackoverflow.com/questions/669438/how-to-get-memory-usage-at-runtime-using-c/14927379
  ///
  /// Retrieve current mem usage of the machine (in MB)
  ///
  /// vm_usage    : Virtual set size (VSS)
  /// resident_set: Resident set size (RSS)
  ///

  vm_usage     = 0.0;
  resident_set = 0.0;

  // the two fields we want                                                                                                                                                                                                                         
  unsigned long vsize;
  long rss;
  {
    std::string ignore;
    std::ifstream ifs("/proc/self/stat", std::ios_base::in);
    ifs >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
        >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
        >> ignore >> ignore >> vsize >> rss;
  }

  long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages                                                                                                                                               
  vm_usage = vsize / 1024.0 / 1000.;
  resident_set = rss * page_size_kb / 1000.;
}
/////////////////////////////////////////////////////////////////

