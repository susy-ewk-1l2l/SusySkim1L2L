#include "SusySkimDriver/SkimEvtLooper.h"
#include "SusySkimMaker/MsgLog.h"

SkimEvtLooper::SkimEvtLooper() :  m_selectorName (""), 
				  m_sumOfWeightsPath(0), 
				  m_sumOfWeightsHist(0),
				  m_currentSumOfWeights(0), 
				  m_currentDSID(0), 
				  m_weighted__AOD(0),
				  m_unweighted__AOD(0),
				  m_mergedCBKTree(0),
				  m_doRunTimeConfig(true),
				  m_skipCopySumwHist(false),
				  m_isTruthOnly(false),
				  m_disableCuts(false)
{

}
/*--------------------------------------------------------------------------------*/
// Attach tree 
/*--------------------------------------------------------------------------------*/
void SkimEvtLooper::Init(TTree* tree)
{

  m_tree = tree;

  // Turn on TTreeCache with a 10MB cache size
  m_tree->SetCacheSize(10*1024*1024);

  // Enable all branches to be read
  m_tree->SetBranchStatus("*",1);

  // Variables
  m_evt             = new EventVariable();
  m_met             = new MetVector();
  m_muon_skim       = new MuonVector();
  m_ele_skim        = new ElectronVector();
  m_tau_skim        = new TauVector();
  m_photon_skim     = new PhotonVector();
  m_jet_skim        = new JetVector();
  m_fatjet_skim     = new JetVector();
  m_trackjet_skim   = new JetVector();
  m_track_skim      = new TrackVector();
  m_calo_skim       = new ObjectVector();
  m_truth_skim      = new TruthVector();
  m_truthEvent_skim = new TruthEvent();
  m_truthEvent_Vjets_skim = new TruthEvent_Vjets();
  m_truthEvent_VV_skim = new TruthEvent_VV();
  m_truthEvent_TT_skim = new TruthEvent_TT();
  m_truthEvent_XX_skim = new TruthEvent_XX();
  m_truthEvent_GG_skim = new TruthEvent_GG();


  // Set Address
  m_tree->SetBranchAddress("event",&m_evt,&m_evt_branch);
  m_tree->SetBranchAddress("electrons",&m_ele_skim,&m_ele_branch);
  m_tree->SetBranchAddress("muons",&m_muon_skim,&m_mu_branch);
  m_tree->SetBranchAddress("taus",&m_tau_skim,&m_tau_branch);
  m_tree->SetBranchAddress("photons",&m_photon_skim,&m_photon_branch);
  m_tree->SetBranchAddress("jets",&m_jet_skim,&m_jet_branch);  
  m_tree->SetBranchAddress("fatjets",&m_fatjet_skim,&m_fatjet_branch);  
  m_tree->SetBranchAddress("trackjets",&m_trackjet_skim,&m_trackjet_branch);  
  m_tree->SetBranchAddress("met",&m_met,&m_met_branch);  
  m_tree->SetBranchAddress("truthEvent",&m_truthEvent_skim,&m_truthEvent_skim_branch);
  m_tree->SetBranchAddress("truthEvent_Vjets",&m_truthEvent_Vjets_skim,&m_truthEvent_Vjets_skim_branch);
  m_tree->SetBranchAddress("truthEvent_VV",&m_truthEvent_VV_skim,&m_truthEvent_VV_skim_branch);
  m_tree->SetBranchAddress("truthEvent_TT",&m_truthEvent_TT_skim,&m_truthEvent_TT_skim_branch);
  m_tree->SetBranchAddress("truthEvent_XX",&m_truthEvent_XX_skim,&m_truthEvent_XX_skim_branch);
  m_tree->SetBranchAddress("truthEvent_GG",&m_truthEvent_GG_skim,&m_truthEvent_GG_skim_branch);
  m_tree->SetBranchAddress("truthParticles",&m_truth_skim,&m_truth_skim_branch);
  m_tree->SetBranchAddress("tracks",&m_track_skim,&m_track_branch);
  m_tree->SetBranchAddress("caloClus",&m_calo_skim,&m_calo_branch);


}
// ------------------------------------------------------------------- //
void SkimEvtLooper::Begin(TTree* tree) 
{

  // Output rootfile 
  m_outputFile = new TFile(fOption+".root","RECREATE");

  m_weighted__AOD   = 0;
  m_unweighted__AOD = 0;

  // Sum up all histograms and
  // TTrees and write them over
  // to the output file stream,
  // such that we can normalize 
  // our trees in the usual manner
  if (!m_skipCopySumwHist) {

    // CBK Histograms
    m_weighted__AOD = getMergedSumwHistograms("weighted__AOD");
    m_unweighted__AOD = getMergedSumwHistograms("unweighted__AOD");
    m_weighted__AOD->SetDirectory(m_outputFile);
    m_unweighted__AOD->SetDirectory(m_outputFile);

  }

  // Avoid compiler warnings...
  (void)tree;

  //
  // ---------------------------------------- //
  //
  m_configMgr = new ConfigMgr();
  m_configMgr->init_skim_running(m_outputFile);

  m_baseUser = getAnalysisSelector(m_selectorName);	
  m_baseUser->setSampleName(m_sampleName);
  m_baseUser->setup(m_configMgr);
  m_baseUser->setTruthOnly(m_isTruthOnly);
  m_baseUser->setDisableCuts(m_disableCuts);

  m_configMgr->obj->collectCuts();
  m_configMgr->obj->print(Objects::SKIM);

  // open event counter histogram for normalization
  if (m_sumOfWeightsPath) {
    MsgLog::INFO("SkimEvtLooper::Begin","Using %s for normalization",m_sumOfWeightsPath );
    TFile* tmpfile = TFile::Open(m_sumOfWeightsPath, "READ");
    if (tmpfile) {
      m_sumOfWeightsHist = dynamic_cast<TH1F*>(tmpfile->Get("weighted__AOD"));
      m_sumOfWeightsHist->SetDirectory(0);
      tmpfile->Close();
    } else {
      Abort("Couldn't open sumOfWeights file");
    }
  }

  // Basic information, required to be in all trees
  m_configMgr->treeMaker->addDefaultVariables();

  //
  m_outputFile->cd();
  m_mergedCBKTree = getMergedCBKTree();
  m_mergedCBKTree->SetDirectory( m_outputFile );

}
// ------------------------------------------------------------------- //
Bool_t SkimEvtLooper::Process(Long64_t entry)
{

  //
  bool gotAllTreeEntries = GetTreeEntries(entry);
  if(!gotAllTreeEntries){
    TString str_entry = TString::LLtoa(entry,10);
    MsgLog::ERROR("SkimEvtLooper::Process", "Entry number %s is invalid! Skipping.", str_entry.Data());
    return kFALSE;
  }

  if( m_doRunTimeConfig ){

    //
    MsgLog::INFO("SkimEvtLooper::Process","Doing a run time configuration...");

    if( m_evt->isTruth() ){
      MsgLog::INFO("SkimEvtLooper::Process","Configuring truth level overlap removal");

      m_configMgr->obj->setMinDrMuJet(0.4);
      m_configMgr->obj->setMinDrJetEle(0.2);
      m_configMgr->obj->setMinDrEleJet(0.4);
      m_configMgr->obj->doUserOR(1);
    }    

    m_doRunTimeConfig = false;

  }


  //
  static Long64_t chainEntry = -1;
  chainEntry++;

  if(chainEntry%10000==0)
    {
      cout << "**** Processing entry " << setw(6) << chainEntry 
	   << " run " << setw(6) << m_evt->runNumber
	   << " event " << setw(7) << m_evt->evtNumber << " ****" << endl;
      
    }

  //
  // TODO: This does nothing in terms of the 
  // initialization on ConfigMgr
  // Furthermore we have only given skim_NoSys to this selector
  //
  std::vector<TString> sysVec = m_configMgr->treeMaker->getSysVector();

  for(unsigned int i=0; i<sysVec.size(); i++){

    TString sys = sysVec[i];

    m_configMgr->obj->clear();
    m_configMgr->treeMaker->Reset();

    // Distribute systematic state to tool
    m_configMgr->setSysState(sys);

    // This will toggle ObjectTools to read these instead of internal vectors
    m_configMgr->obj->skimEvt           = m_evt;
    m_configMgr->obj->skimMet           = m_met;
    m_configMgr->obj->skimMuons         = m_muon_skim;
    m_configMgr->obj->skimElectrons     = m_ele_skim;
    m_configMgr->obj->skimTaus          = m_tau_skim;
    m_configMgr->obj->skimPhotons       = m_photon_skim;
    m_configMgr->obj->skimJets          = m_jet_skim;
    m_configMgr->obj->skimFatjets       = m_fatjet_skim;
    m_configMgr->obj->skimTrackjets     = m_trackjet_skim;
    m_configMgr->obj->skimTracks        = m_track_skim;
    m_configMgr->obj->skimCalo          = m_calo_skim;
    m_configMgr->obj->skimTruth         = m_truth_skim;
    m_configMgr->obj->skimTruthEvent    = m_truthEvent_skim;
    m_configMgr->obj->skimTruthEvent_Vjets = m_truthEvent_Vjets_skim;
    m_configMgr->obj->skimTruthEvent_VV = m_truthEvent_VV_skim;
    m_configMgr->obj->skimTruthEvent_TT = m_truthEvent_TT_skim;
    m_configMgr->obj->skimTruthEvent_XX = m_truthEvent_XX_skim;
    m_configMgr->obj->skimTruthEvent_GG = m_truthEvent_GG_skim;

    // Fill objects, apply cuts the user configured, etc
    m_configMgr->objectTools->get_objects(m_configMgr->obj);


    // Set number of events for normalization from event counter histogram if given
    if (m_sumOfWeightsHist) {
      // if sumOfWeights not set for this dsid read from histogram
      if (m_currentDSID != m_evt->dsid) {
        m_currentDSID = m_evt->dsid;
        int iBin = m_sumOfWeightsHist->GetXaxis()->FindBin(TString::Format("%d", m_currentDSID));
        m_currentSumOfWeights = m_sumOfWeightsHist->GetBinContent(iBin);
        if (!m_currentSumOfWeights) {
          Abort(TString::Format("no sumOfWeights given for dsid %d in file %s", m_currentDSID, m_sumOfWeightsPath).Data());
        } else {
          std::cout << std::endl;
          std::cout << "current DSID:         " << m_currentDSID << std::endl;
          std::cout << "current sumOfWeights: " << m_currentSumOfWeights << std::endl;
          std::cout << "current xsec:         " << m_evt->xsec << std::endl;
          std::cout << std::endl;
        }
      }
      m_configMgr->obj->evt.nEvents = m_currentSumOfWeights;
    }

    // Default variables
    m_configMgr->treeMaker->setDefaultVariables(m_configMgr->obj);
    m_configMgr->doTriggerAna();

    // Do user defined analysis
    m_baseUser->doAnalysis(m_configMgr);

  }

  return kTRUE;
}
// ------------------------------------------------------------------- //
bool SkimEvtLooper::GetTreeEntries(Long64_t entry)
{

  bool gotAll = true;

  if( m_evt_branch              ) gotAll = gotAll && SafelyGetEntry(entry, m_evt_branch);
  if( m_mu_branch               ) gotAll = gotAll && SafelyGetEntry(entry, m_mu_branch);
  if( m_ele_branch              ) gotAll = gotAll && SafelyGetEntry(entry, m_ele_branch);
  if( m_jet_branch              ) gotAll = gotAll && SafelyGetEntry(entry, m_jet_branch);
  if( m_fatjet_branch           ) gotAll = gotAll && SafelyGetEntry(entry, m_fatjet_branch);
  if( m_trackjet_branch         ) gotAll = gotAll && SafelyGetEntry(entry, m_trackjet_branch);
  if( m_track_branch            ) gotAll = gotAll && SafelyGetEntry(entry, m_track_branch);
  if( m_calo_branch             ) gotAll = gotAll && SafelyGetEntry(entry, m_calo_branch);
  if( m_photon_branch           ) gotAll = gotAll && SafelyGetEntry(entry, m_photon_branch);
  if( m_met_branch              ) gotAll = gotAll && SafelyGetEntry(entry, m_met_branch);
  if( m_tau_branch              ) gotAll = gotAll && SafelyGetEntry(entry, m_tau_branch);
  if( m_truthEvent_skim_branch  ) gotAll = gotAll && SafelyGetEntry(entry, m_truthEvent_skim_branch);
  if( m_truthEvent_Vjets_skim_branch ) gotAll = gotAll && SafelyGetEntry(entry, m_truthEvent_Vjets_skim_branch);
  if( m_truthEvent_VV_skim_branch    ) gotAll = gotAll && SafelyGetEntry(entry, m_truthEvent_VV_skim_branch);
  if( m_truthEvent_TT_skim_branch    ) gotAll = gotAll && SafelyGetEntry(entry, m_truthEvent_TT_skim_branch);
  if( m_truthEvent_XX_skim_branch    ) gotAll = gotAll && SafelyGetEntry(entry, m_truthEvent_XX_skim_branch);
  if( m_truthEvent_GG_skim_branch    ) gotAll = gotAll && SafelyGetEntry(entry, m_truthEvent_GG_skim_branch);
  if( m_truth_skim_branch       ) gotAll = gotAll && SafelyGetEntry(entry, m_truth_skim_branch);

  return gotAll;
}
// ------------------------------------------------------------------- //
bool SkimEvtLooper::SafelyGetEntry(Long64_t entry, TBranch* branch)
{
    int getEntry = branch->GetEntry(entry);
    if(getEntry==-1){
      MsgLog::ERROR("SkimEvtLooper::SafelyGetEntry", "GetEntry() I/O occurred error!");
      abort();
    }
    else if(getEntry==0){
      // This is the return code for an invalid entry, so skip it
      return false;
    }

    return true;
}
// ------------------------------------------------------------------- //
void SkimEvtLooper::Terminate()
{

  m_outputFile->cd();
  m_baseUser->finalize(m_configMgr);

  m_configMgr->cutflow->Print();

  m_outputFile->Write();
  m_outputFile->Close();

  if( m_sumOfWeightsHist ) delete m_sumOfWeightsHist;
  //if( m_weighted__AOD    ) delete m_weighted__AOD;
  //if( m_unweighted__AOD  ) delete m_unweighted__AOD;
  //if( m_mergedCBKTree    ) delete m_mergedCBKTree;

}
// ------------------------------------------------------------------- //
TTree* SkimEvtLooper::getMergedCBKTree()
{

  TList* mergeTreeList = new TList();
  TTree* mergedCBKTree = 0;
  Int_t totalEntries = 0;

  for(auto inputFile : m_inputFiles) {

    TFile* input = TFile::Open( inputFile.Data() , "READ");

    if( !input->IsOpen() ){
      MsgLog::ERROR("SkimEvtLooper::getMergedSumwHistograms", "Cannot open input skim file to extract normalization. Check your inputs.");
      abort();
    }

    TTree* cbkTree;
    input->GetObject("CutBookkeepers",cbkTree);
    if(cbkTree){
      TTree* _cbkTree = cbkTree->CloneTree();
      _cbkTree->SetDirectory(0);
      totalEntries += cbkTree->GetEntries();
      mergeTreeList->Add(_cbkTree);
     }
     else{
       MsgLog::ERROR("SkimEvtLooper::getMergedCBKTree", "FATAL ERROR Cannot find CBK Tree!! Check your inputs");;
       abort();
     }

    input->Close();
  }

  if (m_inputFiles.size() > 0) {
    mergedCBKTree = TTree::MergeTrees(mergeTreeList);

    if( mergedCBKTree->GetEntries() != totalEntries ){
      MsgLog::ERROR("SkimEvtLooper::getMergedCBKTree","Entries in merge tree %i does not equal the sum of the child trees %s",totalEntries,mergedCBKTree->GetEntries() );
      abort();
    }
  }

  return mergedCBKTree;

}
// ------------------------------------------------------------------- //
TH1* SkimEvtLooper::getMergedSumwHistograms(TString histname)
{

  TList* mergelist = new TList;
  TH1* merged = 0;

  for(auto inputFile : m_inputFiles) {

    TFile* input = TFile::Open( inputFile.Data() , "READ");

    if( !input->IsOpen() ){
      MsgLog::ERROR("SkimEvtLooper::getMergedSumwHistograms", "FATAL ERROR Cannot open input skim file to extract normalization. Check your inputs.");
      abort();
    }

    TH1* sumwhist;
    input->GetObject(histname, sumwhist);
    if (sumwhist) {
      sumwhist->SetDirectory(0);
      if (!merged) {
        merged = dynamic_cast<TH1*>(sumwhist->Clone());
        merged->SetDirectory(0);
        merged->Reset();
      }
      mergelist->Add(sumwhist);
    } else {
      MsgLog::ERROR("SkimEvtLooper::getMergedSumwHistograms", "FATAL ERROR Cannot find histogram %s to extract normalization. Check your inputs.", histname.Data());
      abort();
    }

    input->Close();
  }

  if (m_inputFiles.size() > 0) {
    merged->Merge(mergelist);
  } else {
    MsgLog::WARNING("SkimEvtLooper::getMergedSumwHistograms", "No input files given - can't merge sumw hists");
  }

  return merged;

}
// ------------------------------------------------------------------- //
SkimEvtLooper::~SkimEvtLooper()
{

}

