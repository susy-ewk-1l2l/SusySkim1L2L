#ifndef XAOD_NTUPLEANALYSIS_SKIMEVTLOOPER_H
#define xAOD_NTUPLEANALYSIS_SKIMEVTLOOPER_H

// ROOT lib
#include <vector>
#include <istream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <math.h>
#include <map>
#include <string>

#include "TSelector.h"
#include "TSystem.h"
#include "TTree.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TChainElement.h"
#include "TH1.h"

// Main include
#include "SusySkimMaker/BaseUser.h"
#include "SusySkimMaker/ObjectTools.h"

using namespace std;

class SkimEvtLooper : public TSelector
{

 public:
  SkimEvtLooper();
  ~SkimEvtLooper();

  virtual void    Begin(TTree *tree);
  virtual void    Init(TTree* tree);
  virtual void    Terminate();
  virtual Bool_t  Notify() { return kTRUE; }
  virtual Int_t   Version() const {
    return 2;
  }
  virtual Bool_t  Process(Long64_t entry);
  bool GetTreeEntries(Long64_t entry);
  bool SafelyGetEntry(Long64_t entry, TBranch* branch);

  // Sets which selector to use
  void setSelectorName(std::string selectorName){m_selectorName=selectorName;}
  void setSampleName(TString sampleName){m_sampleName=sampleName;}
  void setSumOfWeightsHist(const char* path){m_sumOfWeightsPath = path;}
  void setInputFiles(std::vector<TString> inputFiles){m_inputFiles=inputFiles;}
  void setSkipCopySumwHist(bool skipCopySumwHist){m_skipCopySumwHist=skipCopySumwHist;}
  void isTruthOnly(bool isTruthOnly){m_isTruthOnly=isTruthOnly;}
  void setDisableCuts(bool disableCuts){m_disableCuts=disableCuts;}

  // Merge sumw histograms from the input file(s)
  TH1* getMergedSumwHistograms(TString histname);
  TTree* getMergedCBKTree();

 protected:

  TFile* m_outputFile;

  std::string                 m_selectorName;
  TString                     m_sampleName;
  std::vector<TString>        m_inputFiles;

  BaseUser*              m_baseUser;
  ConfigMgr*             m_configMgr;
  TTree*                 m_tree;

  EventVariable*     m_evt;
  MetVector*         m_met;  
  MuonVector*        m_muon_skim;
  PhotonVector*      m_photon_skim;
  TauVector*         m_tau_skim;
  ElectronVector*    m_ele_skim;
  JetVector*         m_jet_skim;
  JetVector*         m_fatjet_skim;
  JetVector*         m_trackjet_skim;
  TrackVector*       m_track_skim;
  ObjectVector*      m_calo_skim;
  TruthVector*       m_truth_skim;
  TruthEvent*        m_truthEvent_skim;
  TruthEvent_Vjets*     m_truthEvent_Vjets_skim;
  TruthEvent_VV*        m_truthEvent_VV_skim;
  TruthEvent_TT*        m_truthEvent_TT_skim;
  TruthEvent_XX*        m_truthEvent_XX_skim;
  TruthEvent_GG*        m_truthEvent_GG_skim;

  TBranch*           m_evt_branch;
  TBranch*           m_evtCleaning_branch;
  TBranch*           m_mu_branch;
  TBranch*           m_photon_branch;
  TBranch*           m_ele_branch;
  TBranch*           m_tau_branch;
  TBranch*           m_jet_branch;
  TBranch*           m_fatjet_branch;
  TBranch*           m_trackjet_branch;
  TBranch*           m_track_branch;
  TBranch*           m_calo_branch;
  TBranch*           m_met_branch;
  TBranch*           m_mu_raw_branch;
  TBranch*           m_truth_skim_branch;
  TBranch*           m_truthEvent_skim_branch;
  TBranch*           m_truthEvent_Vjets_skim_branch;
  TBranch*           m_truthEvent_VV_skim_branch;
  TBranch*           m_truthEvent_TT_skim_branch;
  TBranch*           m_truthEvent_XX_skim_branch;
  TBranch*           m_truthEvent_GG_skim_branch;


  // if given, used to normalize MC to xsec and lumi
  const char* m_sumOfWeightsPath;
  TH1F* m_sumOfWeightsHist;
  float m_currentSumOfWeights;
  int m_currentDSID;

  TH1* m_weighted__AOD;
  TH1* m_unweighted__AOD;
  TTree* m_mergedCBKTree;

  //
  bool m_doRunTimeConfig;

  // should be set for all but the first job when splitting a single
  // file into multiple jobs
  bool m_skipCopySumwHist;

  // Write out truth information only? 
  int m_isTruthOnly;
  
  // Skip all the cuts defined in the user selector?                                  
  bool m_disableCuts;

};


#endif
