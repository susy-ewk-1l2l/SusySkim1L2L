#ifndef DECAY_BOSON_h
#define DECAY_BOSON_h

//+++++++++++++++++++ include STL +++++++++++++++++++++//
#include <iostream>
#include <cstdlib>

//++++++++++++++++++ include ROOT lib ++++++++++++++++++++++//
#include "TLorentzVector.h"
#include "TObject.h"

//+++++++++++++++++++ include Ex lib +++++++++++++++++++++//
#include "Decay_tau.h"

typedef TLorentzVector tlv;

//
// Container of 2-body decay information common to W/Z/H boson.
//
//  *** Naming convention ***
//    q1/q2: children particles 
//

class Decay_boson : public TObject {

 public:

  Decay_boson();

  virtual ~Decay_boson();

  Decay_boson(const Decay_boson &rhs);

  // Do this after filling:
  //         4-vector / pdg of V
  //         V children's tlv, pdg, tau1, tau2 (if needed)
  void finalize();
  void finalize_W();
  void finalize_Z();

  // Return decay label
  static TString getDecayString  (int in_pdg, int in_decayLabel);
  static TString getDecayString_W(int in_decayLabel);
  static TString getDecayString_Z(int in_decayLabel);

  // Swap branch of child1 and child2
  void swap12();
  void check_swap_W();
  void check_swap_Z();

  // Print content
  void print();

  // Reset content
  void clear();

  ClassDef(Decay_boson,2);

  // +++++++++++ Member Variables +++++++++++ //

  // ++++ Variables to be input
  // 4-vectors
  //    -parent boson
  tlv P4;

  //    -children
  tlv pq1;
  tlv pq2;

  // Decay label 
  int  decayLabel;

  // PdgIds
  //    -parent 
  int pdg;

  //    -children
  int q1_pdg, q2_pdg;


  // Child tau decay (optional)
  Decay_tau tau1, tau2;

  // ++++ Variables filled in finalized()

  // MET vector
  tlv pMis;

 private:  




};







#endif
