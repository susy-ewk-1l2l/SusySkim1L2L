#ifndef TRUTHDECAYUTILS_H
#define TRUTHDECAYUTILS_H

#include "TLorentzVector.h"
#include "TString.h"
#include "TVector3.h"

#include <iostream>
#include <iomanip>
#include <vector>

#include "AsgMessaging/StatusCode.h"

typedef TLorentzVector tlv;

//////////////////////////////////////////////////
//
// Easy utilities
//
/////////////////////////////////////////////////


namespace TruthDecayUtils{

 ///////////////////////////////////////
  // Swap the content of a and b.
 template <class T>
   inline void swap(T &a, T &b){
   T temp = a;
   a = b;  b = temp;  return;
 }

 ///////////////////////////////////////
 // DeltaR of two objects
 inline double GetDR(const double &eta1, const double &eta2, const double &phi1, const double &phi2){
   return hypot( eta1-eta2, acos(cos(phi1-phi2))  );   
 }
 inline double GetDR(const tlv &a, const tlv &b){
   return hypot(  a.Eta()-b.Eta(), acos(cos(a.Phi()-b.Phi())) );   
 }
 inline double GetDR_y(const tlv &a, const tlv &b){   
   return hypot(  a.Rapidity()-b.Rapidity(), acos(cos(a.Phi()-b.Phi())) );   
 }


 ///////////////////////////////////////
 // TLorentzVector of child particle in the parent rest frame
 inline  tlv boostBack_parent_RF(const tlv &child_lab, const tlv &parent_lab){
   tlv out = child_lab;
   if(parent_lab.Beta()<1) out.Boost(-parent_lab.BoostVector());  
   return out;
 }

 // Calulate the helicity angle 
 inline double costh_decay(const tlv &child_lab, const tlv &parent_lab){
   if(parent_lab.Beta()>=1) return -9; 
   tlv child_parent = boostBack_parent_RF(child_lab, parent_lab);
   return child_parent.Vect().Unit() * parent_lab.Vect().Unit();
 }


 ///////////////////////////////////////
 // Print 4-momentum. str: particle's name (optional)
 inline void print4mom(const tlv &a, TString name=""){
   std::cout << std::setw(4) << name << ": " 
	     << "pt " << setiosflags(std::ios::fixed) << std::setw(10) << a.Pt()
	     << "  px " << setiosflags(std::ios::fixed) << std::setw(10) << a.Px()
	     << "  py " << setiosflags(std::ios::fixed) << std::setw(10) << a.Py()
	     << "  pz " << setiosflags(std::ios::fixed) << std::setw(10) << a.Pz()
	     << "  cos " << setiosflags(std::ios::fixed) << std::setw(10) << a.CosTheta()
	     << "  eta " << setiosflags(std::ios::fixed) << std::setw(10) << a.Eta()
	     << "  phi " << setiosflags(std::ios::fixed) << std::setw(10) << a.Phi()
	     << "  E " << setiosflags(std::ios::fixed) << std::setw(10) << a.E()
	     << "  P " << setiosflags(std::ios::fixed) << std::setw(10) << a.P()    
	     << "  M " << setiosflags(std::ios::fixed) << std::setw(10) << a.M()
	     << "  Beta " << setiosflags(std::ios::fixed) << std::setw(10) << a.Beta()
	     << std::endl;
 }

 //////////////////////////////////////////////////
 /* 
  (Copied from SusySkimMaker::MsgLog) 
  Written by: Matthew Gignac (UBC)

  Static functions used to log and output messages
  Interally will store all msgs, such that we can 
  retrieve them later when running test jobs to see
  if any errors or warnings have occured

  Note, messages are not logged for INFO stream,
  since its expected to be much larger and not important
  to parse 
 */

 inline TString formatOutput(TString className,TString msg, TString msgStream)
 {
   
   Int_t MAX_CLASS_SIZE = 40;
   
  // Fixed length
   if( className.Length()>MAX_CLASS_SIZE ){
     className = className.Replace(MAX_CLASS_SIZE-3,className.Length(),"...",3);
   }
   
   TString line = TString::Format("%-43s%-20s%-10s",className.Data(),msgStream.Data(),msg.Data() );
  return line;
  
 }
 // --------------------------------------------------------- //
 inline TString format(const char* va_fmt, va_list args)
 {
   
   char buffer[256];
   vsnprintf (buffer,256,va_fmt,args);
   
   TString formatted_string = TString(buffer);
   return formatted_string;
   
 }
 // --------------------------------------------------------- //
 inline void INFO(TString className,const char *va_fmt, ...)
 {
   
   va_list args;
   va_start (args, va_fmt);
   TString infoMsg = format(va_fmt,args);
   va_end (args);
   
   TString line = TString::Format("%s",formatOutput(className,infoMsg,"INFO").Data()  );
   
   // Print
   std::cout << line << std::endl;
   
 }
 // --------------------------------------------------------- //
 inline void WARNING(TString className,const char *va_fmt, ...)
 {
   
   va_list args;
   va_start (args, va_fmt);
   TString warningMsg = format(va_fmt,args);
   va_end (args);
   
   TString line = TString::Format("%s",formatOutput(className,warningMsg,"WARNING").Data()  );
   
   // Print
  std::cout << line << std::endl;
  
 }
 // --------------------------------------------------------- //
 inline void ERROR(TString className,const char *va_fmt, ...)
 {
   
   va_list args;
   va_start (args, va_fmt);
   TString errorMsg = format(va_fmt,args);
   va_end (args);
   
   TString line = TString::Format("%s",formatOutput(className,errorMsg,"ERROR").Data()  );
   
   // Print
   std::cout << line << std::endl;
   
 }

}



 ///////////////////////////////////////
 /* 
  (Copied from SusySkimMaker::StatusCodeCheck) 
  Written by: Matthew Gignac (UBC)
 */

  #define CHECK( ARG )                                           \
   do {                                                        \
      const StatusCode sc__ = ARG;                             \
      const char* APP_NAME = "StatusCodeCheck.h";              \
      if( ! sc__.isSuccess() ) {                               \
         ::Error( APP_NAME, "Failed to execute: \"%s\"",       \
                  #ARG );                                      \
      }                                                        \
   } while( 0 )

 ///////////////////////////////////////


#endif
