#ifndef TRUTHEVENT_WJETS_h
#define TRUTHEVENT_WJETS_h

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TObject.h"

#include "TruthDecayContainer/Decay_boson.h"

typedef TLorentzVector tlv;


class TruthEvent_Vjets : public TObject {

 public:

  TruthEvent_Vjets();

  ~TruthEvent_Vjets();
    
  TruthEvent_Vjets(const TruthEvent_Vjets &rhs);

  void set_evt(const Decay_boson &in_V, 
	       const std::vector<tlv> &in_addPartons,
	       const std::vector<int> &in_addPartons_pdg,
	       const std::vector<tlv> &in_truthJets,
	       const std::vector<int> &in_truthJets_label);

  void finalize();

  void print();

  void clear();

  ClassDef(TruthEvent_Vjets,1);


  // tlv
  Decay_boson V;
  std::vector<tlv> addPartons;
  std::vector<int> addPartons_pdg;

  std::vector<tlv> truthJets;
  std::vector<int> truthJets_label;

  // ++++ derived variables
  double mV;
  double MET;

  // addtional parton flavor
  bool hasCharm=false;
  bool hasBottom=false;

  // Decay label
  int decayLabel;


 private:  




};







#endif
