2018-11-09 Shion Chen <shion.chen@cern.ch>
	* Generalize TruthEvent_Wjets to TruthEvent_Vjets
	* Add new decay container class TruthEvent_VV

2018-07-13 Shion Chen <shion.chen@cern.ch>
	* Add standalone tester (util/TruthDecayContainerTester)
	* Add variables for additional partons in TruthEvent_Wjets
	* Add the particle origin/type classification by IFF (https://twiki.cern.ch/twiki/pub/AtlasProtected/IsolationFakeForum/MakeTruthClassification.hxx)
	* Add CI build test.
	* Make some of the fucntion as static for standalone use without instance.
	* Tagging as 03-01.

2018-06-27 Shion Chen <shion.chen@cern.ch>
	* Add protection in DecayHandle::setTLV etc. against null pointers.
	* Fix the typo / add new samples (410646-7) in TruthDecayContainer/ProcClassifier.h.
	* Improve the literature of comments
	* Add README.md
	* Tagging as 02-07.

2018-06-15 Shion Chen <shion.chen@cern.ch>
	* Adding new higgsino samples in the ProcClassifier.h
	* Tagging as 02-06.

2018-03-25 Shion Chen <shion.chen@cern.ch>
	* Debug DecayHandle::GetDecayChain_WW2L/WZ3L_Sherpa to fill W/Z 4-vector and pdgId.
	* Tagging as 02-05.

2018-03-10 Shion Chen <shion.chen@cern.ch>
	* Disable duplicated dictionaries.
	* Tagging as 02-04.

2018-03-03 Shion Chen <shion.chen@cern.ch>
	* Bug fix in DecayHandle::GetEWGDecay: decay via Z was not picked up.
	* Bug fix in DecayHandle::GetDecayW/Z/H: boson tlv was not filled.
	* Tagging as 02-03.

2018-02-15 Shion Chen <shion.chen@cern.ch>
	* Change the message level of warnings in GetTauDecay (ERROR->WARNING).
	* Tagging as 02-02.

2018-01-26 Shion Chen <shion.chen@cern.ch>
	* Implement decay chain retriving functions with DecayHelper class.
	* Tagging as 02-01.

2018-01-16 Shion Chen <shion.chen@cern.ch>
	* Modify the name of the function / add more DSIDs in ProcClassifier.
	* Tagging as 01-05.

2018-01-09 Shion Chen <shion.chen@cern.ch>
	* Define default values of decayLabel / pMis for Decay_boson.
        * More info printed in TruthEvent_TT::print().
	* Disable auto the 4-vector reconstruction from children's 4-vectors for Decay_W/Z/H.
	* Remove redundant variables (pt,eta,phi.e,m) from Decay_x classes.
	* Tagging as 01-04.

2017-12-23 Shion Chen <shion.chen@cern.ch>
	* Change the naming of the lightest neutralino ("LSP1"->"N1A","LSP2"->"N1B") in TruthEvent_XX/GG to be respectful for GMSB/RPV community :)
	* Tagging as 01-03.

2017-12-22 Shion Chen <shion.chen@cern.ch>
	* Fix a bug around Decay_Z::finalize.
	* Tagging as 01-02.

2017-12-22 Shion Chen <shion.chen@cern.ch>
	* First commit.
	* Tagging as 01-01.
