# NAME     : singletop
# DATA     : 0
# AF2      : 0
# PRIORITY : 0

mc20_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_PHYS.e6527_s3681_r13144_p6490
mc20_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_PHYS.e6527_s3681_r13144_p6490
mc20_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_PHYS.e6671_s3681_r13144_p6490
mc20_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_PHYS.e6671_s3681_r13144_p6490
#601352
#601353
#601354
#601355