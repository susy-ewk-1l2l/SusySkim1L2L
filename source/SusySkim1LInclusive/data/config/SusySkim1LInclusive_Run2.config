
########################## READ BEFORE EDITING #########################
# Take note of the following:
#  1) Any line containing a '#' will be ignored
#     - Note that this means you cannot put comments at the end of the line!
#  2) Separate a field and its value by ":"
#  3) For booleans, use an integer: false==0, true==1
#  4) For paths, they will be expanded, so please always specify with respect to $ROOTCOREBIN env
########################################################################

#
# DxAOD Kernel name: This specifies which Bookkeeper data to extract
# and ultimately will be used to normalize the samples to the total sum of weights
#
DxAODKernel : PHYSKernel

#
# Important directories which define where to look
# for text files needed by the framework (if running in that mode)
#
#
SysFileDir         : SusySkim1LInclusive
GroupSetDir        : SusySkim1LInclusive

SusyToolsConfigFile : SusySkim1LInclusive/SUSYTools_SusySkim1LInclusive_Run2.conf

# In Rel.21, this should be simply the package name
# But will likely be removed anyhow, since the user
# will be required to provide <PackageName/ConfigFile>
SusyToolsConfigDir : SusySkim1LInclusive
isRun3 : 0
#
# Pile-up reweighting information
#
LumiCalcMC20A : GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root
LumiCalcMC20A : GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root
LumiCalcMC20CD : GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root
LumiCalcMC20E : GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root

#Setting automagic PRW for mc16d (and only mc16d at the moment)
UseAutoMagicPRW : 1
PRWUseCommonMCFiles : 1

# Shut off PRW
DisablePRW : 0

# Ignore the GRL
IgnoreGRL : 0

#
# Number of events from event counter histograms
# Note really used much anymore, but here for completeness and flexiability
#

NumEvtPath : $ROOTCOREBIN/data/SusySkimMaker/evtCounters/T_02_11trunk/nEvents.root

#
# Good run list information
#
Grl : GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml
Grl : GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml
Grl : GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml
Grl : GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml
#
# Cross section path
# Alternatively you can read them from $ROOTCOREBIN/data/SUSYTools/mc15_13TeV/ if your local
# SUSYTools version is more up to date then the cvmfs version
#

CrossSection : dev/PMGTools/PMGxsecDB_mc16.txt

UsePMGXsecTool : 1

#
# MET Rebuilding, true or false
#
ExcludeAdditionalLeptons : 0

#
# Filtering options
#
FilterBaseline     : 1
FilterOR           : 0
SkimNLeptons       : 0
DoSampleOR         : 0
DoSampleORZjetZgam : 0

# Combinations need to know how many baseline electrons (muons) with pT > 4.5 (3.0) GeV are in the event
# but we do not care about these in the analysis...
DoCombinationBaseline : 0

# Filter out the decays that shouldnt be in the Sherpa 2.2.1 WplvWmqq sample (363360)
# (set to false, we have fixed samples now)
DoSampleORWplvWmqq : 0

# Data specific filtering
RequireTrigInData : 1
RequireGRL : 1

DoBTagScores : 1
#DL1rContainerName : AntiKt4EMPFlowJets_BTagging201903
#DL1rContainerName_trkJet : AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903

#DL1dv01ContainerName : AntiKt4EMPFlowJets
#DL1dv01ContainerName_trkJet : AntiKtVR30Rmax4Rmin02PV0TrackJets
#TruthjetContainerName : AntiKt4TruthDressedWZJets
#TruthFatjetContainerName.: AntiKt10TruthTrimmedPtFrac5SmallR20Jets
#TrackjetContainerName: AntiKtVR30Rmax4Rmin02PV0TrackJets

# Enable tracks
#
SaveTracks : 0
DoTrackjet : 0
PrimaryTrackContainer : InDetTrackParticles 


# Misc configurations
#
UnitsFromMEV          : 0.001
SaveTruthInfo         : 0
SaveTruthTracks       : 0
SaveTruthEvtInfoFull  : 0
SaveTruthParticleInfo : 0
SaveTruthEvtInfo      : 0
WriteDetailedSkim     : 0
WriteLHE3             : 0

TriggerUpstreamMatching: 1
TriggerMatchingPrefix: TrigMatch_

# 
# 
DoCrackVetoCleaning   : 1

#
# Scale factor control
DoTriggerSFs : 1

#const TString SAVEGLOBALDILEPTONTRIGSFS = "SaveGlobalDileptonTrigSFs";
#const TString SAVEGLOBALPHOTONTRIGSF    = "SaveGlobalPhotonTrigSFs";
SaveGlobalDileptonTrigSFs : 0
SaveGlobalPhotonTrigSFs : 0

DisableTrigMatch: 0

# Use fat jets
#
DoFatjet : 1

# Truth level configurations
#
EmulateTruthBtagging : 0
ContinuousBtagScore : 4
#
# Electron ConfigFile arguments, used in ElectronObject init method
# Note that these files are only used to store the result into the skims
# for later studies and are independent of our signal object def (coming from ST)
#
ElectronTightConfigFile   : ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronLikelihoodTightOfflineConfig2015.conf
ElectronMediumConfigFile  : ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronLikelihoodMediumOfflineConfig2015.conf

# We want to include photons in the MET calculation since we use track soft terms
# We ommit taus since we don't explicitely use taus in the analysis right now (will be included as jets)
#
UsePhotonsInMET: 1
UseTausInMET: 0

#
# Tool verbosity
# VERBOSE = 1, DEBUG = 2, INFO = 3, WARNING = 4, ERROR = 5
#

TrigMatchToolVerbosity : 5
SUSYToolsVerbosity : 5
