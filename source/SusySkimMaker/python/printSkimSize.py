#!/usr/bin/env python

# TODO:
#  - Require user to pass a file, or many files, or a text file with files

import ROOT
import os,sys,argparse

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-f', dest='file_path',help='Path to the file you want to check')
args = parser.parse_args()
file_path=args.file_path

f = ROOT.TFile.Open( file_path, "READ" )
if not f or f.IsZombie():
    raise "Couldn't open file %s" % fileName

# Get the main event tree from the file:
t = f.Get( "skim_NoSys" )
if not t:
    raise "Couldn't find 'CollectionTree; in file %s" % fileName

# The entries in the TTree, for cross-checking:
entries = t.GetEntries()

# Get all the branches of the file:
branches = t.GetListOfBranches()
for i in xrange( branches.GetEntries() ):
    # Get the branch:
    branch = branches.At( i )
    # "Decode" the name of the branch:
    brName = branch.GetName()
    totalBytes= (branch.GetTotBytes( "*" ) / 1024.0)
    print "Branch name %-10s with size %12.3f kB" % (branch.GetName(),totalBytes) 
