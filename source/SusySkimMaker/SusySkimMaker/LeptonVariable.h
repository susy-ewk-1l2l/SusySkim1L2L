#ifndef SusySkimMaker_LeptonVariable_h
#define SusySkimMaker_LeptonVariable_h

// Rootcore
//#include "SusySkimMaker/ObjectVariable.h"
#include "SusySkimMaker/TruthVariable.h"
#include "SusySkimMaker/MCCorrections.h"
#include "SusySkimMaker/TrackVariable.h"

class LeptonVariable : public ObjectVariable
{

 public:
  LeptonVariable();
  virtual ~LeptonVariable(){};

  LeptonVariable(const LeptonVariable&);
  LeptonVariable& operator=(const LeptonVariable&);

  ///
  /// Lepton charge
  ///
  int q;

  ///
  /// Primary author for this lepton
  ///
  int author;

  ///
  /// Isolation variable: Sum of the pT of tracks within a radius of \DeltaR = 0.2.
  ///
  float ptcone20;

  ///
  /// Isolation variable: Sum of the pT of tracks within a radius \DeltaR = 0.3.
  ///
  float ptcone30;

  ///
  /// Isolation variable: Sum of the pT of tracks within a radius of \DeltaR = 0.4.
  ///
  float ptcone40;

  ///
  /// Isolation variable: Sum of the topological cluster ET within a cone of a radius dR = 0.2
  ///
  float topoetcone20;
  
  ///
  /// Isolation variable: Sum of the topological cluster ET within a cone of a radius dR = 0.3
  ///
  float topoetcone30;
  
  ///
  /// Isolation variable:Sum of the topological cluster ET within a cone of a radius dR = 0.4;
  ///
  float topoetcone40;

  ///
  /// Isolation variables: For pT>50 GeV, ptvarcone has a cone of dr=0.2;
  ///
  float ptvarcone20;

  ///
  /// Isolation variables: For pT>33.3 GeV, ptvarcone has a cone of dr=0.3;
  ///
  float ptvarcone30;

  ///
  /// Isolation variables: For pT>25 GeV, ptvarcone has a cone of dr=0.4;
  ///
  float ptvarcone40;

  ///
  /// Isolation variables: pTvarcone calculated with tracks passing the standard d0Sig, Z0SinTheta cuts ("Track-To-Vertex-Associon", TTVA). Available in the derivations with ptag p3517+.
  ///
  float ptvarcone30_TightTTVA_pt1000;
  float ptvarcone30_TightTTVA_pt500;

  float ptvarcone20_TightTTVA_pt1000;
  float ptvarcone20_TightTTVA_pt500;

  float ptcone20_TightTTVALooseCone_pt500;
  float ptcone20_TightTTVALooseCone_pt1000;

  float ptcone20_TightTTVA_pt500;
  float ptcone20_TightTTVA_pt1000;

  float ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000;
  float ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000;

  float ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500;
  float ptcone20_Nonprompt_All_MaxWeightTTVA_pt500;

  ///
  /// Isolation variables: Particle flow cone iso variable.
  ///
  float neflowisol20;

  // Isolation flags for the recommended working points
  // => https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RecommendedIsolationWPs
  
  //Old WPs
  bool IsoFCHighPtCaloOnly;       // electron only
  bool IsoFCTightTrackOnly; //muon only
  bool IsoFCLoose;
  bool IsoFCTight;
  bool IsoFCLoose_FixedRad;
  bool IsoFCTight_FixedRad;

  bool IsoHighPtCaloOnly;       // electron only
  bool IsoHighPtTrackOnly;      // muon only
  bool IsoTightTrackOnly_VarRad;
  bool IsoTightTrackOnly_FixedRad;
  bool IsoLoose_VarRad;
  bool IsoLoose_FixedRad;       // muon only
  bool IsoTight_VarRad;
  bool IsoTight_FixedRad;       // muon only
  bool IsoPflowLoose_VarRad;    // muon only (electron one is marked as 'experimental')
  bool IsoPflowLoose_FixedRad;  // muon only (electron one is marked as 'experimental')
  bool IsoPflowTight_VarRad;    // muon only (electron one is marked as 'experimental')
  bool IsoPflowTight_FixedRad;  // muon only (electron one is marked as 'experimental')
  bool IsoPLVLoose;
  bool IsoPLVTight;
  bool IsoPLImprovedTight;
  bool IsoPLImprovedVeryTight;

  // For the NearbyLepIsoCorr versions of the iso vars
  float corr_ptcone20;
  float corr_ptcone30;
  float corr_ptcone40;
  float corr_topoetcone20;
  float corr_topoetcone30;
  float corr_topoetcone40;
  float corr_ptvarcone20;
  float corr_ptvarcone30;
  float corr_ptvarcone40;
  float corr_ptvarcone30_TightTTVA_pt1000;
  float corr_ptvarcone30_TightTTVA_pt500;
  float corr_ptvarcone20_TightTTVA_pt1000;
  float corr_ptvarcone20_TightTTVA_pt500;
  float corr_ptcone20_TightTTVALooseCone_pt500;
  float corr_ptcone20_TightTTVALooseCone_pt1000;
  float corr_ptcone20_TightTTVA_pt500;
  float corr_ptcone20_TightTTVA_pt1000;
  // float corr_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000;
  // float corr_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000;
  // float corr_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500;
  // float corr_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500;
  // float corr_neflowisol20;

  //Old WPs
  bool  corr_IsoFCHighPtCaloOnly;   // electron only
  bool  corr_IsoFCTightTrackOnly; //muon only
  bool  corr_IsoFCLoose;
  bool  corr_IsoFCTight;
  bool  corr_IsoFCLoose_FixedRad;
  bool  corr_IsoFCTight_FixedRad;

  
  bool corr_IsoHighPtCaloOnly;   // electron only
  bool corr_IsoHighPtTrackOnly;  // muon only
  bool corr_IsoTightTrackOnly_VarRad;
  bool corr_IsoTightTrackOnly_FixedRad;
  bool corr_IsoLoose_VarRad;
  bool corr_IsoLoose_FixedRad;       // muon only
  bool corr_IsoTight_VarRad;
  bool corr_IsoTight_FixedRad;       // muon only
  bool corr_IsoPflowLoose_VarRad;    // muon only (electron one is marked as 'experimental')
  bool corr_IsoPflowLoose_FixedRad;  // muon only (electron one is marked as 'experimental')
  bool corr_IsoPflowTight_VarRad;    // muon only (electron one is marked as 'experimental')
  bool corr_IsoPflowTight_FixedRad;  // muon only (electron one is marked as 'experimental')

  ///
  /// Track associated with the lepton
  ///
  TLorentzVector trackTLV;

  ///
  /// Save the truth type and origin for leptons. If this cannot be found, its defaulted to -999.
  /// Truth variables, the codes can be found here:
  ///   => https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/MCTruthClassifier/trunk/MCTruthClassifier/MCTruthClassifierDefs.h#L24
  ///
  int type;
  int origin;
  int iff_type;
 
  float d0;
  float z0;

  float d0Err;
  float z0Err;

  float pTErr;

  bool signal;
  bool passOR;
  bool baseline;
  //bool baseline_EWKcomb;

  ///
  /// Track Pixel and IBL hits
  ///
  uint8_t nPixHits;
  uint8_t nIBLHits;

  ///
  /// Charge of the truth lepton
  ///
  int truthQ;

  ///
  /// ElectronVariable and MuonVariable implement these for the IDs
  ///
  virtual bool passesTight()     const { return false; }
  virtual bool passesMedium()    const { return false; }
  virtual bool passesLoose()     const { return false; }
  virtual bool passesVeryLoose() const { return false; }
  virtual bool passesTightDNN()     const { return false; }
  virtual bool passesMediumDNN()    const { return false; }
  virtual bool passesLooseDNN()     const { return false; }

  ///
  /// Reconstruction efficiency scale factor, key=systematic name
  ///
  std::map<TString,float> recoSF;

  ///
  /// Trigger efficiencies
  ///
  MCCorrContainer trigEff;

  ///
  /// Method to get the reconstruction SF for a given systematic
  ///
  float getRecoSF(const TString sys); 

  ///
  /// Method to get the trigger eff and SF for a given systematic
  ///
  double getTrigEffMC(const TString instance, const TString sys);
  double getTrigEffData(const TString instance, const TString sys);
  double getTrigSF(const TString instance, const TString sys);

  ///
  /// Electron or muon?
  ///
  virtual bool isEle() const { return false; } //!
  virtual bool isMu()  const { return false; } //!

  ///
  /// Truth link
  ///
  mutable TruthVariable* truthLink;

  bool hasTruthLink(); //!

  // Track link
  TrackVariable* trkLink;

  ///
  /// PromptLeptonTagger variables
  /// 
  int   plt_input_TrackJetNTrack;
  float plt_input_DRlj;
  float plt_input_rnnip;
  float plt_input_DL1mu;
  float plt_input_PtRel;
  float plt_input_PtFrac;
  float plt_input_Topoetcone30Rel;
  float plt_input_Ptvarcone30Rel;
  float promptLepVeto_score;
  float LowPtPLV_score;

  ClassDef(LeptonVariable, 6);

};

#endif
