#ifndef SusySkimMaker_MergeTool_h
#define SusySkimMaker_MergeTool_h

// C++ includes
#include <iostream>
#include <map>
#include <boost/any.hpp>
#include <string>
#include <tuple>

// ROOT includes
#include "TFile.h"
#include "TH1F.h"
#include "TTree.h"
#include "TChain.h"
#include "TString.h"
#include "TObject.h"
#include "TGraphAsymmErrors.h"
#include "TH2.h"

/*

Replicate the work done in SampleHandler already,
with some reorganization such that things are done 
in a more complete and checked manner

Will allow easy user access to do a more complicated merge, 
e.g. split by truth classification, rather than DSIDs, which is done now

*/

class EventObject;
class StatusCode;

class MergeTool : public TObject
{

 public: 

  // Class to old Data types
  class DataType
  {
    public:

    enum Type{
      INT=0,
      DOUBLE=1,
      FLOAT=2,
      BOOL=3,
      ULL=4,
      VECINT=5,
      VECDOUBLE=6,
      VECFLOAT=7,
      VECBOOL=8	
    };

    Type type;

    DataType(Type user_type){type=user_type;}
    ~DataType(){};
  
    int                 intType;
    double              doubleType;
    float               floatType;
    bool                boolType;
    unsigned long long  ullType;
    std::vector<int>* vecIntType = 0;
    std::vector<double>* vecDoubleType = 0;
    std::vector<float>* vecFloatType = 0;
    std::vector<bool>* vecBoolType = 0;
    
  };


  // Grouped with an int, which can be used to separate trees
  struct ChildTree{
    TTree* tree;
    TString branchName;
    int intValue;
  };

 public:

  MergeTool(TChain* parentTree,TChain* cbkChain);
  ~MergeTool();

  ///
  /// Initialize this class
  ///
  StatusCode init(bool isData, bool isAf2,int minEvent=-1,int maxEvent=-1);

  // Each calculation should be mapped to 
  // a single one of these attributes.
  // In SetBranchAddresses(...), when a branch
  // address is set successful, this is stored
  // in m_attributes. Only Attributes which are 
  // true in the map will be executed during the 
  // loop over the parent tree.
  enum MergeAttribute{
    UNKNOWN=0,
    USER=1,
    NORMALIZATION=2,
    CBK_FSSUM=3,
    TOPOVERLAP=4,
    XSEC=5
  };
  
  // This would divide the trees
  void addClone(TString name,TFile* outFile,TString branchName="",int value=-1);

  // Fill all trees added via addClone(..) 
  void calculateCommon();
  void fill();
  bool next();

  void fillOutputTrees(int minEvent=-1,int maxEvent=-1);

  // Print any statistics 
  //   - Warnings if not all events were used in any of the child trees
  void print();

  // Set luminosity
  void setLumi(float lumi){m_lumi=lumi;}

  void SetBranchAddresses();
  void SetBranchAddress(TString name,DataType::Type branchType,MergeAttribute att);
  DataType* getProperty(TString name);



 ///
 /// Private methods
 ///
 private:

  ///
  /// Executes MergeAttribute NORMALIZATION.
  /// Recalculates the genWeight branch, and its systematic variations
  /// as coming from SUSYTools xsec DB. 
  ///
  void calculateNormalization();

  ///
  /// Executes CBK_FSSUM, 
  /// and calculates the normalization of signal samples
  /// by summing cross sections with a non-trival FS value
  /// and using the AllEvents CBK to calculate the normalization
  void calculateSumXsecNorm();

  ///
  /// Executes MergeAttrbute TOPOVERLAP
  ///
  bool doTopOverlap(TString treeName);

  ///
  /// For usage in passTopOverlap
  ///
  bool isTopHTFiltered(int DatasetNumber);
  bool isTopMETFiltered(int DatasetNumber);
  bool isTopMET200(int DatasetNumber);
  bool isTopMET300(int DatasetNumber);
  bool isTopInclusive(int DatasetNumber);
  bool isWtSample(int DatasetNumber);
  bool isTTBARSample(int DatasetNumber);
  bool isDijetSample(int DatasetNumber);

  // Set addresses for parent tree
  // Called from the constructor
  // Should check if the branch exists, if it doesn't turn off
  // any features requiring that information
  //void SetBranchAddresses();
  //void SetBranchAddress(TString name,DataType::Type branchType,MergeAttribute att);

  //
  void setAttributeState(MergeAttribute att, bool state);
  bool getAttributeState(MergeAttribute att);

  // 
  bool branchAddressSet(TString name);

  ///
  /// Fill the map of summed cutBookkeepers from cbkChain
  ///
  void fillCBKMap();

 public:
  std::map<TString,ChildTree*> getChildTrees(){ return m_childTrees; }


 ///
 /// Private fields
 ///
 private:

  ///
  /// Parent tree. A loop is performed over all of its entries
  /// and its contents are modified and distributed to all child 
  /// trees added via the addClone(...) methods.
  ///
  TChain* m_parentTree;

  ///
  /// Histgram used to extract the normalization. 
  /// Assumed to have bin labels with the DSID
  ///
  TH1F* m_evtCounterHist;

  ///
  /// Tree with CutBookkeeper information
  ///
  TChain* m_cbkChain;

  ///
  /// Luminosity scale factor. Default 1.0, i.e. take the normalization from the input trees
  ///
  float m_lumi;

  ///
  /// Data flag, set in the init method
  ///
  bool m_isData;

  int m_eventNumber;
  int m_minEvent;
  int m_maxEvent;


  ///
  /// Child trees. A new tree is created and added into this map 
  /// everytime addClone(...) is called.
  ///
  std::map<TString,ChildTree*> m_childTrees;

  ///
  /// Attributes. Each is associated with a process
  ///
  std::map<MergeAttribute,bool> m_attributes;


  ///
  /// Stores all the branch addresses associated with the parent tree
  ///
  std::map<TString,DataType*> m_fields;

  ///
  /// Map for sumOfWeights - to be extracted from cbkTree
  /// <DatasetNumber, RunNumber, branchName> -> totalSumOfWeights
  ///
  std::map<std::tuple<int, int, TString>, float> m_cbkSumwMap;

  ///
  /// Map for nAcceptedEvents - to be extracted from cbkTree
  /// <DatasetNumber, RunNumber, branchName> -> nAcceptedEvents
  ///
  std::map<std::tuple<int, int, TString>, int> m_cbkNEventMap;

  ///
  /// For calculating the cross sections
  ///
  EventObject* m_eventObject;

  ///
  /// Reweighting for the MET trigger
  ///
  TH1F* m_METTrigSF;

  TGraphAsymmErrors* m_tg;
  TH2* m_muonRates;
  TH2* m_electronRates;


  TGraphAsymmErrors* m_muonFakeRate;
  TGraphAsymmErrors* m_electronFakeRate;
  TGraphAsymmErrors* m_muonRealRate;
  TGraphAsymmErrors* m_electronRealRate;

};


#endif
