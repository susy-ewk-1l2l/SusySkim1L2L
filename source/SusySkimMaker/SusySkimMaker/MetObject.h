#ifndef SusySkimMaker_MetObject_h
#define SusySkimMaker_MetObject_h

// Rootcore
#include "SusySkimMaker/MetVariable.h"
#include "SusySkimMaker/BaseHeader.h"

// xAOD
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

class TreeMaker;
class StatusCode;

// 
typedef std::vector<MetVariable*> MetVector;

//
class MetObject
{

 private:

  ///
  /// Internal call to initialize all tools needed for this class
  ///
  StatusCode init_tools();

  ///
  /// Get the output container name of MET
  ///
  std::string getMetContainerName(MetVariable::MetFlavor flavor);

 public:
  MetObject();
  virtual ~MetObject();

  //
  StatusCode init(TreeMaker*& treeMaker);

  // Fill reco containers
  void fillMetContainer(xAOD::MissingETContainer* met,
                        const xAOD::MissingETContainer* met_truth,
                        const std::map<TString,bool> trigMap, 
                        const double &metSignif,
                        MetVariable::MetFlavor flavor,
                        std::string sys_name);

  // Fill truth containers
  void fillMetContainerTruth(xAOD::MissingETContainer* met,std::string sys_name);

  const MetVector* getObj(TString sys_name);
  void Reset();

 protected:
  std::map<TString,MetVector*>      m_metMap;
  float                              m_convertFromMeV;

};

#endif
