#ifndef SusySkimMaker_V0Object_h
#define SusySkimMaker_V0Object_h

/*
 *  Base class for any object which utilizes xAOD::TrackParticle
 *  Can use the methods in this class to get information about the
 *  track parameters, ie d0, z0, etc
 *
 *  INFO: Being extended to store Tracks
 *
 **/

#include "xAODRootAccess/TEvent.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "SusySkimMaker/V0Variable.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"


// Forward dceclarations
class TreeMaker;

// Typedefs
typedef std::vector<V0Variable*> V0Vector;
typedef std::vector<ObjectVariable*> ObjectVector;

//
class V0Object
{

 public:
   V0Object();
   virtual ~V0Object();

   ///
   /// Initialization method for this class. Connects each TrackletVariable in global m_muonVectorMap map
   /// to output trees found in TreeMaker class
   ///
   StatusCode init(TreeMaker*& treeMaker,xAOD::TEvent* event);


   ///
   /// Fill
   ///
   StatusCode fillV0Container(const xAOD::VertexContainer* primVertex,
                              const xAOD::EventInfo* eventInfo,
                              const xAOD::TruthParticleContainer* truthParticles,
                              std::string sys_name="");


   /*
    * Impact parameter d0 and its error
    **/
   static float getD0(const xAOD::TrackParticle* track);
   static float getD0Err(const xAOD::TrackParticle* track,const xAOD::EventInfo* eventInfo);

   /*
    * Impact parameter z0 and its error
    **/
   static float getZ0(const xAOD::TrackParticle* track,const xAOD::VertexContainer* primVertex);
   static float getZ0Err(const xAOD::TrackParticle* track);


   ///
   const V0Vector* getObj(TString sysName);
   const ObjectVector* getCaloObj(TString sysName);

   ///
   /// Clear internal TrackVectors inside
   ///
   void Reset();

 protected:

   ///
   /// Initialize all tools needed for this class
   ///
   StatusCode init_tools();

   ///
   /// Fill standard track information
   ///
   void fillTrackInfo(V0Variable*& trk,const xAOD::TrackParticle* xAODTrk,
                      const xAOD::VertexContainer* primVertex,
                      const xAOD::EventInfo* eventInfo,
                      const xAOD::TruthParticleContainer* truthParticles);


    double getTrackIsolation(const xAOD::TrackParticle* track,
                             const xAOD::VertexContainer* vertex,
                             double dR);


   ///
   /// Global std::map to store TrackletVectors for all systematic uncertainties
   ///
   std::map<TString,V0Vector*>  m_v0VectorMap;
   std::map<TString,ObjectVector*>  m_caloVectorMap;

 private:
   ///
   /// Convert units from MeV
   ///
   float m_convertFromMeV;

   ///
   /// TEvent object
   ///
   xAOD::TEvent* m_event;

   ///
   /// Primary track container name, read in the CentralDB field
   ///
   std::vector<TString> m_primaryTrackContainer;

   ///
   /// Secondary track container name, read in from CentralDB field
   ///
   TString m_secondaryTrackContainer;

   ///
   /// Vertex container name, that is used to associate a secondary
   /// track to the primary track container
   ///
   TString m_priSecVtxContainer;


   //
   // Track selection tool
   //
   InDet::InDetTrackSelectionTool* m_v0TrkQualityTool;


};

#endif
