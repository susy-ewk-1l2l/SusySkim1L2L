#ifndef SusySkimMaker_PkgDiscovery_h
#define SusySkimMaker_PkgDiscovery_h

#include "TString.h"
#include "AsgMessaging/StatusCode.h"

class PkgDiscovery
{

 public:

  //
  PkgDiscovery(){};
  ~PkgDiscovery(){}

  // All executables are required to call this function 
  static StatusCode Init(TString deepConfig="",TString selector="");

  static TString getDeepConfig();
  static TString getDeepConfigFullPath();


};

#endif
