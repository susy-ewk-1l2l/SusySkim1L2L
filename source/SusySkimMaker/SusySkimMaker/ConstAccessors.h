
#ifndef SusySkimMaker_CONSTACCESSORS_H
#define SusySkimMaker_CONSTACCESSORS_H

static SG::AuxElement::ConstAccessor< unsigned char > cacc_nPrecisionLayers( "numberOfPrecisionLayers" );
static SG::AuxElement::ConstAccessor< unsigned char > cacc_nPrecisionHoleLayers( "numberOfPrecisionHoleLayers" );
static SG::AuxElement::ConstAccessor< unsigned char > cacc_nGoodPrecisionLayers( "numberOfGoodPrecisionLayers" );
static SG::AuxElement::ConstAccessor< float > cacc_InnerDetectorPt( "InnerDetectorPt" );
static SG::AuxElement::ConstAccessor< float > cacc_MuonSpectrometerPt( "MuonSpectrometerPt" );
static SG::AuxElement::ConstAccessor< float > cacc_GenFiltMET( "GenFiltMET" );
static SG::AuxElement::ConstAccessor< float > cacc_GenFiltHT( "GenFiltHT" );
static SG::AuxElement::ConstAccessor<unsigned int> cacc_randomrunnumber("RandomRunNumber");
static SG::AuxElement::ConstAccessor< char > cacc_evtCleanLooseBad("DFCommonJets_eventClean_LooseBad");
static SG::AuxElement::ConstAccessor< char > cacc_evtCleanTightBad("DFCommonJets_eventClean_TightBad");
static SG::AuxElement::ConstAccessor< char > cacc_emulJetCleanTightBad1Jet("Emul_eventClean_TightBad1Jet");
static SG::AuxElement::ConstAccessor< char > cacc_emulJetCleanTightBad2Jet("Emul_eventClean_TightBad2Jet");
static SG::AuxElement::ConstAccessor< char > cacc_signal( "signal" );
static SG::AuxElement::ConstAccessor< char > cacc_cosmic( "cosmic" );
static SG::AuxElement::ConstAccessor< char > cacc_bad( "bad" );
static SG::AuxElement::ConstAccessor< char > cacc_baseline( "baseline" );
//static SG::AuxElement::ConstAccessor< char > cacc_baseline_EWKcomb( "baseline_EWKcomb" ); // to be added by the ST in near future releases
static SG::AuxElement::ConstAccessor< char > cacc_passOR( "passOR" );
static SG::AuxElement::ConstAccessor< float > cacc_dRJet("dRJet");
static SG::AuxElement::ConstAccessor< double > cacc_effscalefact( "effscalefact" );
static SG::AuxElement::ConstAccessor< float > cacc_jvtscalefact( "jvtscalefact" );
static SG::AuxElement::ConstAccessor< char > cacc_bjet( "bjet" );
static SG::AuxElement::ConstAccessor< float > cacc_Jvt( "Jvt" );
static SG::AuxElement::ConstAccessor< double > cacc_btag_weight( "btag_weight" );
static SG::AuxElement::ConstAccessor< int > cacc_HadronConeExclTruthLabelID( "HadronConeExclTruthLabelID" );
static SG::AuxElement::ConstAccessor< int > cacc_PartonTruthLabelID( "PartonTruthLabelID" );
static SG::AuxElement::ConstAccessor< int > cacc_ConeTruthLabelID( "ConeTruthLabelID" );
static SG::AuxElement::ConstAccessor< int > cacc_GhostBHadronsFinalCount( "GhostBHadronsFinalCount" );
static SG::AuxElement::ConstAccessor< float > cacc_D2( "D2" );
static SG::AuxElement::ConstAccessor< float > cacc_ECF1( "ECF1" );
static SG::AuxElement::ConstAccessor< float > cacc_ECF2( "ECF2" );
static SG::AuxElement::ConstAccessor< float > cacc_ECF3( "ECF3" );
static SG::AuxElement::ConstAccessor< float > cacc_Tau1_wta( "Tau1_wta" );
static SG::AuxElement::ConstAccessor< float > cacc_Tau2_wta( "Tau2_wta" );
static SG::AuxElement::ConstAccessor< float > cacc_Tau3_wta( "Tau3_wta" );
static SG::AuxElement::ConstAccessor< float > cacc_XbbScoreQCD( "XbbScoreQCD" );
static SG::AuxElement::ConstAccessor< float > cacc_XbbScoreTop( "XbbScoreTop" );
static SG::AuxElement::ConstAccessor< float > cacc_XbbScoreHiggs( "XbbScoreHiggs" );
static SG::AuxElement::ConstAccessor< int > cacc_FatjetTruthLabel( "R10TruthLabel_R21Consolidated" );
static SG::AuxElement::ConstAccessor< float > cacc_FatjetTruthLabel_dR_W( "R10TruthLabel_R21Consolidated_dR_W" );
static SG::AuxElement::ConstAccessor< float > cacc_FatjetTruthLabel_dR_Z( "R10TruthLabel_R21Consolidated_dR_Z" );
static SG::AuxElement::ConstAccessor< float > cacc_FatjetTruthLabel_dR_H( "R10TruthLabel_R21Consolidated_dR_H" );
static SG::AuxElement::ConstAccessor< float > cacc_FatjetTruthLabel_dR_Top( "R10TruthLabel_R21Consolidated_dR_Top" );
static SG::AuxElement::ConstAccessor< int > cacc_FatjetTruthLabel_NB( "R10TruthLabel_R21Consolidated_NB" );
static SG::AuxElement::ConstAccessor< float > cacc_FatjetTruthLabel_TruthJetMass( "R10TruthLabel_R21Consolidated_TruthJetMass" );
static SG::AuxElement::ConstAccessor< std::vector<int> > cacc_NumTrkPt500( "NumTrkPt500" );
static SG::AuxElement::ConstAccessor< int > cacc_firstEgMotherTruthType( "firstEgMotherTruthType" );
static SG::AuxElement::ConstAccessor< int > cacc_firstEgMotherTruthOrigin( "firstEgMotherTruthOrigin" );
static SG::AuxElement::ConstAccessor< int > cacc_firstEgMotherPdgId( "firstEgMotherPdgId" );
static SG::AuxElement::ConstAccessor< short > cacc_promptLepInput_TrackJetNTrack("PromptLeptonInput_TrackJetNTrack");
static SG::AuxElement::ConstAccessor< float > cacc_promptLepInput_DRlj("PromptLeptonInput_DRlj");
static SG::AuxElement::ConstAccessor< float > cacc_promptLepInput_rnnip("PromptLeptonInput_rnnip");
static SG::AuxElement::ConstAccessor< float > cacc_promptLepInput_DL1mu("PromptLeptonInput_DL1mu");
static SG::AuxElement::ConstAccessor< float > cacc_promptLepInput_PtRel("PromptLeptonInput_PtRel");
static SG::AuxElement::ConstAccessor< float > cacc_promptLepInput_PtFrac("PromptLeptonInput_PtFrac");
static SG::AuxElement::ConstAccessor< float > cacc_promptLepInput_Topoetcone30Rel("PromptLeptonInput_Topoetcone30Rel");
static SG::AuxElement::ConstAccessor< float > cacc_promptLepInput_Ptvarcone30Rel("PromptLeptonInput_Ptvarcone30Rel");
static SG::AuxElement::ConstAccessor< float > cacc_promptLepVeto("PromptLeptonVeto"); 
static SG::AuxElement::ConstAccessor< float > cacc_LowPtPLV("LowPtPLV"); 
static SG::AuxElement::ConstAccessor< float > cacc_promptLepImprovedVetoElectrons("PromptLeptonImprovedVetoBARR");
static SG::AuxElement::ConstAccessor< float > cacc_promptLepImprovedVetoMuons("PromptLeptonImprovedVeto");

static SG::AuxElement::ConstAccessor< float > cacc_corr_ptvarcone20("ptvarcone20"); 
static SG::AuxElement::ConstAccessor< float > cacc_corr_ptvarcone30("ptvarcone30"); 
static SG::AuxElement::ConstAccessor< float > cacc_corr_ptvarcone40("ptvarcone40"); 
static SG::AuxElement::ConstAccessor< float > cacc_corr_ptvarcone30_TightTTVA_pt1000("ptvarcone30_TightTTVA_pt1000");
static SG::AuxElement::ConstAccessor< float > cacc_corr_ptvarcone30_TightTTVA_pt500("ptvarcone30_TightTTVA_pt500");
static SG::AuxElement::ConstAccessor< float > cacc_corr_ptvarcone20_TightTTVA_pt1000("ptvarcone20_TightTTVA_pt1000");
static SG::AuxElement::ConstAccessor< float > cacc_corr_ptvarcone20_TightTTVA_pt500("ptvarcone20_TightTTVA_pt500");
static SG::AuxElement::ConstAccessor< float > cacc_corr_ptcone20_TightTTVALooseCone_pt500("ptcone20_TightTTVALooseCone_pt500");
static SG::AuxElement::ConstAccessor< float > cacc_corr_ptcone20_TightTTVALooseCone_pt1000("ptcone20_TightTTVALooseCone_pt1000");
static SG::AuxElement::ConstAccessor< float > cacc_corr_ptcone20_TightTTVA_pt500("ptcone20_TightTTVA_pt500");
static SG::AuxElement::ConstAccessor< float > cacc_corr_ptcone20_TightTTVA_pt1000("ptcone20_TightTTVA_pt1000");
// static SG::AuxElement::ConstAccessor< float > cacc_corr_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000("ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000");
// static SG::AuxElement::ConstAccessor< float > cacc_corr_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000("ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000");
// static SG::AuxElement::ConstAccessor< float > cacc_corr_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500");
// static SG::AuxElement::ConstAccessor< float > cacc_corr_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500("ptcone20_Nonprompt_All_MaxWeightTTVA_pt500");
// static SG::AuxElement::ConstAccessor< float > cacc_corr_neflowisol20("neflowisol20"); 

static SG::AuxElement::ConstAccessor< float > cacc_ptcone20("ptcone20");
static SG::AuxElement::ConstAccessor< float > cacc_ptcone30("ptcone30");
static SG::AuxElement::ConstAccessor< float > cacc_ptcone40("ptcone40");
static SG::AuxElement::ConstAccessor< float > cacc_ptvarcone20("ptvarcone20");
static SG::AuxElement::ConstAccessor< float > cacc_ptvarcone30("ptvarcone30");
static SG::AuxElement::ConstAccessor< float > cacc_ptvarcone40("ptvarcone40");
static SG::AuxElement::ConstAccessor< float > cacc_ptcone20_TightTTVALooseCone_pt500("ptcone20_TightTTVALooseCone_pt500");
static SG::AuxElement::ConstAccessor< float > cacc_ptcone20_TightTTVALooseCone_pt1000("ptcone20_TightTTVALooseCone_pt1000");
static SG::AuxElement::ConstAccessor< float > cacc_ptcone20_TightTTVA_pt500("ptcone20_TightTTVA_pt500");
static SG::AuxElement::ConstAccessor< float > cacc_ptcone20_TightTTVA_pt1000("ptcone20_TightTTVA_pt1000");
static SG::AuxElement::ConstAccessor< float > cacc_ptvarcone30_TightTTVA_pt1000("ptvarcone30_TightTTVA_pt1000");
static SG::AuxElement::ConstAccessor< float > cacc_ptvarcone30_TightTTVA_pt500("ptvarcone30_TightTTVA_pt500");
static SG::AuxElement::ConstAccessor< float > cacc_ptvarcone20_TightTTVA_pt1000("ptvarcone20_TightTTVA_pt1000");
static SG::AuxElement::ConstAccessor< float > cacc_ptvarcone20_TightTTVA_pt500("ptvarcone20_TightTTVA_pt500");
static SG::AuxElement::ConstAccessor< float > cacc_topoetcone20("topoetcone20");
static SG::AuxElement::ConstAccessor< float > cacc_topoetcone30("topoetcone30");
static SG::AuxElement::ConstAccessor< float > cacc_topoetcone40("topoetcone40");
static SG::AuxElement::ConstAccessor< float > cacc_topoetcone20NonCoreCone("topoetcone20NonCoreCone");
static SG::AuxElement::ConstAccessor< float > cacc_topoetcone30NonCoreCone("topoetcone30NonCoreCone");
static SG::AuxElement::ConstAccessor< float > cacc_topoetcone40NonCoreCone("topoetcone40NonCoreCone");
static SG::AuxElement::ConstAccessor< float > cacc_SUSY20_topoetcone20("SUSY20_topoetcone20");
static SG::AuxElement::ConstAccessor< float > cacc_SUSY20_topoetcone30("SUSY20_topoetcone30");
static SG::AuxElement::ConstAccessor< float > cacc_SUSY20_topoetcone40("SUSY20_topoetcone40");
static SG::AuxElement::ConstAccessor< float > cacc_SUSY20_topoetcone20NonCoreCone("SUSY20_topoetcone20NonCoreCone");
static SG::AuxElement::ConstAccessor< float > cacc_SUSY20_topoetcone30NonCoreCone("SUSY20_topoetcone30NonCoreCone");
static SG::AuxElement::ConstAccessor< float > cacc_SUSY20_topoetcone40NonCoreCone("SUSY20_topoetcone40NonCoreCone");
static SG::AuxElement::ConstAccessor< float > cacc_ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000("ptvarcone30_Nonprompt_All_MaxWeightTTVALooseCone_pt1000");
static SG::AuxElement::ConstAccessor< float > cacc_ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000("ptcone20_Nonprompt_All_MaxWeightTTVALooseCone_pt1000");
static SG::AuxElement::ConstAccessor< float > cacc_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500("ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500");
static SG::AuxElement::ConstAccessor< float > cacc_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500("ptcone20_Nonprompt_All_MaxWeightTTVA_pt500");
static SG::AuxElement::ConstAccessor< float > cacc_neflowisol20("neflowisol20");

static SG::AuxElement::ConstAccessor< char > cacc_lhveryloose ("DFCommonElectronsLHVeryLoose");
static SG::AuxElement::ConstAccessor< char > cacc_lhloose     ("DFCommonElectronsLHLoose");
static SG::AuxElement::ConstAccessor< char > cacc_lhlooseBL   ("DFCommonElectronsLHLooseBL");
static SG::AuxElement::ConstAccessor< char > cacc_lhmedium    ("DFCommonElectronsLHMedium");
static SG::AuxElement::ConstAccessor< char > cacc_lhtight     ("DFCommonElectronsLHTight");
static SG::AuxElement::ConstAccessor< char > cacc_dnnloose    ("DFCommonElectronsDNNLoose");
static SG::AuxElement::ConstAccessor< char > cacc_dnnmedium   ("DFCommonElectronsDNNMedium");
static SG::AuxElement::ConstAccessor< char > cacc_dnntight    ("DFCommonElectronsDNNTight");

static SG::AuxElement::ConstAccessor< char > cacc_passDRcut     ("passDRcut");

static SG::AuxElement::ConstAccessor< unsigned char > cacc_ambiguityType("ambiguityType");
static SG::AuxElement::ConstAccessor< int > cacc_addAmbiguity("DFCommonAddAmbiguity");

static SG::AuxElement::ConstAccessor< char >   cacc_electronsECIDS ("DFCommonElectronsECIDS");
static SG::AuxElement::ConstAccessor< double > cacc_electronsECIDSResult ("DFCommonElectronsECIDSResult");
static SG::AuxElement::ConstAccessor< float > cacc_d0wrtSV( "d0_wrtSV" );
static SG::AuxElement::ConstAccessor< float > cacc_z0wrtSV( "z0_wrtSV" );
static SG::AuxElement::ConstAccessor< float > cacc_ptwrtSV( "pt_wrtSV" );
static SG::AuxElement::ConstAccessor< float > cacc_etawrtSV( "eta_wrtSV" );
static SG::AuxElement::ConstAccessor< float > cacc_phiwrtSV( "phi_wrtSV" );
static SG::AuxElement::ConstAccessor< float > cacc_errD0wrtSV( "errD0_wrtSV" );
static SG::AuxElement::ConstAccessor< float > cacc_errZ0wrtSV( "errZ0_wrtSV" );
static SG::AuxElement::ConstAccessor< float > cacc_errPwrtSV( "errP_wrtSV" );


static SG::AuxElement::ConstAccessor< float > cacc_TP_trk0_d0( "TP_trk0_d0" );
static SG::AuxElement::ConstAccessor< float > cacc_TP_trk0_z0( "TP_trk0_z0" );
static SG::AuxElement::ConstAccessor< float > cacc_TP_trk1_d0( "TP_trk1_d0" );
static SG::AuxElement::ConstAccessor< float > cacc_TP_trk1_z0( "TP_trk1_z0" );

static SG::AuxElement::ConstAccessor< float > cacc_KVUd0( "KVUd0" );
static SG::AuxElement::ConstAccessor< float > cacc_KVUtheta( "KVUtheta" );
static SG::AuxElement::ConstAccessor< float > cacc_KVUqOverP( "KVUqOverP" );
static SG::AuxElement::ConstAccessor< float > cacc_KVUphi( "KVUphi" );

static SG::AuxElement::ConstAccessor< int > cacc_SUSYFinalState( "SUSY_procID" );
static SG::AuxElement::ConstAccessor< int > cacc_SUSYPID1( "SUSY_pid1" );
static SG::AuxElement::ConstAccessor< int > cacc_SUSYPID2( "SUSY_pid2" );

static SG::AuxElement::ConstAccessor< bool > cacc_SmoothWContained80_Tagged( "SmoothWContained80_Tagged" );
static SG::AuxElement::ConstAccessor< bool > cacc_SmoothWContained80_PassMass( "SmoothWContained80_PassMass" );
static SG::AuxElement::ConstAccessor< bool > cacc_SmoothWContained80_PassD2( "SmoothWContained80_PassD2" );
static SG::AuxElement::ConstAccessor< bool > cacc_SmoothWContained80_PassNtrk( "SmoothWContained80_PassNtrk" );
static SG::AuxElement::ConstAccessor< float > cacc_SmoothWContained80_SF( "SmoothWContained80_SF" );

static SG::AuxElement::ConstAccessor< bool > cacc_SmoothZContained80_Tagged( "SmoothZContained80_Tagged" );
static SG::AuxElement::ConstAccessor< bool > cacc_SmoothZContained80_PassMass( "SmoothZContained80_PassMass" );
static SG::AuxElement::ConstAccessor< bool > cacc_SmoothZContained80_PassD2( "SmoothZContained80_PassD2" );
static SG::AuxElement::ConstAccessor< bool > cacc_SmoothZContained80_PassNtrk( "SmoothZContained80_PassNtrk" );
static SG::AuxElement::ConstAccessor< float > cacc_SmoothZContained80_SF( "SmoothZContained80_SF" );



template<class T>
const T* getLink(const xAOD::IParticle* particle, std::string name){
  if (!particle) return 0;
  typedef ElementLink< DataVector<T> > Link_t;

  if (!particle->isAvailable< Link_t >(name) ) {
    return 0;
  }
  const Link_t link = particle->auxdata<Link_t>(name);
  if (!link.isValid()) {
    return 0;
  }
  return *link;
}



#endif
