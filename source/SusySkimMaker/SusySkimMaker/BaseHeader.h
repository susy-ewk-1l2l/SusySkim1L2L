#ifndef SusySkimMaker_BaseHeader_h
#define SusySkimMaker_BaseHeader_h


// Rootcore
// #include "SusySkimMaker/TreeMaker.h"
#include "SusySkimMaker/MetaData.h"


// Systematics
#include "PATInterfaces/SystematicSet.h"

// Root
#include "TTree.h"
#include "TMath.h"

#endif
