

/*
  All units should be in MeV (same as AOD)
*/

namespace Constants
{

  // http://pdg.lbl.gov/2014/listings/rpp2014-list-muon.pdf
  const float MUON_MASS =  105.6583715;

  // http://pdg.lbl.gov/2012/listings/rpp2012-list-electron.pdf
  const float ELECTRON_MASS = 0.510998928;


}
