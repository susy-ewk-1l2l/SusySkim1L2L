#ifndef SusySkimMaker_TREEMAKER_H
#define SusySkimMaker_TREEMAKER_H

/*
 *
 * Class needs to be rewritten with templates.
 *
 * Written by Matthew Gignac
 *
 * Basic class to create TTrees for the xAODNtupleAnalysis package
 *
 *   Stream: defines a set out output trees with a common prefix
 *   Sys: name from systematics, for a given stream, you can have any number of systematics
 *
 *   For example: stream #1: skims, systematics muonSF_up muonSF_down, etc
 *                stream #1: trees, systematics muonSF_up muonSF_down, etc
 *
 *                These trees will appear as skims_muonSF_up, skims_muonSF_down, same for trees
 *
 */


// C++
#include <vector>
#include <utility>

// ROOT
#include "TTree.h"
#include "TObjString.h"
#include "TLorentzVector.h"
#include "TVector3.h"

// RootCore
#include "SusySkimMaker/CutFlowTool.h"
#include "SusySkimMaker/MsgLog.h"

class Objects;

typedef std::map<TString,TTree*> treeMap; 

class TreeMaker
{


 private:

  ///
  /// Private method. Create an output tree for each entry in m_sysStringVector; i.e. the list the user passed 
  /// to the constructor. Called by createTrees(TFile*& file, TString stream="").
  ///
  void createTrees(TString stream);

  ///
  /// Private method. If m_globalStream is set, returns a vector with only this as an entry, otherwise returns all stream, global m_stream
  ///
  std::vector<TString> getStream();

  ///
  /// Private method. If m_sysName is set, returns a vector with only this as an entry, otherwise returns all systematics, global m_sysStringVector.
  ///
  std::vector<TString> getSys();
 
  ///
  /// Formatting method for tree names and branch names
  ///
  TString getFinalTreeName(TString prefix, TString sys);

  ///
  /// Format branch name in case of invalid C++ variable names
  /// (i.e. replace '-' with '_')
  ///
  TString formatBranchName(TString branchName);


 public:

  ///
  /// Constructor; requires as an argument a list of systematics. Will create a TTree for each 
  /// entry in the systematic list when you call createTrees
  ///
  TreeMaker();

  ///
  /// Destructor
  ///
  ~TreeMaker();

  ///
  /// Reset all global variables
  ///
  void Reset();

  ///
  /// Default variables to be put into all trees. 
  ///
  void addDefaultVariables();

  //void setDefaultVariables(unsigned long long PRWHash, int DSID, unsigned long long evtNum, int runNum, float xSec, float trueHt);
  void setDefaultVariables(Objects* obj);

  ///
  /// Wrapper for private createTrees(TString stream=""). Saves the TFile into a global map for later retrieval
  /// and use via TreeMaker::addOuputFile(...). The TFile can be retrieved through TreeMaker::getFile(...)
  ///  
  void createTrees(TFile*& file, TString stream="");

  ///
  /// Wrapper for private createTrees(TString stream=""). Similar to createTrees(TFile*& file, TString stream=""),
  /// except creates a TFile if need be.
  ///  
  void createTrees(TString file, TString stream);

  ///
  /// Add an output file into global map. Requies file to be alive.
  ///
  void addOutputFile(TString fileName,TFile* file);

  ///
  /// Add an output file into global map. First checks if a TFile named file already exists 
  /// in global map. If it does, it uses it and copies it and insert it into the global map
  /// with name fileName. TODO. Revisit this, ends up with two copies of the same TFile
  ///
  void addOutputFile(TString fileName,TString file);

  /// 
  /// Get a file from global map. Aborts in the event the file is not found.
  ///
  TFile* getFile(TString stream);

  ///
  /// Get TTree from global map.
  ///
  TTree* getTree(TString prefix,TString sysName);

  ////////////////////////////////////////////////////
  ///
  /// Add variables to TTrees
  ///
  ////////////////////////////////////////////////////
  // For variables with non-trivial default values
  template <class T>
    inline void addVariable(TString branchName, T defaultValue, std::map<TString,std::pair<T,T> > &cutDic, bool nominalOnly=false)
    {
      std::vector<TString> sysStringVector = getSys();
      std::vector<TString> streams         = getStream();  
      
      for(unsigned int i=0; i<streams.size();i++){
        for(unsigned int s=0; s<sysStringVector.size(); s++){
          if(nominalOnly && !(sysStringVector[s]=="" || sysStringVector[s]=="LOOSE_NOMINAL")) continue;
          TString finalTreeName = getFinalTreeName( streams[i], sysStringVector[s] );
          
          // Get corresponding tree
          std::map<TString,TTree*>::iterator it = m_trees.find(finalTreeName);	
          if(it==m_trees.end()){
            std::cout << "TreeMaker::WARNING::Could not find tree for systematic: " << finalTreeName << std::endl;
            continue;
          }
          // Naming structure: branchName_sysName
          TString name = getFinalTreeName(branchName, sysStringVector[s]);	  
          cutDic.insert( std::pair<TString,std::pair<T,T> >(name, std::pair<T,T>(defaultValue,defaultValue)) );
          
          // Get what we just inserted above
          typename std::map<TString,std::pair<T,T> >::iterator cuts;
          cuts = cutDic.find(name);
          
          // Link to corresponding tree
          it->second->Branch(branchName,&cuts->second.first);
        }
      }
    }

  inline void addFloatVariable(TString branchName,float defaultValue,bool nominalOnly=false){
    addVariable(branchName, defaultValue, m_cutDic_float, nominalOnly);
  }
  inline void addDoubleVariable(TString branchName,double defaultValue,bool nominalOnly=false){
    addVariable(branchName, defaultValue, m_cutDic_double, nominalOnly);
  }
  inline void addIntVariable(TString branchName,int defaultValue,bool nominalOnly=false){
    addVariable(branchName, defaultValue, m_cutDic_int, nominalOnly);
  }
  inline void addStringVariable(TString branchName,TString defaultValue,bool nominalOnly=false){
    addVariable(branchName, defaultValue, m_cutDic_TString, nominalOnly);
  }
  inline void addBoolVariable(TString branchName,bool defaultValue,bool nominalOnly=false){
    addVariable(branchName, defaultValue, m_cutDic_bool, nominalOnly);
  }
  inline void addULongLongVariable(TString branchName,unsigned long long defaultValue,bool nominalOnly=false){
    addVariable(branchName, defaultValue, m_cutDic_ulonglong, nominalOnly);
  }  

  // Add many branches to output trees, all with the same inital value
  void addFloatVariables( TString prefix, std::vector<TString> branches, float init_value, bool nominalOnly=false );
  void addBoolVariables( std::vector<TString> branches, bool init_value, bool nominalOnly=false );

  ////////////////////////////////////////////////////
  // For variables with trivial default values
  template <class T>
    inline void addVariable(TString branchName, T defaultValue, std::map<TString,T> &cutDic, bool nominalOnly=false)
    {
      std::vector<TString> sysStringVector = getSys();
      std::vector<TString> streams         = getStream();  
      
      for(unsigned int i=0; i<streams.size();i++){
        for(unsigned int s=0; s<sysStringVector.size(); s++){
          if(nominalOnly && !(sysStringVector[s]=="" || sysStringVector[s]=="LOOSE_NOMINAL")) continue;
          TString finalTreeName = getFinalTreeName( streams[i], sysStringVector[s] );
          
          // Get corresponding tree
          std::map<TString,TTree*>::iterator it = m_trees.find(finalTreeName);	
          if(it==m_trees.end()){
            std::cout << "TreeMaker::WARNING::Could not find tree for systematic: " << finalTreeName << std::endl;
            continue;
          }
          // Naming structure: branchName_sysName
          TString name = getFinalTreeName(branchName, sysStringVector[s]);	  
          cutDic.insert( std::pair<TString,T>(name,defaultValue) );
          
          // Get what we just inserted above
          typename std::map<TString,T>::iterator cuts;
          cuts = cutDic.find(name);
          
          // Link to corresponding tree
          it->second->Branch(branchName,&cuts->second);
        }
      }
    }

  inline void addTLVariable(TString branchName,bool nominalOnly=false){
    //TLorentzVector. Default: (0,0,0,0)
    addVariable(branchName, TLorentzVector(0,0,0,0), m_cutDic_tlv, nominalOnly);
  }
  inline void addTV3Variable(TString branchName,bool nominalOnly=false){
	  //TVector3. Default: (0,0,0)
	  addVariable(branchName, TVector3(0,0,0), m_cutDic_tv3, nominalOnly);
  }  
  inline void addVecFloatVariable(TString branchName, bool nominalOnly=false){
    addVariable(branchName, std::vector<float>(), m_cutDic_vecFloat, nominalOnly);
  }
  inline void addVecIntVariable(TString branchName, bool nominalOnly=false){
    addVariable(branchName, std::vector<int>(), m_cutDic_vecInt, nominalOnly);
  }
  inline void addVecBoolVariable(TString branchName, bool nominalOnly=false){
    addVariable(branchName, std::vector<bool>(), m_cutDic_vecBool, nominalOnly);
  }
  inline void addVecTStringVariable(TString branchName, bool nominalOnly=false){
    addVariable(branchName, std::vector<TString>(), m_cutDic_vecTString, nominalOnly);
  }

  ////////////////////////////////////////////////////
  /// 
  /// Set variables in TTrees
  ///
  ////////////////////////////////////////////////////
  // For variables with non-trivial default values
  template <class T>
    inline void setVariable(TString branchName, T value, std::map<TString, std::pair<T,T> > &cutDic, TString sysName="")
    {
      if(m_sysName!="None") sysName=m_sysName;
      
      TString name = getFinalTreeName( branchName, sysName );
      typename std::map<TString,std::pair<T,T> >::iterator it;
      it = cutDic.find(name);
      
      if( it != cutDic.end() )  it->second.first = value;  
      else if(cutDic.find(getFinalTreeName( branchName, "" )) != cutDic.end()){
	      // Don't shout if this branch is meant to be just for the noSys tree
      }
      else{
        MsgLog::WARNING("TreeMaker::setVariable", "Unknown class instance %s", name.Data());
      }
    }

  inline void setFloatVariable(TString branchName, float value, TString sysName=""){
    setVariable(branchName, value, m_cutDic_float, sysName);
  }
  inline void setDoubleVariable(TString branchName, double value, TString sysName=""){
    setVariable(branchName, value, m_cutDic_double, sysName);
  }
  inline void setIntVariable(TString branchName, int value, TString sysName=""){
    setVariable(branchName, value, m_cutDic_int, sysName);
  }
  inline void setStringVariable(TString branchName, TString value, TString sysName=""){
	setVariable(branchName, value, m_cutDic_TString, sysName);
  }
  inline void setBoolVariable(TString branchName, bool value, TString sysName=""){
    setVariable(branchName, value, m_cutDic_bool, sysName);
  }
  inline void setULongLongVariable(TString branchName, unsigned long long value, TString sysName=""){
    setVariable(branchName, value, m_cutDic_ulonglong, sysName);
  }  

  ////////////////////////////////////////////////////
  // For variables with trivial default values
  template <class T>
    inline void setVariable(TString branchName, T value, std::map<TString, T> &cutDic, TString sysName="")
    {
      if(m_sysName!="None") sysName=m_sysName;
      
      TString name = getFinalTreeName( branchName, sysName );
      typename std::map<TString,T>::iterator it;
      it = cutDic.find(name);
      
      if( it != cutDic.end() )  it->second = value;  
      else if(cutDic.find(getFinalTreeName( branchName, "" )) != cutDic.end()){
	      // Don't shout if this branch is meant to be just for the noSys tree
      }
      else{
        MsgLog::WARNING("TreeMaker::setVariable", "Unknown class instance %s", name.Data());
      }
    }

  inline void setTLVariable(TString branchName, TLorentzVector value, TString sysName=""){
    setVariable(branchName, value, m_cutDic_tlv, sysName);
  }
  inline void setTV3Variable(TString branchName, TVector3 value, TString sysName=""){
	  setVariable(branchName, value, m_cutDic_tv3, sysName);
  }
  inline void setVecTV3Variable(TString branchName, std::vector<TVector3> value, TString sysName=""){
	  setVariable(branchName, value, m_cutDic_vecTv3, sysName);
  }
  inline void setVecFloatVariable(TString branchName, std::vector<float> value, TString sysName=""){
    setVariable(branchName, value, m_cutDic_vecFloat, sysName);
  }
  inline void setVecIntVariable(TString branchName, std::vector<int> value, TString sysName=""){
    setVariable(branchName, value, m_cutDic_vecInt, sysName);
  }
  inline void setVecBoolVariable(TString branchName, std::vector<bool> value, TString sysName=""){
    setVariable(branchName, value, m_cutDic_vecBool, sysName);
  }
  inline void setVecTStringVariable(TString branchName, std::vector<TString> value, TString sysName=""){
    setVariable(branchName, value, m_cutDic_vecTString, sysName);
  }

  ////////////////////////////////////////////////////
  /// 
  /// Get the value of variable registered to the branch
  ///
  ////////////////////////////////////////////////////
  // For variables with non-trivial default values
  template <class T>
    inline bool getVariable(TString branchName, T &result, std::map<TString, std::pair<T,T> > &cutDic, TString sysName="")
    {
      if(m_sysName!="None") sysName=m_sysName;
      
      TString name = getFinalTreeName( branchName, sysName );
      typename std::map<TString,std::pair<T,T> >::iterator it;
      it = cutDic.find(name);
      
      if( it != cutDic.end() ) {
	      result = it->second.first;  
	      return true;
      }
      else if(cutDic.find(getFinalTreeName( branchName, "" )) != cutDic.end()){
	      // Don't shout if this branch is meant to be just for the noSys tree
      }
      else{
        MsgLog::WARNING("TreeMaker::getVariable", "Unknown branch %s", name.Data());
      }

      return false;
    }

  inline float getFloatVariable(TString branchName, TString sysName=""){
    float value=0.;
    getVariable(branchName, value, m_cutDic_float, sysName); return value;
  }
  inline double getDoubleVariable(TString branchName, TString sysName=""){
    double value=0.;
    getVariable(branchName, value, m_cutDic_double, sysName); return value;
  }
  inline int getIntVariable(TString branchName, TString sysName=""){
    int value=0;
    getVariable(branchName, value, m_cutDic_int, sysName); return value;
  }
  inline bool getBoolVariable(TString branchName, TString sysName=""){
    bool value=false;
    getVariable(branchName, value, m_cutDic_bool, sysName); return value;
  }
  inline unsigned long long getULongLongVariable(TString branchName, TString sysName=""){
    unsigned long long value=0;
    getVariable(branchName, value, m_cutDic_ulonglong, sysName); return value;
  }  

  // For variables with trivial default values
  template <class T>
    inline bool getVariable(TString branchName, T &result, std::map<TString, T> &cutDic, TString sysName="")
    {
      if(m_sysName!="None") sysName=m_sysName;
      
      TString name = getFinalTreeName( branchName, sysName );
      typename std::map<TString,T>::iterator it;
      it = cutDic.find(name);
      
      if( it != cutDic.end() ) {
	      result = it->second;  
	      return true;
      }
      else if(cutDic.find(getFinalTreeName( branchName, "" )) != cutDic.end()){
	      // Don't shout if this branch is meant to be just for the noSys tree
      }
      else{
        MsgLog::WARNING("TreeMaker::getVariable", "Unknown branch %s", name.Data());
      }

      return false;
    }

  inline TLorentzVector getTLVariable(TString branchName, TString sysName=""){
    TLorentzVector value(0,0,0,0);
    getVariable(branchName, value, m_cutDic_tlv, sysName); return value;
  }
  inline TVector3 getTV3Variable(TString branchName, TString sysName=""){
    TVector3 value(0,0,0);
    getVariable(branchName, value, m_cutDic_tv3, sysName); return value;
  }
  inline std::vector<float> getVecFloatVariable(TString branchName, TString sysName=""){
    std::vector<float> value;
    getVariable(branchName, value, m_cutDic_vecFloat, sysName); return value;
  }
  inline std::vector<int> getVecIntVariable(TString branchName, TString sysName=""){
    std::vector<int> value;
    getVariable(branchName, value, m_cutDic_vecInt, sysName); return value;
  }
  inline std::vector<bool> getVecBoolVariable(TString branchName, TString sysName=""){
    std::vector<bool> value;
    getVariable(branchName, value, m_cutDic_vecBool, sysName); return value;
  }
  inline std::vector<TString> getVecTStringVariable(TString branchName, TString sysName=""){
    std::vector<TString> value;
    getVariable(branchName, value, m_cutDic_vecTString, sysName); return value;
  }

  ////////////////////////////////////////////////////
  ///
  /// Append an element to a vector branch
  ///
  ////////////////////////////////////////////////////
  void appendVecFloatVariable(TString branchName, float value,TString sysName=""){
    std::vector<float> vec;
    if( getVariable(branchName, vec, m_cutDic_vecFloat, sysName) ){
      vec.push_back(value);
      setVariable(branchName, vec, m_cutDic_vecFloat, sysName);      
    }
  }
  void appendVecIntVariable(TString branchName, int value,TString sysName=""){
    std::vector<int> vec;
    if( getVariable(branchName, vec, m_cutDic_vecInt, sysName) ){
      vec.push_back(value);
      setVariable(branchName, vec, m_cutDic_vecInt, sysName);      
    }
  }
  void appendVecTV3Variable(TString branchName, TVector3 value,TString sysName=""){
	std::vector<TVector3> vec;
	if( getVariable(branchName, vec, m_cutDic_vecTv3, sysName) ){
	  vec.push_back(value);
	  setVariable(branchName, vec, m_cutDic_vecTv3, sysName);
    }
  }
  void appendVecBoolVariable(TString branchName, bool value,TString sysName=""){
    std::vector<bool> vec;
    if( getVariable(branchName, vec, m_cutDic_vecBool, sysName) ){
      vec.push_back(value);
      setVariable(branchName, vec, m_cutDic_vecBool, sysName);      
    }
  }
  void appendVecTStringVariable(TString branchName, TString value,TString sysName=""){
    std::vector<TString> vec;
    if( getVariable(branchName, vec, m_cutDic_vecTString, sysName) ){
      vec.push_back(value);
      setVariable(branchName, vec, m_cutDic_vecTString, sysName);      
    }
  }

  ////////////////////////////////////////////////////
  ///
  /// Reset the values in a cutDic to the default
  ///
  ////////////////////////////////////////////////////
  template <class T>
    inline void resetVariable(std::map<TString, std::pair<T,T> > &cutDic){
    typename std::map<TString, std::pair<T,T> >::iterator it;
    for(it = cutDic.begin(); it != cutDic.end(); it++) it->second.first = it->second.second;
  }
  template <class T>
    inline void resetVariable(std::map<TString,T> &cutDic, T defaultValue){
    typename std::map<TString, T>::iterator it;
    for(it = cutDic.begin(); it != cutDic.end(); it++) it->second = defaultValue;
  }
  template <class T>
    inline void resetVecVariable(std::map<TString, T> &cutDic){
    typename std::map<TString, T>::iterator it;
    for(it = cutDic.begin(); it != cutDic.end(); it++) it->second.clear();
  }


  ///
  /// Write all TFiles found in global map.
  ///
  void Write();

  ///
  /// Fill a TTree with stream and sysName. If stream is not set, uses m_streams, i.e. all created streams.
  ///
  void Fill(TString sysName,TString stream="");

  /// 
  /// Return global TTree map
  ///
  const std::map<TString,TTree*>& getTreeMap() const {return m_trees;}

  ///
  /// Return global systematic list. Same one user uses in constructor
  ///
  const std::vector<TString>& getSysVector() const {return m_sysStringVector;}

  ///
  /// Assign global systematic vector. Overrides one set in constructor.
  ///
  void setSysStringVector(std::vector<TString> sys){m_sysStringVector=sys;}

  ///
  /// Set global tree state. When this is set, only this systematic is considered, 
  /// instead of the full set found in m_sysStringVector.
  ///
  void setTreeState(TString sysName){m_sysName=sysName;}

  ///
  /// Return m_sysName, global treeState
  ///
  const TString getTreeState() const {return m_sysName;}

  ///
  /// Set global stream state. When this is set, only this stream is considered,
  /// instead of the full set found in m_streams.
  ///
  void setStreamState(TString stream){m_globalStream=stream;}

  ///
  /// Return log of configuration of this tool. Note really used yet.
  ///
  const std::vector< std::pair<std::string,float> > getLog(){return m_log;}

  ///
  /// Write out LHE3 weights into the output tree or not? The format can be specified by the argument 'fillFlatBranches' (set to true by default).
  ///  fillFlatBranches=true:  write out flat branches (a float branch per weight) 
  ///  fillFlatBranches=false: write out vector of weights (one vector<float> branch 'LHE3WeightNames' and one vector<TString> branch 'LHE3WeightNames' per event) 
  ///
  void setWriteLHEWeights(bool writeLHE3=true, bool fillFlatBranches=true){ 
    m_writeLHE3      = writeLHE3; 
    m_writeLHE3_flat = fillFlatBranches; 
  };

 protected:

  /// 
  /// Global systematic name, if this is set tools will use this single entry instead of everything
  /// found in m_sysStringVector.
  ///
  TString m_sysName;

  ///
  /// Global stream name, if this is set methods will use this single entry instead of everything 
  /// found in m_streams.
  ///
  TString m_globalStream;

  ///
  /// Global systematic vector, containing all systematics that are being processed
  ///
  std::vector<TString> m_sysStringVector;

  ///
  /// Global stream vector, contains all streams. I.e. everytime createTrees is called, that stream
  /// name is saved in this vector.
  ///
  std::vector<TString> m_streams;

  ///
  /// Storage for branches
  ///
  std::map<TString, std::pair<float,float> >    m_cutDic_float;
  std::map<TString, std::pair<double,double> >  m_cutDic_double;
  std::map<TString, std::pair<int,int> >        m_cutDic_int;
  std::map<TString, std::pair<TString,TString> > m_cutDic_TString;
  std::map<TString, std::pair<bool,bool> >      m_cutDic_bool;
  std::map<TString, std::pair<unsigned long long,unsigned long long> > m_cutDic_ulonglong;

  std::map<TString,TLorentzVector>        m_cutDic_tlv;
  std::map<TString,TVector3>              m_cutDic_tv3;

  std::map<TString,std::vector<TVector3> > m_cutDic_vecTv3;
  std::map<TString,std::vector<int> >      m_cutDic_vecInt;
  std::map<TString,std::vector<float> >    m_cutDic_vecFloat;
  std::map<TString,std::vector<bool> >     m_cutDic_vecBool;
  std::map<TString,std::vector<TString> >  m_cutDic_vecTString;

  /// 
  /// Global map for TFiles
  ///
  std::map<TString,TFile*> m_outputFiles;

  /// 
  /// Global map for TTrees, created in createTrees.
  ///
  std::map<TString,TTree*> m_trees;

  /// 
  /// Log file to store information about this class and how it was configured. 
  /// Largely undeveloped and not used yet...
  ///
  std::vector< std::pair<std::string,float> > m_log;

  ///
  /// When true, write out the LHE3 event weights. Steered in deep.conf
  ///  
  bool m_writeLHE3;

  ///
  /// When true, write out the flat LHE3 event weight branches (branch per variation), if not, write out a vector of weights (std::vector<float>) per event
  ///
  bool m_writeLHE3_flat;

  ///
  /// Flag for on-the-fly branch definition. Before (true) or after (false) the first fill of th edefault branches.
  ///   
  bool m_isFirstFill;
  
};

#endif
