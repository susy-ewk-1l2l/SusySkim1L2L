#ifndef SusySkimMaker_TruthVariable_h
#define SusySkimMaker_TruthVariable_h

#include "SusySkimMaker/ObjectVariable.h"

class TruthVariable : public ObjectVariable
{

 public:
  TruthVariable();
  virtual ~TruthVariable(){};

  TruthVariable(const TruthVariable&);
  TruthVariable& operator=(const TruthVariable&);

  int q;
  int pdgId;
  int status;
  int barcode;
  int parentPdgId;
  int parentBarcode;

  float decayVtxPerp;
  float prodVtxPerp;

  bool isAntiKt4Jet;
  bool bjet;

  TLorentzVector bareTLV;

  void print(); //!

  ClassDef(TruthVariable, 2);

};

#endif
