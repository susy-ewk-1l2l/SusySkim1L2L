#ifndef SusySkimMaker_TauVariable_h
#define SusySkimMaker_TauVariable_h

// Rootcore
#include "SusySkimMaker/LeptonVariable.h"

class TauVariable : public LeptonVariable
{

 public:

  ///
  /// Default constructor
  ///
  TauVariable();

  ///
  /// Destructor
  ///
  virtual ~TauVariable(){};

  ///
  /// Copy constructor
  ///
  TauVariable(const TauVariable&);

  ///
  /// Assignment operator
  ///
  TauVariable& operator=(const TauVariable&);

  ///
  /// Number of tracks associated with this tau, can be 1 or 3-prong taus
  ///
  int nTrack;
  int nTrackCharged;

  ///
  /// Quality of the tau
  ///
  bool loose;
  bool medium;
  bool tight;

  ClassDef(TauVariable, 6);

};

#endif
