#ifndef SusySkimMaker_ElectronVariable_h
#define SusySkimMaker_ElectronVariable_h

// Rootcore
#include "SusySkimMaker/LeptonVariable.h"

class ElectronVariable : public LeptonVariable 
{

 public:

  /// 
  /// Default constructor
  ///
  ElectronVariable();

  ///
  /// Destructor
  ///
  virtual ~ElectronVariable(){};
 
  ///
  /// Copy constructor
  ///
  ElectronVariable(const ElectronVariable&);

  ///
  /// Assignment operator
  ///
  ElectronVariable& operator=(const ElectronVariable&);

  ///
  /// Medium quality electron: cut based
  ///
  bool medium;

  ///
  /// Tight quality electron: cut based
  ///
  bool tight;

  ///
  /// Tight quality electron: likelihood based, from electron selector tools
  ///
  bool tightllh;

  ///
  /// Medium quality electron: likelihood based, from electron selector tools
  ///
  bool mediumllh;

  ///
  /// LooseBL quality electron: likelihood based, from electron selector tools
  ///
  bool looseBLllh;

  ///
  /// Loose quality electron: likelihood based, from electron selector tools
  ///
  bool loosellh;

  ///
  /// VeryLoose quality electron: likelihood based, from electron selector tools
  ///
  bool veryloosellh;

  ///
  /// LH discriminant value
  ///
  float LHScore;

  ///
  /// Tight quality electron: DNN based, from electron selector tools
  ///
  bool tightdnn;

  ///
  /// Medium quality electron: DNN based, from electron selector tools
  ///
  bool mediumdnn;

  ///
  /// Loose quality electron: DNN based, from electron selector tools
  ///
  bool loosednn;

  ///
  /// Was there a hit in the innermost pixel layer?
  /// If it is masked off, look at the next layer
  ///
  bool passBL;
  
  ///
  /// nPix+nPixDeadSensors
  ///
  uint8_t nPixHitsPlusDeadSensors;

  ///
  /// electron/photon mother's truth information, in case of e->gamma->e chains
  /// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaTruthRun2#Background_Electron_Classificati
  ///

  int egMotherType;
  int egMotherOrigin;
  int egMotherPdgId;

  ///
  /// Electron Charge ID MVA ("charge-flip killer")
  /// https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EgammaChargeMisIdentificationTool
  ///
  bool  ECIDS;
  float ECIDScore;

  int ambiguityType;
  int addAmbiguity;

  ///
  /// ID operating points
  /// Note that Loose here is not Loose+BL, which can be used
  /// via el->passesLoose() && el->passBL
  ///
  bool passesTight()     const { return tightllh; }
  bool passesMedium()    const { return mediumllh; }
  bool passesLoose()     const { return loosellh; }
  bool passesVeryLoose() const { return veryloosellh; }
  bool passesTightDNN()     const { return tightdnn; }
  bool passesMediumDNN()    const { return mediumdnn; }
  bool passesLooseDNN()     const { return loosednn; }

  ///
  /// Always returns true, these are electrons
  ///
  bool isEle() const { return true; } //!
 
  /// 
  /// Always returns false, not electrons
  ///
  bool isMu()  const { return false; } //!

  ClassDef(ElectronVariable, 6);

};

#endif
