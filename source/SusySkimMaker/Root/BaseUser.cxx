#include "SusySkimMaker/BaseUser.h"
#include "SusySkimMaker/MsgLog.h"

// ---------------------------------------------------------- //
BaseUser::BaseUser(TString packageName, TString selectorName) : m_selectorName(selectorName),
                                                                m_packageName(packageName),
                                                                m_sampleName(""),
                                                                m_truthOnly(false),
                                                                m_disableCuts(false)
{

  MsgLog::INFO( "BaseUser::BaseUser","Adding a selector %s to package %s",selectorName.Data(),packageName.Data() );
  addToList(this);

}
// ---------------------------------------------------------- //
BaseUser* getAnalysisSelector(TString selectorName)
{

  BaseUser* UserSelector = 0;

  //
  for( const auto& anaSel : UserSelector->getList() ){
    if( (anaSel->getSelectorName()).EqualTo(selectorName) ){
      UserSelector = anaSel;
      MsgLog::INFO("BaseUser::getAnalysisSelector","Found analysis selector %s!", selectorName.Data() );
      break;
    }
  }

  // TODO: Decide how we want to handle this. Abort??
  if( !UserSelector ){
    MsgLog::ERROR("BaseUser::getAnalysisSelector","Could not find analysis selector %s!!!", selectorName.Data() );	
    abort();
  }

  // Return gracefully
  return UserSelector;

}


