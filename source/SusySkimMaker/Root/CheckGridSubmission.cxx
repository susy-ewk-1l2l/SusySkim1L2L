#include "SusySkimMaker/CheckGridSubmission.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "AsgMessaging/StatusCode.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/MsgLog.h"
#include "PathResolver/PathResolver.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TSystem.h"


// ------------------------------------------------------------------
CheckGridSubmission::CheckGridSubmission() : m_scope("mc15_13TeV"), 
                                             m_disablePRW(false),
                                             m_checkStatus(false),
                                             m_checkGridInfo(true)
{
}
// ------------------------------------------------------------------ //
StatusCode CheckGridSubmission::initialize()
{

  const char* APP_NAME = "CheckGridSubmission";

  // R-tag numbers for PRW checks
  CHECK( readRtagRunNumbers() );

  // Turn on/off AMI/Rucio checks
  CHECK( checkGridProxy() );

  // Flags
  CentralDB::retrieve(CentralDBFields::DISABLEPRW,m_disablePRW);

  // Logs for the checked samples
  m_goodJobs = new JobStatus();
  m_badJobs  = new JobStatus();

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------ //
StatusCode CheckGridSubmission::checkGridProxy()
{
  //
  MsgLog::INFO("CheckGridSubmission::checkGridProxy","Checking for a grid-proxy...");

  // Check if we have a valid grid proxy, otherwise shut-off AMI/Rucio checks
  if( gSystem->Exec("voms-proxy-info")==0 ){
    MsgLog::INFO("CheckGridSubmission::checkGridProxy","Found a grid-proxy! Proceeding with AMI/RUCIO checks!");
    m_checkGridInfo = true;
  }
  else{
    MsgLog::WARNING("CheckGridSubmission::checkGridProxy","No grid proxy detected! Shutting off AMI/Rucio checks!");
    m_checkGridInfo = false;
  }

  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------ //
StatusCode CheckGridSubmission::readRtagRunNumbers()
{

  //
  m_rTagRunNumbers.clear();

  //
  TString path = gSystem->ExpandPathName("$ROOTCOREBIN/../SusySkimMaker/data/PRW/rTagRunNumbers.txt");

  // Try PathResolver instead
  if( !std::ifstream (gSystem->ExpandPathName(path.Data())).good() ) {
    path = TString( PathResolverFindCalibFile( "SusySkimMaker/rTagRunNumbers.txt" ) );
  }    

  //
  MsgLog::INFO("CheckGridSubmission::readRtagRunNumbers","Loading r-tag RunNumber lookup table from %s",path.Data() );

  std::ifstream file;
  file.open ( path.Data() );

  if( !file.is_open() ){
    MsgLog::ERROR("CheckGridSubmission::readRtagRunNumbers","Cannot read in rtag-runNumber mapping text-file!");
    return StatusCode::FAILURE;
  }

  TString line;
  while( file ){
    line.ReadLine(file);
    if( line.Contains("#") || line.IsWhitespace() ) continue;

    // Ensure proper format
    TObjArray* token_cuts = line.Tokenize(" ");
    if( token_cuts->GetSize() < 1 ){
      MsgLog::ERROR("CheckGridSubmission::readRtagRunNumbers","Invalid format for rtag-runNumber. Expect <rTag runNumber> format");
      return StatusCode::FAILURE;
    } 

    TString rTag = ((TObjString*)line.Tokenize(" ")->At(0))->String();
    TString runNumber = ((TObjString*)line.Tokenize(" ")->At(1))->String();

    // Check if this rTag has been read in
    auto it = m_rTagRunNumbers.find( rTag );

    if( it != m_rTagRunNumbers.end() ){
      MsgLog::WARNING("CheckGridSubmission::readRtagRunNumbers","Detected duplicate r-tags, ignoring this contribution");
      continue;
    } 

    // And we are done!
    m_rTagRunNumbers.insert( std::pair<TString,int>( rTag, runNumber.Atoi() ) );

    //
    MsgLog::INFO("CheckGridSubmission::readRtagRunNumbers","Loaded RunNumber %i with r-tag %s",runNumber.Atoi(),rTag.Data() );


  } 

  //
  MsgLog::INFO("CheckGridSubmission::readRtagRunNumbers","Successfully loaded all r-tags");

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------ //
bool CheckGridSubmission::checkSamples(std::vector<TString> samples, bool isData, bool isAf2)
{

  const char* APP_NAME = "CheckGridSubmission";

  EventObject* evt = new EventObject();
  CHECK( evt->init_tools(isData,isAf2) );

  // 
  unsigned int cached_finalState = -1;

  // Loop over samples 
  for( auto& line : samples ){

    // Checks
    if( !line.Contains(".")   ) continue;
    if(  line.IsWhitespace()  ) continue;

    // 
    unsigned int sampleID = getSampleID(line); 

    // 
    float crossSection = 1.0; 
    if( !isData ) {
      crossSection = getXSec(evt,sampleID,cached_finalState);
    }

    //
    AMISampleStatus amiResult;
    if (sampleID!=0) {
      amiResult = getAMIInfo(line,isData);
    }

    //
    RucioSampleStatus rucioResult;
    if( !amiResult.sampleReady ){
      rucioResult = getRucioInfo(line);
    }

    // M.G. Disabled for now, while
    // EventObject changes are made to EventObject structure
    bool validPRWProfile = true; //checkPRW(evt,sampleID,line,isData);

    bool badSample = false;
    if( crossSection<=0            ) badSample = true;
    if( !amiResult.sampleReady     ) badSample = true;
    if( !validPRWProfile           ) badSample = true;

    // 
    if(badSample) m_badJobs->addStatus(line,crossSection,amiResult,rucioResult,isData,validPRWProfile);
    else          m_goodJobs->addStatus(line,crossSection,amiResult,rucioResult,isData,validPRWProfile);
      
  }

  delete evt;

  for(int g=0; g<m_goodJobs->size(); g++){
    std::cout << " >>>> Sample PASSING checks: " << m_goodJobs->samples[g] << std::endl;
    m_goodJobs->printStatus(g);
  }

  for( int b=0; b<m_badJobs->size(); b++ ){
    std::cout << " >>>> Sample FAILING checks: " << m_badJobs->samples[b] << std::endl;
    m_badJobs->printStatus(b);
  }

  MsgLog::INFO("CheckGridSubmission::checkSamples","Detected %i properly prepared samples",m_goodJobs->size() );
  MsgLog::INFO("CheckGridSubmission::checkSamples","Detected %i improperly prepared samples",m_badJobs->size() );

  // All checks succeeded!!
  if( m_badJobs->size()==0 ){
    MsgLog::INFO("CheckGridSubmission::checkSamples","SAMPLES ARE READY FOR GRID SUBMISSION!!!");
    m_checkStatus=true;
  }
  else{
    MsgLog::ERROR("CheckGridSubmission::checkSamples","SAMPLES ARE NOT READY FOR GRID SUBMISSION!!!");
    m_checkStatus=false;
  }

  return m_checkStatus;

}
// -------------------------------------------------------------------------------------- //
float CheckGridSubmission::getXSec(EventObject* evt,unsigned int sampleID,unsigned int& cached_finalState)
{
  
  int upperFS_limit = 220;

  // Try default background cross section (finalState==0)
  float crossSection = -1.0;
  crossSection = evt->getXsec(sampleID);
  
  // This is a signal grid, requires a final state.
  // Loop over all possible final states, since there isn't
  // actually that many. e.g. see the bottom of:
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYSignalUncertainties
  if(crossSection<=0){
    
    // First try cached final state, since usually signal samples are grouped together 
    crossSection = cached_finalState>0 ? evt->getXsec(sampleID,cached_finalState,EventObject::XSEC_INFO::ALL) : -1; 
    
    // Nothing else to do
    if( crossSection>0.0 ) return crossSection;
    
    // OK, now loop over all possible final state
    for( int fs=0; fs<=upperFS_limit; fs++){ 
      crossSection = evt->getXsec(sampleID,fs,EventObject::XSEC_INFO::ALL);
      if( crossSection>0.0 ){
        cached_finalState=fs;
        return crossSection;
      }
    }
  }

  return crossSection;
  
}
// -------------------------------------------------------------------------------------- //
CheckGridSubmission::AMISampleStatus CheckGridSubmission::getAMIInfo(TString sample, bool isData)
{

  AMISampleStatus final_result;

  // Store dsid 
  final_result.dsid = getSampleID(sample);

  // Assign passing values when checks shut off
  if( !m_checkGridInfo ){
    final_result.nEvents       = -1;
    final_result.prodsysStatus = "NOT CHECKED";
    final_result.sampleReady   = true;

    //
    return final_result;
  }

  // Remove scope (the one followed by ":") and trailing slash to have correct format for AMI
  if(sample.Contains(":")) {
    sample = ((TObjString*)sample.Tokenize(":")->At(1))->String();
  }
  sample.ReplaceAll("/","");

  MsgLog::INFO("CheckGridSubmission::getAMIInfo","Checking AMI info for sample %s",sample.Data() );

  //
  TString cmd = "ami show dataset info "+sample+" | grep -E \"(totalEvents|prodsysStatus)\" | cut -d \":\" -f 2- | sed 's/^\\s//;s/\\s$//'";
  TString result = gSystem->GetFromPipe( cmd.Data() );

  TObjArray* tokens = result.Tokenize("\n");
  if (tokens->GetLast() < 1) {
    MsgLog::ERROR("CheckGridSubmission::getAMIInfo","Could not retrieve AMI sample info for %s",sample.Data() );
    final_result.nEvents       = -1;
    final_result.prodsysStatus = "NONE";
  }
  else{
    // assume the command above gives first nEvents and in the next line prodsysStatus
    final_result.nEvents = ((TString)((TObjString*)tokens->At(0))->String()).Atof();
    final_result.prodsysStatus = (TString)((TObjString*)tokens->At(1))->String();
  }

  // Allow partially avaiable statistics for MC
  if (final_result.prodsysStatus != "ALL EVENTS AVAILABLE" && final_result.prodsysStatus != "EVENTS PARTIALLY AVAILABLE") {
    MsgLog::WARNING("CheckGridSubmission::getAMIInfo","Sample %s has %s prodsysStatus",sample.Data(),final_result.prodsysStatus.Data() );
    final_result.sampleReady = false;
  }

  // Data is required to be strickly finished!
  if (isData && final_result.prodsysStatus != "ALL EVENTS AVAILABLE") {
    MsgLog::WARNING("CheckGridSubmission::getAMIInfo","Data sample %s has %s prodsysStatus",sample.Data(),final_result.prodsysStatus.Data() );
    final_result.sampleReady = false;
  }

  return final_result;

}
// -------------------------------------------------------------------------------------- //
CheckGridSubmission::RucioSampleStatus CheckGridSubmission::getRucioInfo(TString sample)
{

  RucioSampleStatus final_result;

  // Assign passing values when shut off
  if( !m_checkGridInfo ){
    final_result.nEvents = -1;

    //
    return final_result;
  }

  MsgLog::INFO("CheckGridSubmission::checkRucioInfo","Checking rucio information for sample %s",sample.Data() );

  TString cmd = "rucio list-files "+sample+" | grep \"Total events\" | cut -d \":\" -f 2 | tr -d \" \"";
  TString result = gSystem->GetFromPipe( cmd.Data() );

  //
  if (!result.IsFloat()) {
    MsgLog::ERROR("CheckGridSubmission::checkRucioInfo","Unexpected rucio output %s",result.Data() );
    final_result.nEvents = -1;
  }
  final_result.nEvents = atof(result.Data());

  return final_result;

}
// -------------------------------------------------------------------------------------- //
bool CheckGridSubmission::checkPRW(EventObject* evt, unsigned int sampleID, TString sample, bool isData)
{

  // Don't check
  if( m_disablePRW ) return true;

  // No PRW checks for data/truth samples
  if( isData ) return true;
  if( sample.Contains("TRUTH") ) return true;

  // Associate sample to r-tag and lookup PRW profiles
  for( auto& r : m_rTagRunNumbers ){

    if( sample.Contains( r.first ) ){
      
      // Configure for this run number
      //evt->setPileUpReweightToolRun( r.second, true);

      if( !evt->checkPRWInputHistogram(sampleID,r.second) ){
        MsgLog::ERROR("CheckGridSubmission::checkPRW","%s does not have a PRW profile!",sample.Data() );
        return false;
      }
      else{
        return true;
      }
    }
  }
  
  //
  MsgLog::ERROR("CheckGridSubmission::checkPRW","Missing r-tag for sample %s, please add one to data/PRW/rTagRunNumbers.txt",sample.Data() );
  return false;

}
// -------------------------------------------------------------------------------------- //
unsigned int CheckGridSubmission::getSampleID(TString sample)
{

  int token = 1;
  if( sample.Contains("user") ) token = 3;

  TString s_sampleID = ((TObjString*)sample.Tokenize(".")->At(token))->String();

  unsigned int sampleID = 0;

  // Set period to 0 for containers
  // Otherwise assume its the DSID
  if( s_sampleID.Contains("period") ) sampleID = 0;
  else{
    sampleID = s_sampleID.Atoi();
  }

  return sampleID;

}
