#include "SusySkimMaker/ElectronVariable.h"


ElectronVariable::ElectronVariable()
{

  setDefault(medium,false);
  setDefault(tight,false);
  setDefault(tightllh,false);
  setDefault(mediumllh,false);
  setDefault(looseBLllh,false);
  setDefault(loosellh,false);
  setDefault(veryloosellh,false);
  setDefault(LHScore,-1.0);
  setDefault(tightdnn,false);
  setDefault(mediumdnn,false);
  setDefault(loosednn,false);
  setDefault(passBL,false);
  setDefault(nPixHitsPlusDeadSensors,0);
  setDefault(egMotherType,-1);
  setDefault(egMotherOrigin,-1);
  setDefault(egMotherPdgId,-1);
  setDefault(ECIDS,false);
  setDefault(ECIDScore,-1);
  setDefault(ambiguityType,-1);
  setDefault(addAmbiguity,-99);
}
// -------------------------------------------------------------------------------- //
ElectronVariable::ElectronVariable(const ElectronVariable &rhs):
  LeptonVariable(rhs),
  medium(rhs.medium),
  tight(rhs.tight),
  tightllh(rhs.tightllh),
  mediumllh(rhs.mediumllh),
  looseBLllh(rhs.looseBLllh),
  loosellh(rhs.loosellh),
  veryloosellh(rhs.veryloosellh),
  LHScore(rhs.LHScore),
  tightdnn(rhs.tightdnn),
  mediumdnn(rhs.mediumdnn),
  loosednn(rhs.loosednn),
  passBL(rhs.passBL),
  nPixHitsPlusDeadSensors(rhs.nPixHitsPlusDeadSensors),
  egMotherType(rhs.egMotherType),
  egMotherOrigin(rhs.egMotherOrigin),
  egMotherPdgId(rhs.egMotherPdgId),
  ECIDS(rhs.ECIDS),
  ECIDScore(rhs.ECIDScore),
  ambiguityType(rhs.ambiguityType),
  addAmbiguity(rhs.addAmbiguity)
{
}
// -------------------------------------------------------------------------------- //
ElectronVariable& ElectronVariable::operator=(const ElectronVariable &rhs)
{

  if(this != &rhs){
    LeptonVariable::operator=(rhs);
    medium                  = rhs.medium;
    tight                   = rhs.tight;
    tightllh                = rhs.tightllh;
    mediumllh               = rhs.mediumllh;
    looseBLllh              = rhs.looseBLllh;
    loosellh                = rhs.loosellh;
    veryloosellh            = rhs.veryloosellh;
    LHScore                 = rhs.LHScore;
    tightdnn                = rhs.tightdnn;
    mediumdnn               = rhs.mediumdnn;
    loosednn                = rhs.loosednn;
    passBL                  = rhs.passBL;
    nPixHitsPlusDeadSensors = rhs.nPixHitsPlusDeadSensors;
    egMotherType            = rhs.egMotherType;
    egMotherOrigin          = rhs.egMotherOrigin;
    egMotherPdgId           = rhs.egMotherPdgId;
    ECIDS                   = rhs.ECIDS;
    ECIDScore               = rhs.ECIDScore;
    ambiguityType           = rhs.ambiguityType;
    addAmbiguity            = rhs.addAmbiguity;
  }

  return *this;

}
