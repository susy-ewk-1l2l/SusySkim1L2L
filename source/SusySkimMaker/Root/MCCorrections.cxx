#include "SusySkimMaker/MCCorrections.h"
#include "SusySkimMaker/MsgLog.h"


MCCorrElement::MCCorrElement()
{
}
// --------------------------------------------------------------------------- //
MCCorrElement::MCCorrElement(const MCCorrElement &rhs) :
  TObject(rhs),
  m_eff( rhs.m_eff ),
  m_sf( rhs.m_sf )
{
}
// --------------------------------------------------------------------------- //
MCCorrElement& MCCorrElement::operator=(const MCCorrElement &rhs)
{

  if (this != &rhs) {
    m_eff = rhs.m_eff;
    m_sf  = rhs.m_sf;
  }

  return *this;

}
// --------------------------------------------------------------------------- //
void MCCorrElement::print() const
{

  // Print efficiencies
  if( m_eff.size()>0 ){
    for( auto& eff : m_eff ){
      MsgLog::INFO("MCCorrElement::print","  -------> Systematic %s with efficiency in MC %f and DATA %f",eff.first.Data(),eff.second.first,eff.second.second);
    }
  }

  // Print scale factors
  if( m_sf.size()>0 ){
    for( auto& sf : m_sf ){
      MsgLog::INFO("MCCorrElement::print","  -------> Systematic %s with scale factor %f",sf.first.Data(),sf.second);
    }
  }


}
// --------------------------------------------------------------------------- //
void MCCorrElement::addEff(TString sys,double mc_eff,double data_eff)
{

  auto it = m_eff.find(sys);

  // Don't save duplicates
  if( it==m_eff.end() ){
    m_eff.insert( std::pair<TString,std::pair<float,float>>(sys,std::make_pair(mc_eff,data_eff)  ));
  }
  //else{
  //  Warning("addEff","Systematic %s already exists!! This request will be ignored!",sys.Data() );
  //}

}
// --------------------------------------------------------------------------- //
void MCCorrElement::addSF(TString sys,double sf)
{

  auto it = m_sf.find(sys);

  if( it==m_sf.end() ){
    m_sf.insert( std::pair<TString,float>(sys,sf) );
  }
//  else{
//    Warning("addSF","Systematic %s already exists!! This request will be ignored!",sys.Data() );
//  }

}
// --------------------------------------------------------------------------- //
std::map<TString,std::pair<float,float>> MCCorrElement::getEff()
{ 
  return m_eff; 
}
// --------------------------------------------------------------------------- //
std::map<TString,float> MCCorrElement::getSF()
{ 
  return m_sf; 
}
// --------------------------------------------------------------------------- //
//                               CONTAINER CLASS 
// --------------------------------------------------------------------------- //
MCCorrContainer::MCCorrContainer() : m_element(0)
{
  m_container.clear();
}
// --------------------------------------------------------------------------- //
MCCorrContainer::MCCorrContainer(const MCCorrContainer &rhs) :
  TObject(rhs),
  m_container( rhs.m_container )
{

}
// --------------------------------------------------------------------------- //
void MCCorrContainer::print() const
{

  for( auto& c : m_container ){
    MsgLog::INFO( "MCCorrContainer::print","Printing information for %s", c.first.Data() );
    c.second->print();
  }

}
// --------------------------------------------------------------------------- //
void MCCorrContainer::clear()
{
  for( auto& c : m_container ){
    if(c.second) delete c.second;
  }
  m_container.clear();
  
}

// --------------------------------------------------------------------------- //
MCCorrContainer& MCCorrContainer::operator=(const MCCorrContainer &rhs)
{

  if (this != &rhs) {
    m_container = rhs.m_container;
  }

  return *this;

}
// --------------------------------------------------------------------------- //
void MCCorrContainer::addSF(TString instance,TString systematic, double SF)
{

  auto it = m_container.find(instance);

  //
  if( it==m_container.end() && !m_element ){
    MCCorrElement* element = new MCCorrElement();
    element->addSF(systematic,SF);
    m_container.insert(std::pair<TString,MCCorrElement*>(instance,element) );
  }
  else{
    it->second->addSF(systematic,SF);
  }
  
}
// --------------------------------------------------------------------------- //
void MCCorrContainer::addEff(TString instance,TString systematic,double mc_eff, double data_eff)
{

  auto it = m_container.find(instance);

  //
  if( it==m_container.end() && !m_element ){
    MCCorrElement* element = new MCCorrElement();
    element->addEff(systematic,mc_eff,data_eff);
    m_container.insert(std::pair<TString,MCCorrElement*>(instance,element) );
  }
  else{
    it->second->addEff(systematic,mc_eff,data_eff);
  }

}
// --------------------------------------------------------------------------- //
void MCCorrContainer::getEff(TString instance, TString systematic, double& mc_eff, double& data_eff)
{

  // Search for this instance
  auto ins_it = m_container.find(instance);

  // Nothing saved for this instance 
  if( ins_it==m_container.end() ){
    //Warning("getEff","Could not find instance %s",instance.Data());
    return;
  }

  auto eff_map = ins_it->second->getEff();

  // Search for this systematic
  auto sys_it = eff_map.find(systematic);

  // Systematic not found
  // TODO: return nominal?
  if( sys_it==eff_map.end() ){
    //Warning("getEff","Could not find systematic %s",systematic.Data());
    return;
  }

  mc_eff   = sys_it->second.first;
  data_eff = sys_it->second.second;

}
// --------------------------------------------------------------------------- //
void MCCorrContainer::getSF(TString instance, TString systematic, double& sf)
{

  // Search for this instance
  auto ins_it = m_container.find(instance);

  if( ins_it==m_container.end() ){
    return;
  }

  auto sf_map = ins_it->second->getSF();

  // Seach for this systematic
  auto sys_it = sf_map.find(systematic);

  // SF for this systematic not found
  if( sys_it==sf_map.end() ){
    return;
  }

  //
  sf = sys_it->second;

}
// --------------------------------------------------------------------------- //
void MCCorrContainer::print()
{

  for( auto& ins : m_container ){
    std::cout << "Printing instance : " << ins.first << std::endl;

    for( auto& sys : ins.second->getEff() ){
      printf("   >>>> Systematic %s with mc eff %f and data eff %f \n",sys.first.Data(),sys.second.first,sys.second.second);
    }
  }

}
// --------------------------------------------------------------------------- //
MCCorrElement* MCCorrContainer::getElement(TString instance)
{
  // Search for this instance
  auto ins_it = m_container.find(instance);

  if( ins_it==m_container.end() ){
    return 0;
  }
  return ins_it->second;
}
// --------------------------------------------------------------------------- //
const std::map<TString,MCCorrElement*> MCCorrContainer::getContainer()
{
  return m_container;
}
