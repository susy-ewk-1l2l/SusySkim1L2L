#include "SusySkimMaker/MetObject.h"
#include "SusySkimMaker/TreeMaker.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "AsgMessaging/StatusCode.h"


MetObject::MetObject() : m_convertFromMeV(1.00)
{
}
// ------------------------------------------------------------------------- //
StatusCode MetObject::init(TreeMaker*& treeMaker)
{

  const char* APP_NAME = "MetObject";

  std::vector<TString> sysNames  = treeMaker->getSysVector();

  for(unsigned int s=0; s<sysNames.size(); s++){
    TString sysName = sysNames[s];
    //
    MetVector* met  = new MetVector();
    // Save into tau vector into map
    m_metMap.insert( std::pair<TString,MetVector*>(sysName,met) );
    // Get tree created by createTrees
    TTree* sysTree = treeMaker->getTree("skim",sysName);
    // Don't write it out
    if(sysTree==NULL) continue;
    else{
      MsgLog::INFO("MetObject::init","Adding a branch met to skim tree %s ", sysTree->GetName() );
      std::map<TString,MetVector*>::iterator metItr = m_metMap.find(sysName);
      sysTree->Branch("met",&metItr->second);
    }
  }

  //
  CHECK( init_tools() );

  // Return gracefully
  return StatusCode::SUCCESS; 

}
// ------------------------------------------------------------------------- //
StatusCode MetObject::init_tools()
{

  // Retrieve any flags needed for this class
  CentralDB::retrieve(CentralDBFields::UNITS,m_convertFromMeV);

  //
  MsgLog::INFO("MetObject::init","Successfully initialized METObject!");

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
void MetObject::fillMetContainer(xAOD::MissingETContainer* met, 
                                 const xAOD::MissingETContainer* met_truth,
                                 const std::map<TString,bool> trigMap,
                                 const double &metSignif,
                                 MetVariable::MetFlavor flavor,
                                 std::string sys_name)
{

  std::map<TString,MetVector*>::iterator it = m_metMap.find(sys_name);

  if( it==m_metMap.end() ){
    MsgLog::ERROR("MetObject::fillMetContainer","Request to get met for unknown systematic: %s",sys_name.c_str());
    return;
  }

  //
  MetVariable* myMET = new MetVariable();

  // MET flavor: e.g. how was it built
  myMET->flavor = flavor;

  // Full rebuilt met
  xAOD::MissingETContainer::const_iterator met_it = met->find( getMetContainerName(flavor) );

  // Each term separately
  xAOD::MissingETContainer::const_iterator met_it_ele  = met->find("RefEle");
  xAOD::MissingETContainer::const_iterator met_it_muon = met->find("Muons");
  xAOD::MissingETContainer::const_iterator met_it_jet  = met->find("RefJet");
  xAOD::MissingETContainer::const_iterator met_it_soft = met->find("PVSoftTrk");

  if( met_it == met->end() ){
    MsgLog::ERROR("MetObject::fillMetContainer", "Could not find rebuilt MET container %s. Skims will not be filled!",getMetContainerName(flavor).c_str() );
    return;
  }

  /*
    Information for full MET
  */
  myMET->Et        = TMath::Sqrt( pow((*met_it)->mpx(),2) + pow((*met_it)->mpy(),2) ) * m_convertFromMeV;   
  myMET->phi       = (*met_it)->phi();
  myMET->px        = (*met_it)->mpx() * m_convertFromMeV;
  myMET->py        = (*met_it)->mpy() * m_convertFromMeV;

  /*
    TLV for full MET
  */
  myMET->SetPxPyPzE(myMET->px,myMET->py,0,myMET->Et);

  /*
    Information for electron term
  */
  if( met_it_ele!=met->end() ){ 
    myMET->Et_ele    = TMath::Sqrt( pow((*met_it_ele)->mpx(),2) + pow((*met_it_ele)->mpy(),2) ) * m_convertFromMeV;   
    myMET->phi_ele   = (*met_it_ele)->phi();
    myMET->px_ele    = (*met_it_ele)->mpx() * m_convertFromMeV;
    myMET->py_ele    = (*met_it_ele)->mpy() * m_convertFromMeV;
  }

  /*
    Information for muon term
  */
  if( met_it_muon!=met->end() ){
    myMET->Et_muon   = TMath::Sqrt( pow((*met_it_muon)->mpx(),2) + pow((*met_it_muon)->mpy(),2) ) * m_convertFromMeV;   
    myMET->phi_muon  = (*met_it_muon)->phi();
    myMET->px_muon   = (*met_it_muon)->mpx() * m_convertFromMeV;
    myMET->py_muon   = (*met_it_muon)->mpy() * m_convertFromMeV;
  }

  /*
    Information for jet term
  */
  if( met_it_jet!=met->end() ){
    myMET->Et_jet    = TMath::Sqrt( pow((*met_it_jet)->mpx(),2) + pow((*met_it_jet)->mpy(),2) ) * m_convertFromMeV;   
    myMET->phi_jet   = (*met_it_jet)->phi();
    myMET->px_jet    = (*met_it_jet)->mpx() * m_convertFromMeV;
    myMET->py_jet    = (*met_it_jet)->mpy() * m_convertFromMeV;
  }

  /*
    Information for soft term
  */
  if( met_it_soft!=met->end() ){
    myMET->Et_soft   = TMath::Sqrt( pow((*met_it_soft)->mpx(),2) + pow((*met_it_soft)->mpy(),2) ) * m_convertFromMeV;   
    myMET->phi_soft  = (*met_it_soft)->phi();
    myMET->px_soft   = (*met_it_soft)->mpx() * m_convertFromMeV;
    myMET->py_soft   = (*met_it_soft)->mpy() * m_convertFromMeV;
  }

  // MET significance
  myMET->metSignif = metSignif;

  // Trigger decision
  myMET->triggerMap = trigMap;

  // MET TLV
  if( met_truth ){
    xAOD::MissingETContainer::const_iterator met_truth_it = met_truth->find("NonInt"); 
    if( met_truth_it != met_truth->end() ){
      myMET->Et_truth = TMath::Sqrt( pow((*met_truth_it)->mpx(),2) + pow((*met_truth_it)->mpy(),2) ) * m_convertFromMeV;
      myMET->px_truth = (*met_truth_it)->mpx() * m_convertFromMeV;
      myMET->py_truth = (*met_truth_it)->mpy() * m_convertFromMeV;
    }
  }

  // Save!
  it->second->push_back(myMET);

}
// ------------------------------------------------------------------------- //
void MetObject::fillMetContainerTruth(xAOD::MissingETContainer* met,std::string sys_name /*=""*/)
{

  std::map<TString,MetVector*>::iterator it = m_metMap.find(sys_name);

  if( it==m_metMap.end() ){
    MsgLog::ERROR("MetObject::fillMetContainerTruth","Request to get met for unknown systematic: %s",sys_name.c_str());
    return;
  }

  //
  MetVariable* myMET = new MetVariable();

  //
  // Use NonInt: computed from the non-interating particles in the event
  //
  xAOD::MissingETContainer::const_iterator met_it = met->find("NonInt");

  if( met_it == met->end() ){
    MsgLog::ERROR("MetObject::fillMetContainerTruth", "Could not find rebuilt MET. Containers will not be filled!");
    return;
  }

  myMET->Et        = TMath::Sqrt( pow((*met_it)->mpx(),2) + pow((*met_it)->mpy(),2) ) * m_convertFromMeV;
  myMET->phi       = (*met_it)->phi();
  myMET->px        = (*met_it)->mpx()*m_convertFromMeV;
  myMET->py        = (*met_it)->mpy()*m_convertFromMeV;
  
  // TLV
  myMET->SetPxPyPzE(myMET->px,myMET->py,0,myMET->Et);

  // Save
  it->second->push_back(myMET);

}
// ------------------------------------------------------------------------- //
std::string MetObject::getMetContainerName(MetVariable::MetFlavor flavor)
{

  // Update as needed
  // Point by default to Final
  std::string name = "Final";

  if( flavor==MetVariable::MetFlavor::TRACK || flavor==MetVariable::MetFlavor::TRACKLOOSE || flavor==MetVariable::MetFlavor::TRACKTIGHTER || flavor==MetVariable::MetFlavor::TRACKTENACIOUS ){
    name = "Track";
  }

  return name;

}
// ------------------------------------------------------------------------- //
const MetVector* MetObject::getObj(TString sysName)
{

  std::map<TString,MetVector*>::iterator it = m_metMap.find(sysName);

  if(it==m_metMap.end()){
    MsgLog::WARNING("MetObject::getObj", "Cannot get met vector for systematic: %s",sysName.Data());
    return NULL;
  }

  return it->second;
  
}
// ------------------------------------------------------------------------- //
void MetObject::Reset()
{

  // Free up memory
  std::map<TString,MetVector*>::iterator it;
  for(it = m_metMap.begin(); it != m_metMap.end(); it++){
    for (MetVector::iterator metItr = it->second->begin(); metItr != it->second->end(); metItr++) {
      delete *metItr;
    }
    it->second->clear();
  }

}
// ------------------------------------------------------------------------- //
MetObject::~MetObject(){
  Reset();
  m_metMap.clear();
}
