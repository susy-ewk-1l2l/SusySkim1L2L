#include "SusySkimMaker/Objects.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/CutFlowTool.h"

Objects::Objects() : skimEvt (0),skimMet(0),skimMuons(0),skimElectrons(0),skimTaus(0),skimJets(0),skimFatjets(0),skimTrackjets(0),skimPhotons(0),skimTracks(0),
                     // ST signal object def
                     s_ST_def(true),
                     // Overlap removal flag
                     m_disableOR(false),
                     // Overlap removal for baseline electrons
                     m_disableORBaselineElectrons(false),
                     // Overlap removal flag for baseline muons (this OR is removed in EWK 3L fake estimate)
                     m_disableORBaselineMuons(false),
                     // Baseline electrons
                     b_electron_et(10.0),b_electron_eta(2.47),
                     // Signal electrons (in GeV)
                     s_electron_et(25.0),s_electron_eta(2.47),s_electron_relIso(0.10),
                     s_electron_tight(true),s_electron_d0(1.0),s_electron_z0(2.0),
                     // Baseline muons (in GeV)
                     b_muon_pt(10.0),b_muon_eta(2.40),
                     // Signal muons (in GeV)
                     s_muon_pt(25.0),s_muon_eta(2.40),s_muon_absIso(1.80),s_muon_relIso(-1.0),
                     s_muon_d0(0.2),s_muon_z0(1.0),
                     // Baseline photons
                     b_photon_et(25.0),b_photon_eta(2.47),
                     // Signal photons (in GeV)
                     s_photon_et(25.0),s_photon_eta(2.47),s_photon_relIso(0.10),
                     s_photon_tight(true),
                     // Signal jets( in GeV)
                     s_cJet_pt(25.0),s_cJet_eta(2.80),
                     s_fJet_pt(30.0),s_fJet_eta_min(2.80),s_fJet_eta_max(4.50),
                     // Signal fatjets( in GeV)
                     s_cFatjet_pt(200.0),s_cFatjet_eta(2.0),s_cFatjet_m(-1),
                     // Signal taus
                     s_tau_pt(20.0),s_tau_eta(2.50),
                     // Signal tracks
                     s_trk_pt(-1.0),s_trk_minEta(-1.0),s_trk_maxEta(-1.0),
                     s_trk_d0(-1.0),s_trk_d0Sig(-1.0),s_trk_z0SinTheta(-1.0),s_trk_relTrkIso(-1.0),s_trk_caloIso20(-1.0),s_trk_fitQuality(-1.0),
                     s_trk_minPixHits(-1),s_trk_maxSCTHits(-1),s_trk_maxPixSpoiltHits(-1),s_trk_maxGangedFakes(-1),
                     s_trk_nSiHoles(-1),s_trk_pixOutliers(-1),s_trk_nPixLayers(-1),s_trk_expBLayerHits(-1),
                     // MET
                     s_METFlavor(MetVariable::MetFlavor::DEFAULT),
                     // Overlap removal
                     s_user_or(0), s_user_or_baware(0),
                     m_minDr_m_j(0.4),m_minDr_j_e(0.2),m_minDr_j_g(0.2),m_minDr_e_j(0.4),m_minDr_g_j(0.4),
                     m_minDr_t_m(-1.0),m_minDr_t_e(-1.0),m_minDr_t_j(-1.0),
                     // Truth overlap removal information
                     m_truthEleJetDR(-1.0),m_truthMuJetDR(-1.0),
                     // Event cleaning
                     m_applyBadJetVeto(true),
                     m_applyEvtLooseBadJetVeto(false),
                     m_applyEvtTightBadJetVeto(false),
                     m_applyTSTCleaning(false),
                     m_applyBadMuonVeto(false),
                     m_applyCosmicMuonVeto(false),
                     m_applyBadTileJetVeto(false)

{

  // Clear all vectors
  clear();

  // Clean log file
  m_log.clear();

  // Load list of bad tile modules
  loadListOfBadTileModules();

}
// ------------------------------------------------------------- //
void Objects::clear()
{

  /**
   * Clear all objects in this class
   */

  // MET
  met.Reset();
  trackMet.Reset();

  // Leptons (only electrons+muons)
  baseLeptons.clear();
  signalLeptons.clear();

  // Muons
  muons.clear();
  signalMuons.clear();
  baseMuons.clear();

  // Electrons
  electrons.clear();
  signalElectrons.clear();
  baseElectrons.clear();

  // Photons
  photons.clear();
  signalPhotons.clear();
  basePhotons.clear();
  
  // Tracks
  tracks.clear(); 
  baseTracks.clear();
  signalTracks.clear();
  pixelThreeLayer.clear();
  pixelFourLayer.clear();
  msTracks.clear();
  stdTracks.clear();

  // Taus
  taus.clear();
  baseTaus.clear();
  signalTaus.clear();

  // Jets
  baselineJets.clear();
  jets.clear();
  cJets.clear();
  bJets.clear();
  fJets.clear();
  aJets.clear();

  // Fatjets
  fatjets.clear();
  cFatjets.clear();

  // Trackjets
  trackjets.clear();

  // Truth
  truth.clear();
  truthParticles.clear();
  truthLeptons.clear();
  truthStatus3Leptons.clear();
  truthAllJets.clear();
  truthJets.clear();
  charginos.clear();
  truthTaus.clear();

}


// ------------------------------------------------------------- //
void Objects::loadListOfBadTileModules(){

  //
  // Reference:
  //  - Data quality INT note: https://cds.cern.ch/record/2663169/files/ATL-COM-DAPR-2019-001.pdf?
  //    where you can know the name of bad tile modules and affected period
  //  - eta/phi module/channel converter: http://atlas.web.cern.ch/Atlas/SUB_DETECTORS/TILE/EtaPhiConverter/converter.html
  //  - Implementatoin in the Strong 0L framework: https://gitlab.cern.ch/atlas-susy-0l-inclusive/ZeroLeptonRun2/blob/master/Root/PhysObjProxyUtils.cxx#L1098-1155
  //

  // Load bad tile info
  m_badTileDict.clear();  

  // 2015
  m_badTileDict.push_back( fillBadTileInfo(266904, 284484,   0.,  0.9, 0.8, 1.0) ); // LBA10
  m_badTileDict.push_back( fillBadTileInfo(266904, 284484, -1.6, -0.9, 1.9, 2.1) ); // EBC21
  // 2016
  m_badTileDict.push_back( fillBadTileInfo(306988, 311481, -0.9 , 0. ,  0.34,  0.54) ); // LBC5  
  m_badTileDict.push_back( fillBadTileInfo(302053, 311481, 0.  , 0.9, -1.33, -1.13) ); // LBA52
  // 2017  
  m_badTileDict.push_back( fillBadTileInfo(325713, 340453, -0.9,  0., -0.25, -0.05) ); // LBC63
  m_badTileDict.push_back( fillBadTileInfo(325713, 340453,  0.8, 1.7,  0.14,  0.34) ); // EBA3   
  // 2018
  m_badTileDict.push_back( fillBadTileInfo(350310, 352514, 0., 0.9,  2.7,  3.0) ); // LBA29,30
  m_badTileDict.push_back( fillBadTileInfo(355261, 364292, 0., 0.9,  3.0,  999) ); // LBA32 (phi>0)
  m_badTileDict.push_back( fillBadTileInfo(355261, 364292, 0., 0.9, -999, -3.0) ); // LBA32 (phi<0)

}
// ------------------------------------------------------------- //
void Objects::readObjects(const EventVariable* skimEvt,
			  const MetVector* skimMet,
			  const MuonVector* skimMuons, 
			  const ElectronVector* skimElectrons,
			  const JetVector* skimJets,
			  const JetVector* skimFatjets,
			  const JetVector* skimTrackjets,
			  const TauVector* skimTaus,
                          const PhotonVector* skimPhotons,
                          const TrackVector* skimTracks,
                          const ObjectVector* skimCalo,
			  const TruthEvent* skimTruthEvent,
			  const TruthEvent_Vjets* skimTruthEvent_Vjets,
			  const TruthEvent_VV* skimTruthEvent_VV,
			  const TruthEvent_VVV* skimTruthEvent_VVV,
			  const TruthEvent_TT* skimTruthEvent_TT,
			  const TruthEvent_XX* skimTruthEvent_XX,
			  const TruthEvent_GG* skimTruthEvent_GG,
			  const TruthVector* skimTruth)
{


  // Copy pointers into internal vectors of this class
  if(skimEvt)               this->evt                =* skimEvt;
  if(skimMet)               this->metVector          =* skimMet;
  if(skimMuons)             this->muons              =* skimMuons;
  if(skimElectrons)         this->electrons          =* skimElectrons;
  if(skimJets)              this->jets               =* skimJets;
  if(skimFatjets)           this->fatjets            =* skimFatjets;
  if(skimTrackjets)         this->trackjets          =* skimTrackjets;
  if(skimTaus)              this->taus               =* skimTaus;
  if(skimPhotons)           this->photons            =* skimPhotons;
  if(skimTracks)            this->tracks             =* skimTracks;
  if(skimCalo)              this->caloClus           =* skimCalo;
  if(skimTruthEvent)        this->truthEvent         =* skimTruthEvent;
  if(skimTruthEvent_Vjets)  this->truthEvent_Vjets   =* skimTruthEvent_Vjets;
  if(skimTruthEvent_VV)     this->truthEvent_VV      =* skimTruthEvent_VV;
  if(skimTruthEvent_VVV)    this->truthEvent_VVV      =* skimTruthEvent_VVV;
  if(skimTruthEvent_TT)     this->truthEvent_TT      =* skimTruthEvent_TT;
  if(skimTruthEvent_XX)     this->truthEvent_XX      =* skimTruthEvent_XX;
  if(skimTruthEvent_GG)     this->truthEvent_GG      =* skimTruthEvent_GG;
  if(skimTruth)             this->truth              =* skimTruth;

  // Sort objects by pT
  std::sort( this->muons.begin(),this->muons.end(),compare);
  std::sort( this->electrons.begin(),this->electrons.end(),compare);
  std::sort( this->photons.begin(),this->photons.end(),compare);
  std::sort( this->tracks.begin(),this->tracks.end(),compare);
  std::sort( this->taus.begin(),this->taus.end(),compare);
  std::sort( this->jets.begin(),this->jets.end(),compare);
  std::sort( this->fatjets.begin(),this->fatjets.end(),compare);
  std::sort( this->trackjets.begin(),this->trackjets.end(),compare);
  std::sort( this->truth.begin(),this->truth.end(),compare);

}
// ------------------------------------------------------------- //
void Objects::classifyObjects()
{

  // Do custom overlap removal? (used for truth analysis)
  // For now: do overlap removal with baseline objects
  if (s_user_or) {
    this->doOverlap(this->muons,
		    this->electrons,
		    this->photons,
		    this->jets);
  }

  // Remove fatjets overlapped with electrons (that pass the standard OR)
  this->doOverlap_FatJet(this->electrons, this->photons, this->fatjets);

  // Important to do this before leptons/jets 
  // since it's faster to look up the leptons/jets
  // in the sorted containers created in getTruth(), rather than 
  // sorting them three times for each call jet/ele/mu calls below
  this->getTruth();

  // Point default configuration to standard "met" container
  this->getMET();

  // Classify leptons and jets according to signal requirements
  this->getMuons();
  this->getElectrons();
  this->getTaus();  
  this->getJets();
  this->getFatjets();
  this->getPhotons();

  // Fill track vector
  // Very important that this is called after filling 
  // the electrons/muons and jets, since an overlap 
  // removal is performed! Don't move!
  this->getTracks();

  // Group electrons and muons
  this->fillLeptonVectors();

}
// ------------------------------------------------------------- //
void Objects::fillLeptonVectors()
{

  for(unsigned int i=0; i<this->baseMuons.size(); i++      ) this->baseLeptons.push_back(this->baseMuons[i]);
  for(unsigned int i=0; i<this->signalMuons.size(); i++    ) this->signalLeptons.push_back(this->signalMuons[i]);
  for(unsigned int i=0; i<this->baseElectrons.size(); i++  ) this->baseLeptons.push_back(this->baseElectrons[i]);
  for(unsigned int i=0; i<this->signalElectrons.size(); i++) this->signalLeptons.push_back(this->signalElectrons[i]);

  // Sort these vectors
  std::sort( this->baseLeptons.begin(),this->baseLeptons.end(),compare);
  std::sort( this->signalLeptons.begin(),this->signalLeptons.end(),compare);

}
// ------------------------------------------------------------- //
void Objects::getMET()
{

  // Select the flavor of MET that the user wants
  for( auto& etmiss : this->metVector){
    if( etmiss->flavor==s_METFlavor ){
      met = *etmiss;
    }
  }

  // Load track MET in the cache
  const MetVariable* trackMet_tmp =
    s_METFlavor==MetVariable::MetFlavor::LOOSE ?     getMETFlavor(MetVariable::MetFlavor::TRACKLOOSE, false) :
    s_METFlavor==MetVariable::MetFlavor::TIGHTER ?   getMETFlavor(MetVariable::MetFlavor::TRACKTIGHTER, false) :
    s_METFlavor==MetVariable::MetFlavor::TENACIOUS ? getMETFlavor(MetVariable::MetFlavor::TRACKTENACIOUS, false) :
    getMETFlavor(MetVariable::MetFlavor::TRACK, false);
    
  if(trackMet_tmp) trackMet = *trackMet_tmp;
}
// ------------------------------------------------------------- //
const MetVariable* Objects::getMETFlavor(MetVariable::MetFlavor flavor, bool verbose) const
{

  for( auto& etmiss : this->metVector){
    if( etmiss->flavor==flavor ) return etmiss;
  }

  if(verbose) MsgLog::WARNING("Objects::getMETFlavor","Cannot find MET flavor %i!! Returning NULL!",flavor); 
  return 0;


}
// ------------------------------------------------------------- //
void Objects::getMuons()
{

  for(unsigned int i=0; i<this->muons.size(); i++){

    //
    MuonVariable* mu = this->muons[i];
    
    // OR for baseline muons
    if( !m_disableORBaselineMuons && !m_disableOR && !mu->passOR ) continue; 

    // Associate a truth particle
    // using the barcode!
    for( auto& truthLepton : this->truthLeptons ){
      if( abs(truthLepton->pdgId)==13 && mu->barcode==truthLepton->barcode ){
        mu->truthLink = truthLepton;
        break;
      }
    }

    // Baseline muons
    if( IsBaselineMuon(mu) ) this->baseMuons.push_back(mu);
    else continue;
     
    // Signal Muons
    if( !m_disableOR && !mu->passOR ) continue; 
    if( IsSignalMuon(mu) ) this->signalMuons.push_back(mu);
    
  }

  // MS tracks
  for( auto& t : this->tracks ){
    if( t->trackType==TrackVariable::TrackType::MSONLY ){
      msTracks.push_back(t);
    }
  }

}
// ------------------------------------------------------------- //
bool Objects::IsBaselineMuon(ObjectVariable* mu)
{

  // M.G. Lets try to keep all baseline cuts
  // within the 'ObjectVariable' scope;
  // i.e. doesn't depend on MuonVariable/ElectronVariable
  // quantities, so we can pass TruthVaraibles to this method too

  // pT and eta cuts
  if(b_muon_pt>=0 && mu->Pt() < b_muon_pt                  ) return false;
  if(b_muon_eta>=0 && TMath::Abs(mu->Eta()) >= b_muon_eta  ) return false;

  // Baseline 
  return true;
  
}
// ------------------------------------------------------------- //
bool Objects::IsSignalMuon(MuonVariable* mu)
{

  // Use SUSYTools signal muon def.
  if( s_ST_def){
    if( !mu->signal ) return false;
    else              return true;
  }

  // Pt and Eta cut
  if(s_muon_pt>=0 && mu->Pt() < s_muon_pt                 ) return false;
  if(s_muon_eta>=0 && TMath::Abs(mu->Eta()) > s_muon_eta  ) return false;

  // Isolation cuts
  if(s_muon_absIso>=0 && mu->ptcone20 > s_muon_absIso ) return false;
  if(s_muon_relIso>=0 && (mu->ptcone30 / mu->Pt()) > s_muon_relIso ) return false;
  
  // Impact parameter cuts
  if(s_muon_d0>=0 && TMath::Abs(mu->d0) > s_muon_d0 ) return false;
  if(s_muon_z0>=0 && TMath::Abs(mu->z0) > s_muon_z0 ) return false;

  return true;

}
// ------------------------------------------------------------- //
void Objects::getElectrons()			  
{

  for(unsigned int i=0; i<this->electrons.size(); i++){

    //
    ElectronVariable* ele = this->electrons[i];

    // OR for baseline electrns
    if( !m_disableORBaselineElectrons && !m_disableOR && !ele->passOR ) continue; 

    // Associate to a truth particle
    for( auto& truthLepton : this->truthLeptons ){
      if( abs(truthLepton->pdgId)==11 && ele->barcode==truthLepton->barcode ){
        ele->truthLink = truthLepton;
        break;
      }
    }

    // Baseline Electrons 
    if( IsBaselineElectron(ele) ) this->baseElectrons.push_back(ele);
    else continue;
   
    // Signal Electrons
    if( !m_disableOR && !ele->passOR ) continue;
    if( IsSignalElectron(ele) ) this->signalElectrons.push_back(ele);

  }  

}
// ------------------------------------------------------------- //
bool Objects::IsBaselineElectron(ObjectVariable* ele)
{

  // M.G. Lets try to keep all baseline cuts 
  // within the 'ObjectVariable' scope;
  // i.e. doesn't depend on MuonVariable/ElectronVariable 
  // quantities, so we can pass TruthVaraibles to this method too

  if(b_electron_et>=0 && ele->Et() < b_electron_et           ) return false;
  if(b_electron_eta>=0 && abs(ele->Eta()) >= b_electron_eta  ) return false;

  return true;

}
// ------------------------------------------------------------- //
bool Objects::IsSignalElectron(ElectronVariable* ele)
{
  
  // Use SUSYTools signal electron def.
  if( s_ST_def){
    if( !ele->signal )   return false;
    else                 return true;
  }

  // Quality of electron
  if( s_electron_tight && !isTightPP(ele) ) return false;

  // Pt and Eta cut
  if(s_electron_et>=0 && ele->Et() < s_electron_et          ) return false;
  if(s_electron_eta>=0 && abs(ele->Eta()) > s_electron_eta  ) return false;

  // Isolation cuts
  if(s_electron_relIso>=0 && ( ele->ptcone20 / ele->Pt() ) > s_electron_relIso ) return false;;

  // Impact parameter cuts
  if(s_electron_d0>=0 && TMath::Abs(ele->d0) > s_electron_d0 ) return false;
  if(s_electron_z0>=0 && TMath::Abs(ele->z0) > s_electron_z0 ) return false;

  return true;

}
// ------------------------------------------------------------- //
bool Objects::isTightPP(const ElectronVariable* ele)
{

  return ele->tight;

}
// ------------------------------------------------------------- //
bool Objects::isMediumPP(const ElectronVariable* ele)
{

  return ele->medium;

}
// ------------------------------------------------------------- //
void Objects::getPhotons()			  
{

  for(unsigned int i=0; i<this->photons.size(); i++){

    //
    PhotonVariable* pho = this->photons[i];

    // OR for all photons
    if( !m_disableOR && !pho->passOR ) continue;

    // Associate to a truth particle
    //for( auto& truthLepton : this->truthLeptons ){
    //  if( abs(truthLepton->pdgId)==11 && ele->barcode==truthLepton->barcode ){
    //    ele->truthLink = truthLepton;
    //    break;
    //  }
    //}

    // Baseline Photons 
    if( IsBaselinePhoton(pho) ) this->basePhotons.push_back(pho);
    else continue;
   
    // Signal Photons
    if( IsSignalPhoton(pho) ) this->signalPhotons.push_back(pho);

  }  

}
// ------------------------------------------------------------- //
bool Objects::IsBaselinePhoton(ObjectVariable* pho)
{

  // M.G. Lets try to keep all baseline cuts 
  // within the 'ObjectVariable' scope;
  // i.e. doesn't depend on MuonVariable/ElectronVariable 
  // quantities, so we can pass TruthVaraibles to this method too

  if(b_photon_et>=0 && pho->Et() < b_photon_et           ) return false;
  if(b_photon_eta>=0 && abs(pho->Eta()) >= b_photon_eta  ) return false;

  return true;

}
// ------------------------------------------------------------- //
bool Objects::IsSignalPhoton(PhotonVariable* pho)
{
  
  // Use SUSYTools signal electron def.
  if( s_ST_def){
    if( !pho->signal )   return false;
    else                 return true;
  }

  // Quality of photon
  if( s_photon_tight && !pho->tight ) return false;

  // Pt and Eta cut
  if(s_photon_et>=0 && pho->Et() < s_photon_et          ) return false;
  if(s_photon_eta>=0 && abs(pho->Eta()) > s_photon_eta  ) return false;

  // Isolation cuts
  if(s_photon_relIso>=0 && ( pho->topoetcone20 / pho->Pt() ) > s_photon_relIso ) return false;;

  // Impact parameter cuts
  //if(s_electron_d0>=0 && TMath::Abs(ele->d0) > s_electron_d0 ) return false;
  //if(s_electron_z0>=0 && TMath::Abs(ele->z0) > s_electron_z0 ) return false;

  return true;

}
// ------------------------------------------------------------- //
void Objects::getTaus()
{

  for(unsigned int i=0; i<this->taus.size(); i++){
    TauVariable* tau = this->taus[i];

    // Baseline taus
    this->baseTaus.push_back(tau);

    // Signal taus
    if( IsSignalTau(tau) ) this->signalTaus.push_back(tau);
    
  }  

}
// ------------------------------------------------------------- //
bool Objects::IsSignalTau(TauVariable* tau)
{

  // pT and eta cuts
  if(s_tau_pt>=0 && tau->Pt() < s_tau_pt ) return false;
  if(s_tau_eta>=0 && TMath::Abs(tau->Eta()) > s_tau_eta ) return false;

  return true;

}
// ------------------------------------------------------------- //
void Objects::getJets()
{

  for(uint i=0; i<this->jets.size(); i++){

    //
    JetVariable* jet = this->jets[i];

    // Decorate if this jet passes to any bad tile modules.
    jet->badTile = isBadTileJet(jet);

    // baselineJets: stored before overlap removal!
    if( isCentralJet(jet) ) this->baselineJets.push_back(jet);

    // OR for all jet containers
    if( !m_disableOR && !jet->passOR ) continue;

    // Associate a truth link
    // TODO: Is there a better way to do this matching?
    // i.e. leptons use their barcode...
    for( auto& truthJet : this->truthJets ){
      if( jet->DeltaR( *truthJet) < 0.4 ){
        jet->truthLink = truthJet;
        break;
      }
    }

    // Forward jets
    // Until we get them saved from SUSYTools, forward jets outside signal classification
    if( isForwardJet(jet) ){
      this->fJets.push_back(jet);
      this->aJets.push_back(jet);	
     }

    // Signal requirements, only when OR enabled
    if( !m_disableOR && !jet->signal ) continue;

    // Central jets
    if( isCentralJet(jet) ){
      this->cJets.push_back(jet);
      this->aJets.push_back(jet);
    }

    // B-tagged jets
    if( isBtaggedJet(jet) )  this->bJets.push_back(jet);

    // Forward jets
    //if( isForwardJet(jet) )  this->fJets.push_back(jet);
    
  }

}

// ------------------------------------------------------------- //
bool Objects::isCentralJet(ObjectVariable* jet)
{

  if(s_cJet_pt>=0 && jet->Pt() < s_cJet_pt                 ) return false;
  if(s_cJet_eta>=0 && TMath::Abs(jet->Eta()) > s_cJet_eta  ) return false;

  return true;
}
// ------------------------------------------------------------- //
bool Objects::isForwardJet(JetVariable* jet)
{

  if(s_fJet_pt>=0 && jet->Pt() < s_fJet_pt                         ) return false;
  if(s_fJet_eta_min>=0 && TMath::Abs(jet->Eta()) < s_fJet_eta_min  ) return false;
  if(s_fJet_eta_max>=0 && TMath::Abs(jet->Eta()) > s_fJet_eta_max  ) return false;

  return true;
}
// ------------------------------------------------------------- //
bool Objects::isBtaggedJet(JetVariable* jet)
{

  // All b-tagged jets are required to be central jets 
  // Or do we want separate classifications?
  //
  // TODO: Higgsino analysis currently uses lower pT b-jets than our
  // other central jets, so we might improve this classification...
  if( !isCentralJet(jet) ) return false;

  // Consistent with the SFs we have stored
  if( !jet->bjet ) return false;

  // Return gracefully
  return true;

}
// ------------------------------------------------------------------- //
bool Objects::isBadTileJet(ObjectVariable* jet){
  
  const int RN = 
    this->evt.isMC ? 
    this->evt.randomRunNumber :
    this->evt.runNumber       ;

  bool isBad = false;
  for(auto bi : m_badTileDict){
    isBad = (bi.minRN < RN && RN < bi.maxRN)
			&& (bi.minEta < jet->Eta() && jet->Eta() < bi.maxEta)
			&& (bi.minPhi < jet->Phi() && jet->Phi() < bi.maxPhi);
    if(isBad) break;
  }

  return isBad;
}

// ------------------------------------------------------------- //
bool Objects::isCentralFatjet(ObjectVariable* fatjet)
{

  if(s_cFatjet_pt>=0  && fatjet->Pt() < s_cFatjet_pt               ) return false;
  if(s_cFatjet_eta>=0 && TMath::Abs(fatjet->Eta()) > s_cFatjet_eta ) return false;
  if(s_cFatjet_m>=0   && fatjet->M() < s_cFatjet_m                 ) return false;

  return true;
}
// ------------------------------------------------------------- //
void Objects::getFatjets()
{

  for(uint i=0; i<this->fatjets.size(); i++){

    JetVariable* fatjet = this->fatjets[i];
    if( isCentralFatjet(fatjet) ) this->cFatjets.push_back(fatjet);
   
  }

}
// -------------------------------------------------------------------- //
void Objects::getTracks()
{

  for( auto& t : this->tracks ){

    // Filled in getMuons
    if( t->trackType==TrackVariable::TrackType::MSONLY ) continue;

    // Associate a truth particle to this track
    // Use the barcode for association
    for( auto& tP : this->truth ){
      if( tP->isAntiKt4Jet ) continue;
      if( t->barcode==tP->barcode ){
        t->truthLink = tP;
        break;
      }
    }

    // Also associated truth particles to
    // any of the associated tracks!
    for( int a=0; a<t->nAssociatedTracks(); a++){
      auto associatedTrk = t->getAssociatedTrack (a);
      for( auto& tP : this->truth ){  
        if( tP->isAntiKt4Jet ) continue;
        if( associatedTrk->barcode==tP->barcode ){
          associatedTrk->truthLink = tP;
          break;
        }
      } // Loop over truth particles
    } // Loop over all associated tracks

    //  Overlap removal!
    if( !passTrackOR(t) ){
      continue;
    }

    // Standard tracks
    // Same requirements as disappearing tracks,
    // but without disappearing track conditions
    if( t->trackType==TrackVariable::TrackType::STD ){
      //if( IsSignalTrack(t,false) ){
        stdTracks.push_back(t);
     // }
    } 

    // Baseline tracks
    baseTracks.push_back(t);

    // Signal tracks
    // Before and after MS overlap removal
    if( IsSignalTrack(t) ){
      signalTracks.push_back(t);
    }

  } // Loop over tracks


}
// -------------------------------------------------------------------- //
bool Objects::IsSignalTrack(TrackVariable* t,bool doDisappearingTrk)
{

  /*
    Return false if any of the below 
    criteria are not met!
  */

  /*
     Kinematic selection
  */
  if( s_trk_pt>=0.0 && t->Pt() < s_trk_pt                                        ) return false;
  if( s_trk_minEta>=0.0 && fabs(t->Eta()) < s_trk_minEta                         ) return false;
  if( s_trk_maxEta>=0.0 && fabs(t->Eta()) > s_trk_maxEta                         ) return false;

  /*
    Hit requirements
  */
  if( s_trk_nPixLayers>=0 && t->nPixLayers<s_trk_nPixLayers                      ) return false;
  if( s_trk_minPixHits>=0 && t->nPixHits < s_trk_minPixHits                      ) return false;
  if( s_trk_maxPixSpoiltHits>=0 && t->nPixSpoiltHits > s_trk_maxPixSpoiltHits    ) return false;
  if( s_trk_maxGangedFakes>=0 && t->nGangedFlaggedFakes > s_trk_maxGangedFakes   ) return false;
  if( s_trk_nSiHoles>=0 && (t->nPixHoles+t->nSCTHoles)>s_trk_nSiHoles            ) return false;
  if( s_trk_pixOutliers>=0 && t->nPixOutliers>s_trk_pixOutliers                  ) return false;
  if( s_trk_expBLayerHits>=0 && t->nExpBLayerHits<s_trk_expBLayerHits            ) return false;

  /*
    Impact parameter requirements
  */
  if( s_trk_d0Sig>=0.0 && fabs(t->d0/(t->d0Err)) > s_trk_d0Sig                   ) return false;
  if( s_trk_d0>=0 && fabs(t->d0) > s_trk_d0                                      ) return false;
  if( s_trk_z0SinTheta>=0.0 && fabs(t->z0*sin(t->Theta())) > s_trk_z0SinTheta    ) return false;

  /*
    Isolation requirements
  */
  if( s_trk_relTrkIso>=0.0 && (t->ptcone40/t->Pt()) > s_trk_relTrkIso            ) return false;
  if( s_trk_caloIso20>=0.0 && t->etclus20Topo > s_trk_caloIso20                  ) return false;

  /*
   chi^2/nDOF
  */
  if( s_trk_fitQuality>=0 && t->fitQuality < s_trk_fitQuality                    ) return false;

  /*
    Specific requirements for disappearing track conditons
  */
  if( doDisappearingTrk ){

    // SCT hit requirements
    if( s_trk_maxSCTHits>=0 && t->nSCTHits > s_trk_maxSCTHits                    ) return false;

    // Three layer disappearing tracks
    if( t->nPixLayers==3 ){
      this->pixelThreeLayer.push_back(t);
    }
    // Four layer disappearing tracks
    if( t->nPixLayers>=4 ){
      this->pixelFourLayer.push_back(t);
    }
  }

  // Signal track
  return true;



}
// -------------------------------------------------------------------- //
bool Objects::isAssociatedTrk(TrackVariable* trk,TrackVariable* parent )
{

  if( trk->Pt() < 0.3        ) return false;
  if( fabs(trk->Eta()) > 2.0 ) return false;
  if( trk->vtxQuality > 2.0  ) return false;
  if( trk->nSCTHits < 8      ) return false;
  if( trk->nSCTHoles > 1     ) return false;
  if( trk->nSCTOutliers > 0  ) return false;
  if( trk->nPixHits > 0      ) return false;
  if( fabs(trk->d0SV) > 6.0  ) return false;
  if( fabs(trk->z0SV) > 10.0 ) return false;

  if( parent ){
    double vtxR = trk->vtx.Perp();
    if(parent->nPixLayers<=3 && vtxR>119.0        ) return false;
     else if( parent->nPixLayers>=4 && vtxR<123.0 ) return false;
  }

  return true;

}
// -------------------------------------------------------------------- //
void Objects::getTruth()
{

  for( auto& t : this->truth ){

    // Truth electrons
    if( abs(t->pdgId)==11 && IsBaselineElectron(t) ){
      truthLeptons.push_back(t);
    }

    // Truth muons
    if( abs(t->pdgId)==13 && IsBaselineMuon(t) ){
      truthLeptons.push_back(t);
    }

    //VBF TruthVectors
    // Truth status == 3 leptons
    if ( abs(t->pdgId)==11 | abs(t->pdgId)==12 | abs(t->pdgId)==13 | abs(t->pdgId)==14 | abs(t->pdgId)==15 | abs(t->pdgId)==16  )  {
        if (t->status==3) truthStatus3Leptons.push_back(t);
    }

    //Separate truth taus
    if( abs(t->pdgId)==15 ){
      truthTaus.push_back(t);
    }

    //All Truth jets
    if (t->isAntiKt4Jet) {
        // Check for overlap with leptons
        // Exclude those jets
        bool overlap = false;
        for( auto& lep : this->truthLeptons ){
            if( m_truthEleJetDR>=0.0 && abs(lep->pdgId)==11 && lep->DeltaR(*t)<m_truthEleJetDR ) {
                overlap = true;
                break;
            }
            if( m_truthMuJetDR>=0.0 && abs(lep->pdgId)==13 && lep->DeltaR(*t)<m_truthMuJetDR ){  
                overlap = true;
                break;
            }
        }
      if( !overlap ) truthAllJets.push_back(t);
    }
  }

  for( auto& t : this->truth ){

    // Truth jets
    if( t->isAntiKt4Jet && isCentralJet(t) ){

      // Check for overlap with leptons
      // Exclude those jets
      bool overlap = false;
      for( auto& lep : this->truthLeptons ){
        if( m_truthEleJetDR>=0.0 && abs(lep->pdgId)==11 && lep->DeltaR(*t)<m_truthEleJetDR ) {
          overlap = true;
          break;
        }
        if( m_truthMuJetDR>=0.0 && abs(lep->pdgId)==13 && lep->DeltaR(*t)<m_truthMuJetDR ){
          overlap = true;
          break;
        }
      }
      if( !overlap ){
        truthJets.push_back(t);
      }
    }

    // Special containers
    if( abs(t->pdgId)==1000024 ) {
      charginos.push_back(t);
    }

  } // Loop over truth container

}
// -------------------------------------------------------------------- //
void Objects::doOverlap(MuonVector& muons, ElectronVector& electrons, PhotonVector& photons, JetVector& jets)
{

  // Used for truth only analysis

  /*
    Remove jet if deltaR(e,j)<0.2
  */
  for(int e=electrons.size()-1; e>-1; e--){
    if( !IsBaselineElectron(electrons[e]) ) continue;
    for(unsigned int j=0; j<jets.size(); j++){
      if( !isCentralJet(jets[j]) ) continue;
      if( s_user_or_baware && isBtaggedJet(jets[j])  ) continue; // do not kill b-jets!
      float dR = jets[j]->DeltaR(*electrons[e]);
      if(dR <= m_minDr_j_e){
        jets.erase(jets.begin()+j);
      }
    }
  }

  /*
    Remove jet if deltaR(g,j)<0.2
  */
  for(int g=photons.size()-1; g>-1; g--){
    if( !IsBaselinePhoton(photons[g]) ) continue;
    for(unsigned int j=0; j<jets.size(); j++){
      if( !isCentralJet(jets[j]) ) continue;
      if( s_user_or_baware && isBtaggedJet(jets[j])  ) continue; // do not kill b-jets!
      float dR = jets[j]->DeltaR(*photons[g]);
      if(dR <= m_minDr_j_g){
        jets.erase(jets.begin()+j);
      }
    }
  }

  /*
    Remove muon if overlaping with a jet
  */
  for(unsigned int j=0; j<jets.size(); j++){
     if( !isCentralJet(jets[j]) ) continue;
    for(int m=muons.size()-1; m>-1; m--){
      if( !IsBaselineMuon(muons[m]) ) continue;
      float dR = jets[j]->DeltaR(*muons[m]);
      if(dR <= m_minDr_m_j){
        muons.erase(muons.begin()+m);
      }
    }
  }

  /*
    After above e,j remove any electrons if deltaR(j,e) (0.2,0.4]
  */
  for(unsigned int j=0; j<jets.size(); j++){
    if( !isCentralJet(jets[j])) continue;
    for(int e=electrons.size()-1; e>-1; e--){
      if( !IsBaselineElectron(electrons[e]) ) continue;
      float dR = jets[j]->DeltaR(*electrons[e]);
      if(dR <= m_minDr_e_j){
        electrons.erase(electrons.begin()+e);
      }
    }
  }

  /*
    After above g,j remove any photons if deltaR(g,e) (0.2,0.4]
  */
  for(unsigned int j=0; j<jets.size(); j++){
    if( !isCentralJet(jets[j])) continue;
    for(int g=photons.size()-1; g>-1; g--){
      if( !IsBaselinePhoton(photons[g]) ) continue;
      float dR = jets[j]->DeltaR(*photons[g]);
      if(dR <= m_minDr_g_j){
        photons.erase(photons.begin()+g);
      }
    }
  }

}
// -------------------------------------------------------------------- //
void Objects::doOverlap_FatJet(ElectronVector& electrons, PhotonVector& photons, JetVector& fatjets)
{

  /*
    Remove jet if deltaR(e,fatjet)<1.0
  */
  for(int e=electrons.size()-1; e>-1; e--){
    if( !IsBaselineElectron(electrons[e]) ) continue;
    if( !electrons[e]->passOR ) continue;

    for(unsigned int j=0; j<fatjets.size(); j++){
      float dR = fatjets[j]->DeltaR(*electrons[e]);
      if(dR < 1.0)  fatjets.erase(fatjets.begin()+j);      
    }
  }

  /*
    Remove jet if deltaR(gamma,fatjet)<1.0
  */
  for(int g=photons.size()-1; g>-1; g--){
    if( !IsBaselinePhoton(photons[g]) ) continue;
    if( !photons[g]->passOR ) continue;

    for(unsigned int j=0; j<fatjets.size(); j++){
      float dR = fatjets[j]->DeltaR(*photons[g]);
      if(dR < 1.0)  fatjets.erase(fatjets.begin()+j);      
    }
  }

}
// ------------------------------------------------------------- //
bool Objects::passTrackOR(TrackVariable* trk)
{
 
  // Disabled for MSonly and standard tracks for now
  if( trk->trackType==TrackVariable::TrackType::MSONLY ) return true;
  if( trk->trackType==TrackVariable::TrackType::STD    ) return true;

  // Track-electron OR
  if( m_minDr_t_e>=0.0 ){
    for( auto& ele : this->baseElectrons ){
      if( ele->DeltaR(*trk)<m_minDr_t_e ){
        //printf("Tracklet (%f,%f) overlaps with electron (%f,%f) \n",trk->Pt(),trk->Eta(),ele->Pt(),ele->Eta() );
        return false;
      }
    }
  }

  // Track-muon OR
  if( m_minDr_t_m>=0.0 ){
    for( auto& mu : this->baseMuons ){
      if( mu->DeltaR(*trk)<m_minDr_t_m ){
        //printf("Tracklet (%f,%f) overlaps with muon (%f,%f) author %i \n",trk->Pt(),trk->Eta(),mu->Pt(),mu->Eta(),mu->author );
        return false;
      }
    } 
  }

  // Track-jet OR
  if( m_minDr_t_j>=0.0 ){
    for(auto& jet : this->aJets ){
      if( jet->DeltaR(*trk)<m_minDr_t_j ){
        //printf("Tracklet (%f,%f) overlaps with jet (%f,%f) \n",trk->Pt(),trk->Eta(),jet->Pt(),jet->Eta() );
        return false;
      }
    }
  }

  // Passes OR
  return true;

}
// ------------------------------------------------------------- //
bool Objects::hasCosmicMuon()
{

  for( auto& muon : this->baseMuons ){
    if(muon->passOR && muon->cosmic) return true;
  }

  // No cosmic muons
  return false;

}
// ------------------------------------------------------------- //
bool Objects::hasBadJet()
{

  // Search for a bad jet
  for( const auto& jet : this->aJets ){
    if( jet->bad ) return true;
  }

  // Event didn't have a bad jet
  return false;

}
// ------------------------------------------------------------- //
bool Objects::hasBadTileJet()
{

  // Search for a bad tile jet
  for( const auto& jet : this->aJets ){
    if( jet->badTile ) return true;
  }

  // Event didn't have a bad tile jet
  return false;

}
// ------------------------------------------------------------- //
bool Objects::hasBadMuon()
{

  // Veto events with any muon
  // classified as bad

  // FIXME: decide whether to keep this version of bad muon or the other one
  /*for( const auto& mu : this->baseMuons ){
    if( mu->bad ) return true;
  }*/

  // Note: this way applies the veto before considering OR, as described in
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/SusyObjectDefinitionsr2013TeV#Muons
  for(unsigned int i=0; i<this->muons.size(); i++){
    MuonVariable* mu = this->muons[i];
    if( IsBaselineMuon(mu) && mu->bad) return true;
  }

  return false;

}
// ------------------------------------------------------------- //
bool Objects::passEventCleaning(CutFlowTool*& cutflow, TString stream, double weight)
{

  ///
  /// Apply all recommended event cleaning cuts!
  ///

  if( !this->evt.passEvtCleaning( EventVariable::GRL ) ) return false;
  cutflow->bookCut(stream,"Pass good run list (only data)", weight );

  if( !this->evt.passEvtCleaning( EventVariable::LAr ) ) return false;
  cutflow->bookCut(stream,"passLAr", weight );

  if( !this->evt.passEvtCleaning( EventVariable::Tile ) ) return false;
  cutflow->bookCut(stream,"passTile", weight );

  if( !this->evt.passEvtCleaning( EventVariable::IncompleteEvt ) ) return false;
  cutflow->bookCut(stream,"passIncompleteEvt", weight );

  if( !this->evt.passEvtCleaning( EventVariable::SCT ) ) return false;
  cutflow->bookCut(stream,"Pass SCT cleaning cuts", weight );

  if( !this->evt.passEvtCleaning( EventVariable::PrimVtx ) ) return false;
  cutflow->bookCut(stream,"PV with >=2 tracks", weight );

  if( m_applyCosmicMuonVeto ){
    if( this->hasCosmicMuon() ) return false;
    cutflow->bookCut(stream,"Pass cosmic veto", weight );
  }

  if( m_applyBadMuonVeto ){
    if( !this->evt.passEvtCleaning( EventVariable::BadMuon ) ) return false;    
    cutflow->bookCut(stream,"Pass bad muon veto", weight );
  }

  if( m_applyBadTileJetVeto ){
    if( this->hasBadTileJet() ) return false;
    cutflow->bookCut(stream,"Pass bad tile jet veto", weight );
  }

  // Jet level Jet Cleaning
  if( m_applyBadJetVeto ){
    if( !this->evt.passEvtCleaning( EventVariable::BadJet ) ) return false;
    cutflow->bookCut(stream,"Pass bad jet veto", weight );
  }

  // Event level Jet Cleaning
  if( m_applyEvtLooseBadJetVeto ){
    if( !this->evt.passEvtCleaning( EventVariable::EvtLooseBadJet ) ) return false;
    cutflow->bookCut(stream,"Pass loose bad jet event veto", weight );
  }

  if( m_applyEvtTightBadJetVeto ){
    if( !this->evt.passEvtCleaning( EventVariable::EvtTightBadJet ) ) return false;
    cutflow->bookCut(stream,"Pass tight bad jet event veto", weight );
  }

  // MET Cleaning cut
  if( m_applyTSTCleaning ){
    if( (this->met.Et_soft / this->met.Et ) > 0.4 ) return false;
    cutflow->bookCut(stream,"Pass TST/MET<0.4 cleaning cut", weight ); 
  }

  // Passes cleaning cuts!
  return true;

}
// ------------------------------------------------------------- //
void Objects::print(objLevel level)
{

  if(level==SKIM){
    std::cout << std::endl;
    std::cout << "========================== Cuts applied to skim objects ============================" << std::endl; 
    for(unsigned int i=0; i<m_cutDic.size(); i++){
      std::cout << std::left << std::setw(60) << m_cutDic[i].first << std::setw(10) <<  m_cutDic[i].second  << std::endl;   
    }
    std::cout << "====================================================================================" << std::endl; 
    std::cout << std::endl;
  }

 
}
// ------------------------------------------------------------- //
std::vector< std::pair<std::string,float> > Objects::getLog()
{

  // First store all the configuration 
  collectCuts();

  return m_log;

}
// ------------------------------------------------------------- //
void Objects::collectCuts()
{

  defineCutName("Signal electron: Et",                   s_electron_et);
  defineCutName("Signal electron: eta",                  s_electron_eta);
  defineCutName("Signal electron: relative isolation",   s_electron_relIso);
  defineCutName("Signal electron: d0",                   s_electron_d0);
  defineCutName("Signal electron: z0 )",                 s_electron_z0);
  defineCutName("Signal muon: pT",                       s_muon_pt);
  defineCutName("Signal muon: eta",                      s_muon_eta);
  defineCutName("Signal muon: absolute isolation",       s_muon_absIso);
  defineCutName("Signal muon: d0",                       s_muon_d0);
  defineCutName("Signal muon: z0 ",                      s_muon_z0);
  defineCutName("Signal jet: central jet pT",            s_cJet_pt);
  defineCutName("Signal jet: central jet eta",           s_cJet_eta);
  defineCutName("Signal jet: forward jet pT",            s_fJet_pt);
  defineCutName("Signal jet: forwatd jet min eta",       s_fJet_eta_min);
  defineCutName("Signal jet: forwatd jet max eta",       s_fJet_eta_max);
  defineCutName("Signal jet: central fat jet pT",        s_cFatjet_pt);
  defineCutName("Signal jet: central fat jet eta",       s_cFatjet_eta);
  defineCutName("Signal jet: central fat jet min mass",  s_cFatjet_m);
  defineCutName("Signal tau: pT",                        s_tau_pt);
  defineCutName("Signal tau: eta",                       s_tau_eta);
  //defineCutName("Overlap removal: muon-jet",             m_minDr_m_j);
  //defineCutName("Overlap removal: jet-electron",         m_minDr_j_e);
  //defineCutName("Overlap removal: electron-jet",         m_minDr_e_j);

}
// ------------------------------------------------------------- //
void Objects::defineCutName(std::string name,float variable)
{
  m_cutDic.push_back( std::pair<std::string,float >(name,variable) );
  m_log.push_back(    std::pair<std::string,float >(name,variable) );
}
// ------------------------------------------------------------- //
bool Objects::skimObjectsExist() const
{

  if( !this->skimEvt         ) return false;
  if( !this->skimMet         ) return false; 
  if( !this->skimMuons       ) return false;
  if( !this->skimElectrons   ) return false;
  if( !this->skimTaus        ) return false;
  if( !this->skimJets        ) return false;
  if( !this->skimFatjets     ) return false;
  if( !this->skimTrackjets   ) return false;
  if( !this->skimPhotons     ) return false;

  // All objects exist
  return true;

}
// ------------------------------------------------------------- //
Objects::~Objects()
{
    delete skimEvt;
    delete skimMet;
    delete skimMuons;
    delete skimElectrons;
    delete skimTaus;
    delete skimJets;
    delete skimFatjets;
    delete skimTrackjets;
    delete skimPhotons;
    delete skimTracks;
    delete skimCalo;
    delete skimTruth;
    delete skimTruthEvent;
    delete skimTruthEvent_Vjets;
    delete skimTruthEvent_VV;
    delete skimTruthEvent_VVV;
    delete skimTruthEvent_TT;
    delete skimTruthEvent_XX;
    delete skimTruthEvent_GG;
}
