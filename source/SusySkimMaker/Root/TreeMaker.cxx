#include "SusySkimMaker/TreeMaker.h"
#include "SusySkimMaker/Objects.h"

#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"

// ----------------------------------------------------------------------------------- //
TreeMaker::TreeMaker() : m_writeLHE3(false),
                         m_writeLHE3_flat(false),
                         m_isFirstFill(true)
{

  m_sysStringVector.clear();
  m_streams.clear();

  Reset();

}
// ----------------------------------------------------------------------------------- //
void TreeMaker::addDefaultVariables()
{
  TString streamState = m_globalStream;
  TString sysState    = m_sysName; 

  for( auto stream : m_streams ){
    for( auto sys :  m_sysStringVector ){

     // Only for tree stream
     if(stream.Contains("skim")) continue;

     this->setStreamState(stream);
     this->setTreeState(sys);

     this->addULongLongVariable("PRWHash",0);
     this->addULongLongVariable("EventNumber",-999);
     this->addFloatVariable("xsec",0.0);
     this->addFloatVariable("GenHt",0.0);
     this->addFloatVariable("GenMET",0.0);
     this->addIntVariable("DatasetNumber",-999);
     this->addIntVariable("RunNumber",-999);
     this->addIntVariable("RandomRunNumber",-999);
     this->addIntVariable("FS",0);

     if(m_writeLHE3 && !m_writeLHE3_flat){
       this->addVecFloatVariable("LHE3Weights",true);        // nominalOnly=true -> fill only in the nominal trees
       this->addVecTStringVariable("LHE3WeightNames",true);  // nominalOnly=true -> fill only in the nominal trees
     }

    } 
  }

  // Return back to state user called this function with
  m_globalStream = streamState;
  m_sysName = sysState;

}
// ----------------------------------------------------------------------------------- //
void TreeMaker::setDefaultVariables(Objects* obj)
{

  TString streamState = m_globalStream;
  TString sysState    = m_sysName;

  for( auto stream : m_streams ){
    for( auto sys : m_sysStringVector ){

     // Only for tree stream 
     if(stream.Contains("skim")) continue;

     this->setStreamState(stream);
     this->setTreeState(sys);

     this->setULongLongVariable("PRWHash",obj->evt.PRWHash);
     this->setULongLongVariable("EventNumber",obj->evt.evtNumber);
     this->setFloatVariable("xsec",obj->evt.xsec);
     this->setFloatVariable("GenHt",obj->evt.GenHt);
     this->setFloatVariable("GenMET",obj->evt.GenMET);
     this->setIntVariable("DatasetNumber",obj->evt.dsid);
     this->setIntVariable("RunNumber",obj->evt.runNumber);
     this->setIntVariable("RandomRunNumber",obj->evt.randomRunNumber);
     this->setIntVariable("FS",obj->evt.FS);

     // Fill branches for LHE3 weights (only for nominal tree)
     if(m_writeLHE3 && sys==""){

        MCCorrElement* LHEvariations = obj->evt.MCCorr.getElement("LHE3Weight");

        if(LHEvariations){	     
	 std::vector<TString> names;
	 std::vector<float> weights;

	    for( auto var : LHEvariations->getSF() ){
            TString varName = var.first;

	    // Skip default weight and metadata (for Sherpa)
	    //if(varName=="NTrials" || varName=="Weight" || varName=="WeightNormalisation" || varName=="MEWeight") continue;
	    if(varName=="NTrials" || varName=="Weight" || varName=="WeightNormalisation") continue;
	       
            float sf = var.second;
            weights.push_back(sf);
            names.push_back(varName);

            if(m_writeLHE3_flat){
              // Define the branch if not yet
              if(m_isFirstFill) {
                std::cout << "<TreeMaker::setDefaultVariables> INFO Creating branch LHE3Weight_"+varName << " on the fly" << std::endl;
                this->addFloatVariable("LHE3Weight_"+varName,1.0);
              }
              // Fill the relative event weight w.r.t the nominal	  
              this->setFloatVariable("LHE3Weight_"+varName,sf);	 
            }

          } // loop over variations

          if(!m_writeLHE3_flat){
            this->setVecFloatVariable("LHE3Weights", weights);	 
            this->setVecTStringVariable("LHE3WeightNames", names);	 
          }

        } // if(LHEvariations)

      } // if(m_writeLHE3)

    } 
  }       

  // Turn off the on-the-fly branch definition
  m_isFirstFill = false;
    
  // Return back to state user called this function with
  m_globalStream = streamState;
  m_sysName = sysState;
  
}
// ----------------------------------------------------------------------------------- //
void TreeMaker::createTrees(TFile*& file, TString stream)
{

  // Save TFile into global map for later use
  addOutputFile(stream,file);

  // Create the output files and save into file
  createTrees(stream);

}
// ----------------------------------------------------------------------------------- //
void TreeMaker::createTrees(TString file, TString stream)
{

  // Save TFile into global map for later use
  addOutputFile(stream,file);

  // Create the output files and save into file
  createTrees(file);

}
// ----------------------------------------------------------------------------------- //
void TreeMaker::createTrees(TString stream)
{

  std::map<TString,TFile*>::iterator fileFinder = m_outputFiles.find(stream);	

  if( fileFinder == m_outputFiles.end() ){
    std::cout << "<TreeMaker::createTrees> FATAL ERROR: No root file found for this stream : " << stream << std::endl;
    abort();
  }

  if( !fileFinder->second->IsOpen() ){
    std::cout << "TreeMaker::ERROR Rootfile you wish to create a tree in is not open! Exiting!" << std::endl;
    abort();
  }

  std::cout << "<TreeMaker::createTrees> INFO Associating to root file: " << fileFinder->second->GetName() << std::endl;

  // Keep track of all streams user creates
  m_streams.push_back(stream);

  for(unsigned int s=0; s<m_sysStringVector.size(); s++){

    // Naming structure always called from getFinalTreeName
    TString finalTreeName = getFinalTreeName(stream,m_sysStringVector[s]);
    //
    std::cout << "<TreeMaker::createTrees> INFO Creating an output tree named: " << finalTreeName << std::endl;
    //
    TTree* myTree = new TTree(finalTreeName,finalTreeName);
    // Set autosave size (determines how often tree writes to disk)
    myTree->SetAutoSave(100000000);
    // Max tree size determines when a new file and tree are written
    myTree->SetMaxTreeSize(2000000000000);
    //
    myTree->SetDirectory(fileFinder->second);
    // Save tree into global TTree map
    m_trees.insert( std::pair<TString,TTree*>(finalTreeName,myTree) );

  }

}
// ----------------------------------------------------------------------------------- //
void TreeMaker::addOutputFile(TString fileName,TFile* file)
{

  std::cout << "<TreeMaker::addOutputFile> INFO Adding an output file: " << fileName << std::endl;

  m_outputFiles.insert( std::pair<TString,TFile*>(fileName,file) );
}
// ----------------------------------------------------------------------------------- //
void TreeMaker::addOutputFile(TString fileName,TString file)
{

  std::cout << "<TreeMaker::addOutputFile> INFO Searching for a file name : " << file << std::endl;

  // First check if a file with this name has been added already
  std::map<TString,TFile*>::iterator it = m_outputFiles.find(file);
  
  // Already a TFile with this name, use it!
  if( it != m_outputFiles.end() ){
    std::cout << "<TreeMaker::addOutputFile> INFO Using already existing TFile " << file << std::endl;
    // Just copy it, this could be done better?
    m_outputFiles.insert( std::pair<TString,TFile*>(fileName,it->second) );
  }
  else{
    TFile* outputFile = new TFile(fileName,"RECREATE");
    m_outputFiles.insert( std::pair<TString,TFile*>(file,outputFile) );
  }

}
// ----------------------------------------------------------------------------------- //
TFile* TreeMaker::getFile(TString stream)
{

  std::map<TString,TFile*>::iterator fileFinder = m_outputFiles.find(stream);

  if( fileFinder == m_outputFiles.end() ){
    std::cout << " <TreeMaker::getFile> WARNING	Cannot find file for stream: " << stream << ". Returning NULL." << std::endl;
    return NULL;
  }

  return fileFinder->second;

}
// ----------------------------------------------------------------------------------- //
TTree* TreeMaker::getTree(TString prefix,TString sysName)
{

  std::map<TString,TTree*>::iterator it = m_trees.find( getFinalTreeName(prefix,sysName) );

  if(it!=m_trees.end()) return it->second;
  else                  return NULL;

}
// ----------------------------------------------------------------------------------- //
void TreeMaker::addFloatVariables( TString prefix, std::vector<TString> branches, float init_value, bool nominalOnly)
{

  if( !prefix.IsWhitespace() ) prefix += "_";

  for(unsigned int i=0; i<branches.size(); i++){
    addFloatVariable( prefix+branches[i],init_value,nominalOnly);
  }

}
// ----------------------------------------------------------------------------------- //
void TreeMaker::addBoolVariables( std::vector<TString> branches, bool init_value, bool nominalOnly)
{

  for(unsigned int i=0; i<branches.size(); i++){
    addBoolVariable(branches[i],init_value,nominalOnly);
  }

}
// ----------------------------------------------------------------------------------- //
void TreeMaker::Fill(TString sysName,TString stream)
{

  std::vector<TString> streams;
  streams.clear();

  if(stream.IsWhitespace()){
    streams = m_streams;
  }
  else{
   streams.push_back(stream);
  }

  for(unsigned int i=0; i<streams.size();i++){

    TString finalTreeName = getFinalTreeName( streams[i] , sysName );
    //    std::cout << "finalTreeName " << finalTreeName << std::endl;

    //
    std::map<TString,TTree*>::iterator it = m_trees.find(finalTreeName);
    //
    if(it==m_trees.end()){
      //std::cout << "<TreeMaker::Fill> WARNING Could not find tree for systematic: " << sysName << std::endl;
      //std::cout << "<TreeMaker::Fill> Tree name is: " << finalTreeName << std::endl;
      continue;
    }
    //
    //it->second->Print();
    it->second->Fill();
  }

}
// ----------------------------------------------------------------------------------- //
void TreeMaker::Write()
{

  std::map<TString,TTree*>::iterator it;
  std::map<TString,TFile*>::iterator file;

  std::cout << "=======================================" << std::endl;
  for(it = m_trees.begin(); it != m_trees.end(); it++){

    TString fileName =  ((TObjString*)it->first.Tokenize("_")->At(0))->String();

    file = m_outputFiles.find(fileName);

    if( file == m_outputFiles.end() ){
      std::cout << " <TreeMaker::Write> WARNING No output file to write tree! " << std::endl;
      continue;
    }

    file->second->cd();

    std::cout << "  => TreeMaker::Writing tree " << it->first << std::endl;
    it->second->Write();

  }  
  std::cout << "=======================================" << std::endl;

}
// ----------------------------------------------------------------------------------- //
std::vector<TString> TreeMaker::getStream()
{

  std::vector<TString> streams;
  streams.clear();

  // User only wants these variables assigned to this output stream
  if(m_globalStream != ""){
    streams.push_back(m_globalStream);
  }
  // Add this variable too all streams
  else{
    streams = m_streams;
  }
 
  return streams;

}
// ----------------------------------------------------------------------------------- //
std::vector<TString> TreeMaker::getSys()
{

  std::vector<TString> sysStringVector;
  sysStringVector.clear();

  // If the user only wants these variables assigned to a certain tree
  if(m_sysName!="None"){
    sysStringVector.push_back(m_sysName);
  }
  // Add these variables to all user defined trees
  else{
    sysStringVector = m_sysStringVector;
  }

  return sysStringVector;

}
// ----------------------------------------------------------------------------------- //
TString TreeMaker::getFinalTreeName(TString prefix, TString sys)
{

  TString finalTreeName = "";

  if( !sys.IsWhitespace() ){
    finalTreeName = prefix+"_"+sys;
  }
  // TODO: How else can we do this, such that we don't depend on the string name of the tree
  else if( sys.IsWhitespace() && !prefix.Contains("data",TString::kIgnoreCase) ){
    finalTreeName = prefix+"_NoSys";
  }
  else{
    finalTreeName = prefix;
  }

  return finalTreeName;

}
// ----------------------------------------------------------------------------------- //
TString TreeMaker::formatBranchName(TString branchName)
{
  // Valid C++ variable name cannot have a '-' character in it.
  // Since some triggers have this char in it, let's replace
  // it with a '_'
  if(branchName.Contains("-")){
    branchName.ReplaceAll("-","_");
  }

  return branchName;
}
// ----------------------------------------------------------------------------------- //
void TreeMaker::Reset()
{

  m_sysName      = "None";
  m_globalStream = "";

  // Reset all maps to their default values
  resetVariable(m_cutDic_float);
  resetVariable(m_cutDic_double);
  resetVariable(m_cutDic_int);
  resetVariable(m_cutDic_bool);
  resetVariable(m_cutDic_ulonglong);

  // TLorentzVector -> Set to default: (0,0,0,0)
  resetVariable(m_cutDic_tlv, TLorentzVector(0,0,0,0) );

  // Vectors -> Just clear
  resetVecVariable(m_cutDic_vecInt);
  resetVecVariable(m_cutDic_vecFloat);
  resetVecVariable(m_cutDic_vecBool);
  resetVecVariable(m_cutDic_vecTString);

}
// ----------------------------------------------------------------------------------- //
TreeMaker::~TreeMaker()
{

}
