#include "SusySkimMaker/TauObject.h"
#include "SusySkimMaker/CentralDB.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "TauAnalysisTools/TauSelectionTool.h"
#include "SusySkimMaker/TreeMaker.h"
#include "SusySkimMaker/Timer.h"
#include "AsgMessaging/StatusCode.h"

const char* APP_NAME = "TauObject";

TauObject::TauObject() : m_looseTauSelTool(0),
                         m_mediumTauSelTool(0),
                         m_tightTauSelTool(0)
{

}
// ------------------------------------------------------------------------------------------------------------------- //
StatusCode TauObject::init(TreeMaker*& treeMaker)
{

  const char* APP_NAME = "TauObject";

  for(auto& sysName : treeMaker->getSysVector()){
    TauVector* tau  = new TauVector();
    // Save into tau vector into map
    m_tauVectorMap.insert( std::pair<TString,TauVector*>(sysName,tau) );
    // Get tree created by createTrees
    TTree* sysTree = treeMaker->getTree("skim",sysName);
    // Don't write it out
    if(sysTree==NULL) continue;
    else{
      Info("TauObject::init","Adding a branch taus to skim tree: %s ",sysTree->GetName() );
      std::map<TString,TauVector*>::iterator tauItr = m_tauVectorMap.find(sysName);
      sysTree->Branch("taus",&tauItr->second);
    }
  }

  //
  CHECK( init_tools() );

  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------------------------------------------------- //
StatusCode TauObject::init_tools()
{

  CentralDB::retrieve(CentralDBFields::UNITS,m_convertFromMeV);

  // TODO: Come from centralDB
  std::string loose_inputfile = "SUSYTools/tau_selection_loose.conf";
  std::string medium_inputfile = "SUSYTools/tau_selection_medium.conf";
  std::string tight_inputfile = "SUSYTools/tau_selection_tight.conf";

  // Loose
  m_looseTauSelTool = new TauAnalysisTools::TauSelectionTool("TauObject__LooseTauSelectionTool");
  CHECK( m_looseTauSelTool->setProperty("ConfigPath", loose_inputfile) );
  CHECK( m_looseTauSelTool->initialize() );

  // Medium
  m_mediumTauSelTool = new TauAnalysisTools::TauSelectionTool("TauObject__MediumTauSelectionTool");
  CHECK( m_mediumTauSelTool->setProperty("ConfigPath", medium_inputfile) );
  CHECK( m_mediumTauSelTool->initialize() );

  // Tight
  m_tightTauSelTool = new TauAnalysisTools::TauSelectionTool("TauObject__TightTauSelectionTool");
  CHECK( m_tightTauSelTool->setProperty("ConfigPath", tight_inputfile) );
  CHECK( m_tightTauSelTool->initialize() );

  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------------------------------------------------- //
void TauObject::fillTauContainer(xAOD::TauJet* tau_xAOD, const xAOD::VertexContainer* primVertex, std::string sys_name,std::map<TString,float> recoSF)
{

  Timer::Instance()->Start( "TauObject::fillTauContainer" );

  std::map<TString,TauVector*>::iterator it = m_tauVectorMap.find(sys_name);

  if( it==m_tauVectorMap.end() ){
    std::cout << "<TauObject::fillTauContainer> ERROR Request to get tau for unknown systematic: " << sys_name << std::endl;
    return;
  }

  TauVariable* tau = new TauVariable();

  // TLV
  tau->SetPtEtaPhiM( tau_xAOD->pt() * m_convertFromMeV,
		     tau_xAOD->eta(), 
		     tau_xAOD->phi(),
		     tau_xAOD->m() * m_convertFromMeV );
  // Charge
  tau->q = tau_xAOD->charge();

  // Track information
  tau->nTrack = tau_xAOD->nTracks();

  // Truth information
  tau->type   = 0;
  tau->origin = 0;

  // PID
  tau->loose       = (bool)m_looseTauSelTool->accept( *tau_xAOD );
  tau->medium      = (bool)m_mediumTauSelTool->accept( *tau_xAOD );
  tau->tight       = (bool)m_tightTauSelTool->accept( *tau_xAOD );

  // Reco SFs
  tau->recoSF = recoSF;

  // 
  it->second->push_back(tau);

  // Maybe useful for the future
  // so keep around
  (void)primVertex;

  Timer::Instance()->End( "TauObject::fillTauContainer" );

}
// ------------------------------------------------------------------------------------------------------------------- //
const TauVector* TauObject::getObj(TString sysName)
{

  std::map<TString,TauVector*>::iterator it = m_tauVectorMap.find(sysName);

  if(it==m_tauVectorMap.end()){
    std::cout << "TauObject::getBasicObj::Cannot get tau vector for systematic: " << sysName << std::endl;
    return NULL;
  }

  return it->second;

}
// ------------------------------------------------------------------------------------------------------------------- //
void TauObject::Reset()
{
  std::map<TString,TauVector*>::iterator it;
  for(it = m_tauVectorMap.begin(); it != m_tauVectorMap.end(); it++){

    // Free up memory
    for (TauVector::iterator tauItr = it->second->begin(); tauItr != it->second->end(); tauItr++) {
      delete *tauItr;
    }

    it->second->clear();
  }
}
// ------------------------------------------------------------------------------------------------------------------- //
TauObject::~TauObject()
{

  if( m_looseTauSelTool  ) delete m_looseTauSelTool;
  if( m_mediumTauSelTool ) delete m_mediumTauSelTool;
  if( m_tightTauSelTool  ) delete m_tightTauSelTool;

}

