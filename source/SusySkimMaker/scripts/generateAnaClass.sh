#!/bin/bash

#
# Script to generate an analysis class
# for SusySkimMaker framework
#
# Written by Matthew Gignac (UBC,2017)
#

if [[ $# < 2 ]]; then
  echo  "Please pass class name to this script..."
  echo "   => Usage ./generateAnaClass.sh <ClassName> <PackageName> " 
  exit
fi

CLASSNAME="$1"
PACKAGENAME="$2"

# Header file
HEADER="$PACKAGENAME/$CLASSNAME.h"
SOURCE="Root/$CLASSNAME.cxx"

# Move into package
cd ../../$PACKAGENAME

# Helper functions
function addToHeader
{
  echo "$1" >> $HEADER
}
function addToSource
{
  echo "$1" >> $SOURCE
}


addToHeader "#ifndef ${PACKAGENAME}_${CLASSNAME}_h"
addToHeader "#define ${PACKAGENAME}_${CLASSNAME}_h"

addToHeader ""
addToHeader "//RootCore"
addToHeader "#include \"SusySkimMaker/BaseUser.h\""
addToHeader ""

addToHeader "class $CLASSNAME : public BaseUser"
addToHeader "{"
addToHeader ""
addToHeader ' public:'
addToHeader "  $CLASSNAME();"
addToHeader "  ~$CLASSNAME() {};"
addToHeader ""
addToHeader "  void setup(ConfigMgr*& configMgr);"
addToHeader "  bool doAnalysis(ConfigMgr*& configMgr);"
addToHeader "  bool passCuts(ConfigMgr*& configMgr);"
addToHeader "  void finalize(ConfigMgr*& configMgr);"
addToHeader ""
addToHeader "  bool init_merge(MergeTool*& /*mergeTool*/){return true; } "
addToHeader "  bool execute_merge(MergeTool*& /*mergeTool*/){return true;} "
addToHeader ""
addToHeader ""
addToHeader "};"
addToHeader ""
addToHeader "static const BaseUser* ${CLASSNAME}_instance __attribute__((used)) = new ${CLASSNAME}();"
addToHeader ""
addToHeader "#endif"



addToSource "#include \"$PACKAGENAME/$CLASSNAME.h\""
addToSource ""
addToSource ""
addToSource "// ------------------------------------------------------------------------------------------ //"
addToSource "$CLASSNAME::$CLASSNAME() : BaseUser(\"${PACKAGENAME}\",\"${CLASSNAME}\")"
addToSource "{"
addToSource ""
addToSource "}"
addToSource "// ------------------------------------------------------------------------------------------ //"
addToSource "void $CLASSNAME::setup(ConfigMgr*& configMgr)"
addToSource "{"
addToSource ""
addToSource "  // Define any variables you want to write out here. An example is given below"
addToSource "  configMgr->treeMaker->addFloatVariable(\"met\",0.0);"
addToSource ""
addToSource "  // Make a cutflow stream"
addToSource "  configMgr->cutflow->defineCutFlow(\"cutFlow\",configMgr->treeMaker->getFile(\"tree\"));" 
addToSource ""
addToSource ""
addToSource "  // Object class contains the definitions of all physics objects, eg muons, electrons, jets"
addToSource "  // See SusySkimMaker::Objects for available methods; configMgr->obj"
addToSource ""
addToSource "}"
addToSource "// ------------------------------------------------------------------------------------------ //"
addToSource "bool $CLASSNAME::doAnalysis(ConfigMgr*& configMgr)"
addToSource "{"
addToSource ""
addToSource "  /*"
addToSource "    This is the main method, which is called for each event"
addToSource "  */"
addToSource ""
addToSource "  // Skims events by imposing any cuts you define in this method below"
addToSource "  if( !passCuts(configMgr) ) return false;"
addToSource ""
addToSource "  // Fill output trees, build observables, what ever you like here."
addToSource "  // You need to define the variable above in setup(...), before filling here"
addToSource "  configMgr->treeMaker->setFloatVariable(\"met\",configMgr->obj->met.Et );"
addToSource ""
addToSource "  // Fill the output tree"
addToSource "  configMgr->treeMaker->Fill(configMgr->getSysState(),\"tree\");"
addToSource ""
addToSource "  return true;"
addToSource ""
addToSource "}"
addToSource "// ------------------------------------------------------------------------------------------ //"
addToSource "bool $CLASSNAME::passCuts(ConfigMgr*& configMgr)"
addToSource "{"
addToSource ""
addToSource "  /*"
addToSource "   This method is used to apply any cuts you wish before writing"
addToSource "   the output trees"
addToSource "  */"
addToSource ""
addToSource "  double weight = configMgr->objectTools->getWeight(configMgr->obj);"
addToSource ""
addToSource "  // Fill cutflow histograms"
addToSource "  configMgr->cutflow->bookCut(\"cutFlow\",\"allEvents\",weight );"
addToSource ""
addToSource "  // Apply all recommended event cleaning cuts"
addToSource "  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, \"cutFlow\", weight ) ) return false;"
addToSource ""
addToSource "  return true;"
addToSource ""
addToSource "}"
addToSource "// ------------------------------------------------------------------------------------------ //"
addToSource "void $CLASSNAME::finalize(ConfigMgr*& configMgr)"
addToSource "{"
addToSource ""
addToSource "  /*"
addToSource "   This method is called at the very end of the job. Can be used to merge cutflow histograms "
addToSource "   for example. See CutFlowTool::mergeCutFlows(...)"
addToSource "  */"
addToSource ""
addToSource "}"
addToSource "// ------------------------------------------------------------------------------------------ //"

echo ""
echo "Finshed setting up \"$CLASSNAME\" in the package \"$PACKAGENAME\" ..."
echo "Remember you must compile to use your package: rc find_packages; rc compile"
echo ""

