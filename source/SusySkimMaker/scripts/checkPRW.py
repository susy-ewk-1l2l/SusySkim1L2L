#!/usr/bin/python

import ROOT
import os,sys,argparse


def getSUSYToolsList() :

  mylist = [];

  f = ROOT.TFile.Open( "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/merged_prw_mc15c_latest.root", "READ" )
  if not f or f.IsZombie():
      raise "Couldn't open file %s" % fileName
  f.cd("PileupReweighting")

  for key in ROOT.gDirectory.GetListOfKeys():
    if 'pileup_chan' in key.GetName() :
      print 'Adding %s PRW profile to SUSYTools list' % key.GetName()
      mylist.append( key.GetName() )

  f.Close();

  return mylist


####################

# Load in SUSYTools profiles
SUSYToolsList = getSUSYToolsList()

# List of files we want to checl
file_path='../data/PRW/merged_prw.root'
myfile = ROOT.TFile.Open( file_path, "READ" )
if not myfile or myfile.IsZombie():
    raise "Couldn't open file %s" % fileName
myfile.cd("PileupReweighting")

# Loop over and check if they are in SUSYTools list
for key in ROOT.gDirectory.GetListOfKeys():

  print "Trying to match %s " % key.GetName()
  for st in SUSYToolsList :
    if st==key.GetName():
      print st

    
