# SusySkim1L2L

This is where the SusySkim1L2L code lives and includes selectors for the 1L/2L search.

See [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisFramework)
for more details on the SusySkimAna code that our package relies on.

Currently based on `AnalysisBase,25.2.32`

### Overview
1. [Setup Instructions](#Setup-Instructions)
2. [Running Locally](#Running-Locally)
3. [Large-Scale Productions on the Grid](#Running-On-the-Grid)
4. [Remarks to Continous Integration](#Remarks-to-Continous-Integration)

## Setup Instructions

Remark: The tool ```acm``` (ATLAS compilation/package management) provides some useful wrappers for setting up the AnalysisRelease, compiling the code, etc. to make life a bit easier and is used in the following instructions. It is documented [here](https://gitlab.cern.ch/atlas-sit/acm/blob/master/README.md) and available after calling ```setupATLAS```.

Another remark: `setupATLAS` now sets up a python3 environment as default it recommended to manually restrict to python2 via `setupATLAS -2`.

### First Time Setup
Clone (this branch of) repository recursively with all submodules

    git clone --recursive ssh://git@gitlab.cern.ch:7999/susy-ewk-1l2l/SusySkim1L2L.git;

Setup the Analysis Release
    
    cd SusySkim1L2L
    mkdir build; cd build;
    setupATLAS
    acmSetup AnalysisBase,25.2.32

**Note:** Put your r25 packages in the ```package_filters.txt``` in order to let CMake know that you want to compile only them, otherwise you will end up with a lot of errors!

To compile just type

    acm compile

### Subsequent Setups
For any further setups using the same Analysis Release it is sufficient to execute
```
acmSetup
```
within the ```build``` directory. In case new packages are added it may be
required to run ```acm find_packages``` before compiling.


## Running Locally
Below some example commands to run the selectors locally can be found.

**Remarks:** in case you are processing
* AFII files (such as signal samples typically), add the `--isAf2` flag
* data files, the add `--isData` flag

the commands below.

### Running locally
Currently the [OneLep Selector](source/SusySkim1LInclusive/Root/OneLep.cxx) implements the selection for the 1L/2L search.
It can be run locally for a Run 2 dataset with
```
run_xAODNtMaker -F /eos/atlas/atlascerngroupdisk/phys-hmbs/susy-ewk/EWK1L2L_ANA-SUSY-2024-05/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_s3681_r13167_p6266/DAOD_PHYS.40034090._000001.pool.root.1 -writeTrees 1 -selector OneLep  -MaxEvents 200 -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config
```
and for a Run 3 dataset with
```
run_xAODNtMaker -F /eos/atlas/atlascerngroupdisk/phys-hmbs/susy-ewk/EWK1L2L_ANA-SUSY-2024-05/mc23_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e8514_s4159_r15530_p6266/DAOD_PHYS.40055558._000001.pool.root.1 -writeTrees 1 -selector OneLep  -MaxEvents 500 -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config
```


For single files use '-F', instead for datasets use '-s'.


## Running On the Grid
To govern large-scale production on the grid group sets containing multiple samples can be defined (see [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisFramework#Large_group_productions)). These group sets can then be submitted, and later downloaded and merged in one go. This requires to additionally load the following

```
lsetup panda
lsetup pyami
lsetup "rucio -w"
```

and to set up a voms proxy.
 
### Submitting to Grid
To submit a group set

```
run_xAODNtMaker -groupSet MYGROUPSET  -selector MYSELECTOR -tag MYTAG --submitToGrid
```

**Note:** It's recommended to specify a destination of the grid output via the `-destinationSite` flag,
otherwise the output will be stored on some scratch disk where it will deleted after 2 weeks or so.
Ideally, the destination is a local group disk or something similar where there is
no constraint on the lifetime and the ntuple production can be downloaded also at a later
stage again if needed.

**Note2:** Currently it seems to be needed to give the grid some hint how many files of a sample should be
processed per job. Hence, it beneficial to add `-nGBPerJob 15` to the submission command.

For Run 3 data (for example, 2022)
```
run_xAODNtMaker -groupSet data22 --isData -tag 1L2LvX.YY -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config --submitToGrid 
```
while for Run 3 MC (for example, Z+jets)
```
run_xAODNtMaker -groupSet mc23a -tag 1L2LvX.YY -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config --submitToGrid  -includeFiles zjets.txt 
```

### Download to Local Disc
To download the files that have been produced on the grid do the following (the download of the skims is skipped here)

```
run_download --disableSkims -d DOWNLOADDIR -u USER -tag MYTAG -groupSet MYGROUPSET -deepConfig CONFIG
```
where `USER` refers to the rucio account name of the user who submitted the tasks.

**Hint:** Downloading might take some time, in particular when experimental
systematics have been processed as well. You can "parallelize" this step to some extend
by using `-includeFiles` flag and e.g. one job per file to your local batch system.


### Merge Samples
To merge the downloaded samples type
```
run_merge -d DOWNLOADDIR -u USER -tag MYTAG -groupSet MYGROUPSET -deepConfig CONFIG
```
This step will also calculate the sum of weights and adjust the `genWeight` branch to include it. As default it will normalize the trees in MC to 1/pb, which can be adjusted with the flag `-lumi`.

**Hint:** Similar to the download step above, merging ntuples can take quite some
time hence it may be beneficial to also "parallalize" the merging step via a batch
system (again via the `-includeFiles` flag). Merging is also quite i/o intensive
so it's best done on some fast disk (typically the local `\tmp` dir suits that
purpose well).


The run_merge command has to be done twice. The first one will create the folder.
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -u eballabe -d ./ -tag 1L2LvX.YY -groupSet mc23a -includeFiles zjets.txt
```
It is expected that it will give bad errors the first time due to missing samples.

You shall simlink the samples into the newly created tree folder
```
ln -s ../../whatever_tree_folder/user.*.zjets* mc23a/1L2Lv*/trees
```
and now you shall rerun the command againg
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -u eballabe -d ./ -tag 1L2LvX.YY -groupSet mc23a -lumi 26071.4 -includeFiles zjets.txt
```
The `-lumi 26071.4` corresponds to mc23a. For the lumi values to set, different values are reported in the [Run2](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GoodRunListsForAnalysisRun2) and [Run3](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GoodRunListsForAnalysisRun3) TWikis. The values change according to the GRL and the OflLumi tag, so always check these two with the ones set in the config files.

## Monitoring Grid Productions
Unfortunately, we don't submit our tasks and they finish completely automatically,
at least not all of them. For a certain fraction some interventions are required,
mostly re-trying tasks that ended in finished state, i.e. did not process all available
events due to some temporary problems on the grid site (e.g. input files not available, ...).

The (to my knowledge) only official tool to make these interventions is `pbook` which is available
on the command line when you set up panda via `lsetup panda`. But this tool is arguably
not very convenient to use, hence people have built wrappers around it. One useful package
that might come in handy is [pandamonium](https://github.com/dguest/pandamonium).
It's probably best installed (in a virtual python environment) via pip

```shell
pip install pandamonium
```

There are a couple of examples in the package's README, but the most basic call to
list all your grid tasks can be already quite helpful (use your account name):

```shell
pandamon user.<your user name>
```

This gives you a nice brief overview of the progress of all your tasks (by just
sending an http request to the BigPanda web page). It has
quite some functionality, have a look via `pandamon -h`. It becomes really useful
as you can restrict to finished tasks and pipe them into a resubmit command:

```shell
pandamon user.<your user name> -i finished -t | panda-resub-taskid
```

That can safe a lot of work. As of now, two of of these "pipeable" wrappers around `pbook`
are shipped this package: `panda-resub-taskid` and `panda-kill-taskid`. They also come
with some options (see again via `-h`) that are worth knowing.

Final remark: there are probably plenty of other ways and tools to get your ntuple produced
on the grid (which essentially extend the poor functionality of the basis tools), that's just
one of them. Always look out if other people are doing something smarter!

### Tips & Tricks to known Problems during Grid Productions
Please find below a some solutions to problems that can occur when running on the
grid. Don't hesitate to add your own tips & tricks that might help others!

#### Handling exhausted tasks
Exhausted tasks should be resubmitted with more files per job which can be
achieved via a flag of the resubmit executable of pandamonium:

```shell
panda-resub-taskid -n X
```

## Remarks to Continous Integration
As usual, a set of instructions is defined in the [CI file](.gitlab-ci.yml) to build the project, run the selectors and validate their output after changes to the code and pushing them to the repository. To avoid broken pipelines, please find some remarks below to fix common problems.

### Updating the Analysis Release
After moving to a more recent Analysis Release it needs to be updated in several places:
* update the associated variable at the top of [.gitlab-ci.yml](.gitlab-ci.yml)
* update the name of the base image in [Dockerfile](Dockerfile)
* at the top of this README to keep the documentation up to date ;).

### Submission to the grid 


data15 Run2 on grid
```
run_xAODNtMaker -groupSet data15 --isData -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid 
```
data16 Run2 on grid
```
run_xAODNtMaker -groupSet data16 --isData -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid 
```
mc20a Run2 on grid
```
run_xAODNtMaker -groupSet mc20a -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid  -includeFiles wjets.txt 
```
signalmc20a Run2 on grid
```
run_xAODNtMaker -groupSet signalmc20a -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid  -includeFiles signal_200_50.txt
```
signal_fs_mc20a Run2 on grid
```
run_xAODNtMaker -groupSet signal_fs_mc20a -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid  -includeFiles signal_200_50.txt
```


data17 Run2 on grid
```
run_xAODNtMaker -groupSet data17 --isData -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid 
```
mc20d Run2 on grid
```
run_xAODNtMaker -groupSet mc20d -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid  -includeFiles wjets.txt 
```
signalmc20d Run2 on grid
```
run_xAODNtMaker -groupSet signalmc20d -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid  -includeFiles signal_200_50.txt
```
signal_fs_mc20d Run2 on grid
```
run_xAODNtMaker -groupSet signal_fs_mc20d -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid  -includeFiles signal_200_50.txt
```


data18 Run2 on grid
```
run_xAODNtMaker -groupSet data18 --isData -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid 
```
mc20e Run2 on grid
```
run_xAODNtMaker -groupSet mc20e -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid  -includeFiles wjets.txt 
```
signalmc20e Run2 on grid
```
run_xAODNtMaker -groupSet signalmc20e -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid  -includeFiles signal_200_50.txt
```
signal_fs_mc20e Run2 on grid
```
run_xAODNtMaker -groupSet signal_fs_mc20e -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config --submitToGrid  -includeFiles signal_200_50.txt
```


data22 Run3 on grid
```
run_xAODNtMaker -groupSet data22 --isData -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config --submitToGrid 
```
mc23a Run3 on grid
```
run_xAODNtMaker -groupSet mc23a -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config --submitToGrid  -includeFiles wjets.txt 
```
signalmc23a Run3 on grid
```
run_xAODNtMaker -groupSet signalmc23a -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config --submitToGrid  -includeFiles signal_200_50.txt 
```
signal_fs_mc23a Run3 on grid
```
run_xAODNtMaker -groupSet signal_fs_mc23a -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config --submitToGrid  -includeFiles signal_200_50.txt 
```


data23 Run3 on grid
```
run_xAODNtMaker -groupSet data23 --isData -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config --submitToGrid 
```
mc23d Run3 on grid
```
run_xAODNtMaker -groupSet mc23d -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config --submitToGrid  -includeFiles wjets.txt 
```
signalmc23d Run3 on grid
```
run_xAODNtMaker -groupSet signalmc23d -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config --submitToGrid  -includeFiles signal_200_50.txt 
```
signal_fs_mc23d Run3 on grid
```
run_xAODNtMaker -groupSet signal_fs_mc23d -tag 1L2Lv6 -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config --submitToGrid  -includeFiles signal_200_50.txt 
```


### Merge samples (requires lsetup pyami)
### run_merge command to be done twice, first time to create the folder and a second time to merge the trees. 

data15
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet data15
```
data16
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet data16
```
mc20a
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet mc20a -lumi 36104.16 -includeFiles wjets.txt
```
signalmc20a
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet signalmc20a -lumi 36104.16 -includeFiles signal_200_50.txt
```
signal_fs_mc20a
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet signal_fs_mc20a -lumi 36104.16 -includeFiles signal_200_50.txt
```


data17
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet data17
```
mc20d
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet mc20d -lumi 44307.4 -includeFiles wjets.txt
```
signalmc20d
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet signalmc20d -lumi 44307.4 -includeFiles signal_200_50.txt
```
signal_fs_mc20d
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet signal_fs_mc20d -lumi 44307.4 -includeFiles signal_200_50.txt
```


data18
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet data18
```
mc20e
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet mc20e -lumi 58450.1 -includeFiles wjets.txt
```
signalmc20e
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet signalmc20e -lumi 58450.1 -includeFiles signal_200_50.txt
```
signal_fs_mc20e
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet signal_fs_mc20e -lumi 58450.1 -includeFiles signal_200_50.txt
```


data22
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet data22
```
mc23a
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet mc23a -lumi 26071.4 -includeFiles zjets.txt
```
signalmc23a
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet signalmc23a -lumi 26071.4 -includeFiles signal_200_50.txt
```
signal_fs_mc23a
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet signal_fs_mc23a -lumi 26071.4 -includeFiles signal_200_50.txt
```


data23
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet data23
```
mc23d
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet mc23d -lumi 25767.5 -includeFiles wjets.txt
```
signalmc23d
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet signalmc23d -lumi 25767.5 -includeFiles signal_200_50.txt
```
signal_fs_mc23d
```
run_merge -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -u eballabe -d ./ -tag 1L2Lv6 -groupSet signal_fs_mc23d -lumi 25767.5 -includeFiles signal_200_50.txt
```


### TWikis with recommendations
* [DAOD_PHYS derivation](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PhysPhysliteProductionRun3)
* [Run2 MC20 samples](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/CentralMC20ProductionListNew)
* [Run3 MC23 samples](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/CentralMC23ProductionListNew)
* [GRL Run2](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GoodRunListsForAnalysisRun2)
* [GRL Run3](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GoodRunListsForAnalysisRun3)
* [Lowest unprescaled triggers](https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled)
* [Trigger recommendations](https://twiki.cern.ch/twiki/bin/view/Atlas/Run3TriggerRec)
* [Luminosity](https://twiki.cern.ch/twiki/bin/view/Atlas/LuminosityForPhysics)
